-- MySQL dump 10.14  Distrib 5.5.45-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pluckt
-- ------------------------------------------------------
-- Server version	5.5.45-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `review_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `follow_id` int(11) DEFAULT NULL,
  `user_list_video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD06F647A76ED395` (`user_id`),
  KEY `IDX_FD06F647F8697D13` (`comment_id`),
  KEY `IDX_FD06F6473E2E969B` (`review_id`),
  KEY `IDX_FD06F6478711D3BC` (`follow_id`),
  KEY `IDX_FD06F647B9229E7E` (`user_list_video_id`),
  CONSTRAINT `FK_FD06F6473E2E969B` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`),
  CONSTRAINT `FK_FD06F6478711D3BC` FOREIGN KEY (`follow_id`) REFERENCES `follow` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FD06F647A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_FD06F647B9229E7E` FOREIGN KEY (`user_list_video_id`) REFERENCES `user_list_video` (`id`),
  CONSTRAINT `FK_FD06F647F8697D13` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,1,5,NULL,'2015-10-12 15:55:18','comment',NULL,NULL),(2,1,NULL,2,'2015-10-12 16:02:56','review',NULL,NULL),(4,1,NULL,NULL,'2015-10-13 02:04:22','follow',2,NULL),(12,1,NULL,NULL,'2015-10-15 15:38:51','follow',10,NULL),(15,1,6,NULL,'2015-11-06 19:53:51','comment',NULL,NULL),(16,1,7,NULL,'2015-11-06 20:22:25','comment',NULL,NULL),(18,1,NULL,NULL,'2015-11-18 15:03:53','userlistvideo',NULL,2),(19,1,NULL,NULL,'2015-11-18 15:05:27','userlistvideo',NULL,3),(20,1,NULL,3,'2015-12-10 00:25:17','review',NULL,NULL),(21,1,8,NULL,'2015-12-10 00:32:49','comment',NULL,NULL);
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artist`
--

DROP TABLE IF EXISTS `artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic_id` int(11) DEFAULT NULL,
  `page_cover_id` int(11) DEFAULT NULL,
  `aka` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `bio` longtext COLLATE utf8_unicode_ci NOT NULL,
  `birth_city` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `birth_country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `base_city` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `base_country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `quote` longtext COLLATE utf8_unicode_ci NOT NULL,
  `schools` longtext COLLATE utf8_unicode_ci NOT NULL,
  `trivia` longtext COLLATE utf8_unicode_ci NOT NULL,
  `agent_info` longtext COLLATE utf8_unicode_ci NOT NULL,
  `facebook_url` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_url` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `companies` longtext COLLATE utf8_unicode_ci NOT NULL,
  `links` longtext COLLATE utf8_unicode_ci NOT NULL,
  `projects` longtext COLLATE utf8_unicode_ci NOT NULL,
  `training` longtext COLLATE utf8_unicode_ci NOT NULL,
  `filmography` longtext COLLATE utf8_unicode_ci NOT NULL,
  `awards` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1599687F67B9AF6` (`profile_pic_id`),
  UNIQUE KEY `UNIQ_159968712B821C9` (`page_cover_id`),
  CONSTRAINT `FK_159968712B821C9` FOREIGN KEY (`page_cover_id`) REFERENCES `media_upload` (`id`),
  CONSTRAINT `FK_1599687F67B9AF6` FOREIGN KEY (`profile_pic_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artist`
--

LOCK TABLES `artist` WRITE;
/*!40000 ALTER TABLE `artist` DISABLE KEYS */;
INSERT INTO `artist` VALUES (3,'Joel Ruiz','BB',NULL,NULL,'','male','1990-11-30','','','AF','','AF','Test Edit Quote','','','','','','','','','','',''),(4,'Test Artist','DZ',6,7,'Just Another Test Artist','male','2015-08-02','Bio','','AO','','AD','','','','','','','','','','','',''),(5,'Lino Brocka','PH',NULL,NULL,'','male','2015-10-19','','','PH','','PH','','','','','','','','','','','','');
/*!40000 ALTER TABLE `artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artist_type`
--

DROP TABLE IF EXISTS `artist_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist_type` (
  `artist_id` int(11) NOT NULL,
  `type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`artist_id`,`type_id`),
  KEY `IDX_3060D1B6B7970CF8` (`artist_id`),
  CONSTRAINT `FK_3060D1B6B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artist_type`
--

LOCK TABLES `artist_type` WRITE;
/*!40000 ALTER TABLE `artist_type` DISABLE KEYS */;
INSERT INTO `artist_type` VALUES (3,1),(3,2),(3,3),(4,1),(4,4),(4,5),(5,1);
/*!40000 ALTER TABLE `artist_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `award_event`
--

DROP TABLE IF EXISTS `award_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_id` int(11) DEFAULT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_34884A91F98F144A` (`logo_id`),
  CONSTRAINT `FK_34884A91F98F144A` FOREIGN KEY (`logo_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award_event`
--

LOCK TABLES `award_event` WRITE;
/*!40000 ALTER TABLE `award_event` DISABLE KEYS */;
INSERT INTO `award_event` VALUES (1,61,'Cinemalaya','Cinemalaya'),(2,62,'Gawad Urian','Gawad Urian Awards 2015');
/*!40000 ALTER TABLE `award_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `target_class` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CA76ED395` (`user_id`),
  CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (5,1,'2015-10-12 15:55:18','Test with activity log','Pluckt\\AdminBundle\\Entity\\Video','3'),(6,1,'2015-11-06 19:53:51','Test','Pluckt\\AdminBundle\\Entity\\Video','3'),(7,1,'2015-11-06 20:22:25','Another test for the counter.','Pluckt\\AdminBundle\\Entity\\Video','3'),(8,1,'2015-12-10 00:32:49','test','Pluckt\\AdminBundle\\Entity\\Video','6');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curated_list`
--

DROP TABLE IF EXISTS `curated_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curated_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag_featured` tinyint(1) NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `flag_pick` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curated_list`
--

LOCK TABLES `curated_list` WRITE;
/*!40000 ALTER TABLE `curated_list` DISABLE KEYS */;
INSERT INTO `curated_list` VALUES (1,1,'Curated Films: Brocka Classics',0),(2,1,'Cinemalaya 2014',0),(3,0,'Staff Picks',1);
/*!40000 ALTER TABLE `curated_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curated_list_video`
--

DROP TABLE IF EXISTS `curated_list_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curated_list_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3F9CFD2E3DAE168B` (`list_id`),
  KEY `IDX_3F9CFD2E29C1004E` (`video_id`),
  KEY `order_id_idx` (`order_id`),
  CONSTRAINT `FK_3F9CFD2E29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_3F9CFD2E3DAE168B` FOREIGN KEY (`list_id`) REFERENCES `curated_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curated_list_video`
--

LOCK TABLES `curated_list_video` WRITE;
/*!40000 ALTER TABLE `curated_list_video` DISABLE KEYS */;
INSERT INTO `curated_list_video` VALUES (26,1,6,1),(27,1,7,2),(28,1,8,3),(29,2,3,1),(30,2,4,2),(35,3,3,1),(36,3,4,2),(37,3,8,3),(38,3,6,4);
/*!40000 ALTER TABLE `curated_list_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fn_page`
--

DROP TABLE IF EXISTS `fn_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fn_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `video_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C8F9F9ADF67B9AF6` (`profile_pic_id`),
  KEY `IDX_C8F9F9AD29C1004E` (`video_id`),
  KEY `IDX_C8F9F9ADA76ED395` (`user_id`),
  CONSTRAINT `FK_C8F9F9AD29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_C8F9F9ADA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_C8F9F9ADF67B9AF6` FOREIGN KEY (`profile_pic_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fn_page`
--

LOCK TABLES `fn_page` WRITE;
/*!40000 ALTER TABLE `fn_page` DISABLE KEYS */;
INSERT INTO `fn_page` VALUES (1,'Mansyon Test Field Note','',45,'Test description for a field note page.','',4,'0000-00-00 00:00:00',1),(2,'Story of Baby Angelo: Shooting the Film','',44,'Story inspirations could practically come from anywhere. In direk Joel Ruiz\'s case, it came from an elevator.','<p>\"It is a different look at how lives intersect. How each of us unconsciously affects one another,\" Joel explains. A resident director and one of the founders of Arkeomedia, Joel has proven prowess both in writing and directing. Mansyon, the Best Short Film recipient of 2005\'s Cinemalaya is just one among his roster which includes Ang Kapatid kong si Elvis, Putot, and Big Time. Each had garnered awards and nominations from competitions here and abroad.</p>\r\n<p>Despite more than enough experience in the industry, he admits that \'Baby Angelo\' still caused him jitters as this is the first time he directed a full length feature. In his words, \"It was very difficult, exhilarating, and depressing.\"</p>',3,'0000-00-00 00:00:00',1),(3,'Test Normal Field Note','',46,'Test normal field note','',3,'2015-10-15 22:46:10',1),(4,'Test Field Note 2','',47,'Test','',3,'2015-10-15 22:48:14',1);
/*!40000 ALTER TABLE `fn_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follow`
--

DROP TABLE IF EXISTS `follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `target_class` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68344470A76ED395` (`user_id`),
  CONSTRAINT `FK_68344470A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow`
--

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;
INSERT INTO `follow` VALUES (2,1,'2015-10-13 02:04:22','Pluckt\\UserBundle\\Entity\\User','9'),(10,1,'2015-10-15 15:38:51','Pluckt\\UserBundle\\Entity\\User','1');
/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_upload`
--

DROP TABLE IF EXISTS `media_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_create_id` int(11) DEFAULT NULL,
  `filename` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `engine` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `date_create` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ABC47CC1EEFE5067` (`user_create_id`),
  CONSTRAINT `FK_ABC47CC1EEFE5067` FOREIGN KEY (`user_create_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_upload`
--

LOCK TABLES `media_upload` WRITE;
/*!40000 ALTER TABLE `media_upload` DISABLE KEYS */;
INSERT INTO `media_upload` VALUES (1,1,'55bc850556dfe.png','http://pluckt/uploads/fe/6d/55bc850556dfe.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/fe\\/6d\\/fe\\/6d\\/55bc850556dfe.png\"}','2015-08-01 16:36:21'),(2,1,'55bcab46b0457.png','http://pluckt/uploads/57/04/55bcab46b0457.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/57\\/04\\/57\\/04\\/55bcab46b0457.png\"}','2015-08-01 19:19:34'),(3,1,'55bcacd74806a.png','http://pluckt/uploads/6a/80/55bcacd74806a.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/6a\\/80\\/6a\\/80\\/55bcacd74806a.png\"}','2015-08-01 19:26:15'),(4,1,'55bda1447f6d8.png','http://pluckt/uploads/d8/f6/55bda1447f6d8.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/d8\\/f6\\/d8\\/f6\\/55bda1447f6d8.png\"}','2015-08-02 12:49:08'),(5,1,'55bda14788d75.png','http://pluckt/uploads/75/8d/55bda14788d75.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/75\\/8d\\/75\\/8d\\/55bda14788d75.png\"}','2015-08-02 12:49:11'),(6,1,'55bdb05cd3de4.png','http://pluckt/uploads/e4/3d/55bdb05cd3de4.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/e4\\/3d\\/e4\\/3d\\/55bdb05cd3de4.png\"}','2015-08-02 13:53:32'),(7,1,'55bdb06035843.png','http://pluckt/uploads/43/58/55bdb06035843.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/43\\/58\\/43\\/58\\/55bdb06035843.png\"}','2015-08-02 13:53:36'),(8,1,'55bf037d0fd49.jpg','http://pluckt/uploads/49/fd/55bf037d0fd49.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/49\\/fd\\/49\\/fd\\/55bf037d0fd49.jpg\"}','2015-08-03 14:00:29'),(9,1,'55bf039021984.jpg','http://pluckt/uploads/84/19/55bf039021984.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/84\\/19\\/84\\/19\\/55bf039021984.jpg\"}','2015-08-03 14:00:48'),(10,1,'55f44c4664258.jpg','http://pluckt/uploads/58/42/55f44c4664258.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/58\\/42\\/58\\/42\\/55f44c4664258.jpg\"}','2015-09-13 00:01:10'),(11,1,'55f44c6586a39.jpg','http://pluckt/uploads/39/6a/55f44c6586a39.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/39\\/6a\\/39\\/6a\\/55f44c6586a39.jpg\"}','2015-09-13 00:01:41'),(12,1,'55f44c6b9d18f.jpg','http://pluckt/uploads/8f/d1/55f44c6b9d18f.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/8f\\/d1\\/8f\\/d1\\/55f44c6b9d18f.jpg\"}','2015-09-13 00:01:47'),(13,1,'55f44c8fce61b.jpg','http://pluckt/uploads/1b/e6/55f44c8fce61b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/1b\\/e6\\/1b\\/e6\\/55f44c8fce61b.jpg\"}','2015-09-13 00:02:23'),(14,1,'55f44c945a1e3.jpg','http://pluckt/uploads/e3/a1/55f44c945a1e3.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/e3\\/a1\\/e3\\/a1\\/55f44c945a1e3.jpg\"}','2015-09-13 00:02:28'),(15,1,'55f44d2644ece.jpg','http://pluckt/uploads/ce/4e/55f44d2644ece.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/ce\\/4e\\/ce\\/4e\\/55f44d2644ece.jpg\"}','2015-09-13 00:04:54'),(16,1,'55f44d2853db4.jpg','http://pluckt/uploads/b4/3d/55f44d2853db4.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/b4\\/3d\\/b4\\/3d\\/55f44d2853db4.jpg\"}','2015-09-13 00:04:56'),(17,1,'55f4da35b3663.jpg','http://pluckt/uploads/63/36/55f4da35b3663.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/63\\/36\\/63\\/36\\/55f4da35b3663.jpg\"}','2015-09-13 10:06:45'),(18,1,'55f4da8695c97.jpg','http://pluckt/uploads/97/5c/55f4da8695c97.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/97\\/5c\\/97\\/5c\\/55f4da8695c97.jpg\"}','2015-09-13 10:08:06'),(19,1,'55f4db1364849.jpg','http://pluckt/uploads/49/48/55f4db1364849.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/49\\/48\\/49\\/48\\/55f4db1364849.jpg\"}','2015-09-13 10:10:27'),(20,1,'55f4db1d431f0.jpg','http://pluckt/uploads/f0/31/55f4db1d431f0.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/f0\\/31\\/f0\\/31\\/55f4db1d431f0.jpg\"}','2015-09-13 10:10:37'),(21,1,'55f4db25b2880.jpg','http://pluckt/uploads/80/28/55f4db25b2880.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/80\\/28\\/80\\/28\\/55f4db25b2880.jpg\"}','2015-09-13 10:10:45'),(22,1,'55f4db2e78bc3.jpg','http://pluckt/uploads/c3/8b/55f4db2e78bc3.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/c3\\/8b\\/c3\\/8b\\/55f4db2e78bc3.jpg\"}','2015-09-13 10:10:54'),(23,1,'55f52531a44ae.jpg','http://pluckt/uploads/ae/44/55f52531a44ae.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/ae\\/44\\/ae\\/44\\/55f52531a44ae.jpg\"}','2015-09-13 15:26:41'),(24,1,'55fbfc6442f8d.jpg','http://pluckt/uploads/8d/2f/55fbfc6442f8d.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/8d\\/2f\\/8d\\/2f\\/55fbfc6442f8d.jpg\"}','2015-09-18 19:58:28'),(25,1,'55fbfc77d9f6b.jpg','http://pluckt/uploads/6b/9f/55fbfc77d9f6b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/6b\\/9f\\/6b\\/9f\\/55fbfc77d9f6b.jpg\"}','2015-09-18 19:58:47'),(26,1,'55fbfc898dbc3.jpg','http://pluckt/uploads/c3/db/55fbfc898dbc3.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/c3\\/db\\/c3\\/db\\/55fbfc898dbc3.jpg\"}','2015-09-18 19:59:05'),(27,1,'55fbfc9350cea.jpg','http://pluckt/uploads/ea/0c/55fbfc9350cea.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/ea\\/0c\\/ea\\/0c\\/55fbfc9350cea.jpg\"}','2015-09-18 19:59:15'),(28,1,'56012c172e5aa.png','http://pluckt/uploads/aa/e5/56012c172e5aa.png',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/aa\\/e5\\/aa\\/e5\\/56012c172e5aa.png\"}','2015-09-22 18:23:19'),(29,1,'56012c2af3489.jpg','http://pluckt/uploads/89/34/56012c2af3489.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/89\\/34\\/89\\/34\\/56012c2af3489.jpg\"}','2015-09-22 18:23:38'),(30,1,'56012c36128f5.jpg','http://pluckt/uploads/f5/28/56012c36128f5.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/f5\\/28\\/f5\\/28\\/56012c36128f5.jpg\"}','2015-09-22 18:23:50'),(31,1,'56012c4a9ff6e.jpg','http://pluckt/uploads/6e/ff/56012c4a9ff6e.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/6e\\/ff\\/6e\\/ff\\/56012c4a9ff6e.jpg\"}','2015-09-22 18:24:10'),(32,1,'56012c5646fde.jpg','http://pluckt/uploads/de/6f/56012c5646fde.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/de\\/6f\\/de\\/6f\\/56012c5646fde.jpg\"}','2015-09-22 18:24:22'),(33,1,'56020178a4f0c.jpg','http://pluckt/uploads/0c/4f/56020178a4f0c.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/0c\\/4f\\/0c\\/4f\\/56020178a4f0c.jpg\"}','2015-09-23 09:33:44'),(34,1,'560201b5545ca.jpg','http://pluckt/uploads/ca/45/560201b5545ca.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/ca\\/45\\/ca\\/45\\/560201b5545ca.jpg\"}','2015-09-23 09:34:45'),(35,1,'560201e88de49.jpg','http://pluckt/uploads/49/de/560201e88de49.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/49\\/de\\/49\\/de\\/560201e88de49.jpg\"}','2015-09-23 09:35:36'),(36,1,'560d5e25b69e6.jpg','http://pluckt/uploads/e6/69/560d5e25b69e6.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/e6\\/69\\/e6\\/69\\/560d5e25b69e6.jpg\"}','2015-10-02 00:24:05'),(37,1,'56114fecadc7b.jpg','http://pluckt/uploads/7b/dc/56114fecadc7b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/7b\\/dc\\/7b\\/dc\\/56114fecadc7b.jpg\"}','2015-10-05 00:12:28'),(38,1,'56114fef585b8.jpg','http://pluckt/uploads/b8/85/56114fef585b8.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/b8\\/85\\/b8\\/85\\/56114fef585b8.jpg\"}','2015-10-05 00:12:31'),(39,1,'561cdcd50f338.jpg','http://pluckt/uploads/38/f3/561cdcd50f338.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/38\\/f3\\/38\\/f3\\/561cdcd50f338.jpg\"}','2015-10-13 18:28:37'),(40,1,'561f51978a0c3.jpg','http://pluckt/uploads/c3/a0/561f51978a0c3.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/c3\\/a0\\/c3\\/a0\\/561f51978a0c3.jpg\"}','2015-10-15 15:11:19'),(41,1,'561f519c5b6ca.jpg','http://pluckt/uploads/ca/b6/561f519c5b6ca.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/ca\\/b6\\/ca\\/b6\\/561f519c5b6ca.jpg\"}','2015-10-15 15:11:24'),(42,1,'561f51d4c2d21.jpg','http://pluckt/uploads/21/2d/561f51d4c2d21.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/21\\/2d\\/21\\/2d\\/561f51d4c2d21.jpg\"}','2015-10-15 15:12:20'),(43,1,'561f51dc7f443.jpg','http://pluckt/uploads/43/f4/561f51dc7f443.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/43\\/f4\\/43\\/f4\\/561f51dc7f443.jpg\"}','2015-10-15 15:12:28'),(44,1,'561f5a4638e42.jpg','http://pluckt/uploads/42/8e/561f5a4638e42.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/42\\/8e\\/42\\/8e\\/561f5a4638e42.jpg\"}','2015-10-15 15:48:22'),(45,1,'561fa7a2b00db.jpg','http://pluckt/uploads/db/00/561fa7a2b00db.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/db\\/00\\/db\\/00\\/561fa7a2b00db.jpg\"}','2015-10-15 21:18:26'),(46,1,'561fbc30bc21b.jpg','http://pluckt/uploads/1b/c2/561fbc30bc21b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/1b\\/c2\\/1b\\/c2\\/561fbc30bc21b.jpg\"}','2015-10-15 22:46:08'),(47,1,'561fbcac037b6.jpg','http://pluckt/uploads/b6/37/561fbcac037b6.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/b6\\/37\\/b6\\/37\\/561fbcac037b6.jpg\"}','2015-10-15 22:48:12'),(48,1,'5623db5309e9a.jpg','http://pluckt/uploads/9a/9e/5623db5309e9a.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/9a\\/9e\\/9a\\/9e\\/5623db5309e9a.jpg\"}','2015-10-19 01:48:03'),(49,1,'5623dd0d34b1b.jpg','http://pluckt/uploads/1b/4b/5623dd0d34b1b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/1b\\/4b\\/1b\\/4b\\/5623dd0d34b1b.jpg\"}','2015-10-19 01:55:25'),(50,1,'5623ddc873bbc.jpg','http://pluckt/uploads/bc/3b/5623ddc873bbc.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/bc\\/3b\\/bc\\/3b\\/5623ddc873bbc.jpg\"}','2015-10-19 01:58:32'),(51,1,'5623e0be3426a.jpg','http://pluckt/uploads/6a/42/5623e0be3426a.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/6a\\/42\\/6a\\/42\\/5623e0be3426a.jpg\"}','2015-10-19 02:11:10'),(52,1,'5623e18975369.jpg','http://pluckt/uploads/69/53/5623e18975369.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/69\\/53\\/69\\/53\\/5623e18975369.jpg\"}','2015-10-19 02:14:33'),(53,1,'563e5029de4a2.jpg','http://pluckt/uploads/a2/e4/563e5029de4a2.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/a2\\/e4\\/a2\\/e4\\/563e5029de4a2.jpg\"}','2015-11-08 03:25:29'),(54,1,'563e502e5cad0.jpg','http://pluckt/uploads/d0/ca/563e502e5cad0.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/d0\\/ca\\/d0\\/ca\\/563e502e5cad0.jpg\"}','2015-11-08 03:25:34'),(55,1,'563e521f64ce8.jpg','http://pluckt/uploads/e8/4c/563e521f64ce8.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/e8\\/4c\\/e8\\/4c\\/563e521f64ce8.jpg\"}','2015-11-08 03:33:51'),(56,1,'563e5221656de.jpg','http://pluckt/uploads/de/56/563e5221656de.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/de\\/56\\/de\\/56\\/563e5221656de.jpg\"}','2015-11-08 03:33:53'),(57,1,'563e52255ca9b.jpg','http://pluckt/uploads/9b/ca/563e52255ca9b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/9b\\/ca\\/9b\\/ca\\/563e52255ca9b.jpg\"}','2015-11-08 03:33:57'),(58,1,'563e52295ade2.jpg','http://pluckt/uploads/e2/ad/563e52295ade2.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/e2\\/ad\\/e2\\/ad\\/563e52295ade2.jpg\"}','2015-11-08 03:34:01'),(59,1,'563e5727d585b.jpg','http://pluckt/uploads/5b/58/563e5727d585b.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/5b\\/58\\/5b\\/58\\/563e5727d585b.jpg\"}','2015-11-08 03:55:19'),(60,1,'5643640472a41.jpg','http://pluckt/uploads/41/2a/5643640472a41.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/41\\/2a\\/41\\/2a\\/5643640472a41.jpg\"}','2015-11-11 23:51:32'),(61,1,'56436469c53c0.jpg','http://pluckt/uploads/c0/53/56436469c53c0.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/c0\\/53\\/c0\\/53\\/56436469c53c0.jpg\"}','2015-11-11 23:53:13'),(62,1,'5648a2b305880.jpg','http://pluckt/uploads/80/58/5648a2b305880.jpg',1,'Zulu\\MediaBundle\\Model\\Engine\\LocalFile','{\"full_path\":\"\\/var\\/www\\/html\\/pluckt\\/app\\/..\\/web\\/uploads\\/80\\/58\\/80\\/58\\/5648a2b305880.jpg\"}','2015-11-15 23:20:19');
/*!40000 ALTER TABLE `media_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `date_crate` datetime NOT NULL,
  `quantity` decimal(8,2) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `meta` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase`
--

LOCK TABLES `purchase` WRITE;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `target_class` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_794381C6A76ED395` (`user_id`),
  CONSTRAINT `FK_794381C6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (2,'2015-10-12 16:02:56','Test review with activity log.','Test',5,1,'Pluckt\\AdminBundle\\Entity\\Video','3'),(3,'2015-12-10 00:25:17','Another Test','3 star test',3,1,'Pluckt\\AdminBundle\\Entity\\Video','3');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `wallet_amount` decimal(8,2) NOT NULL,
  `fb_id` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_token` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pic_id` int(11) DEFAULT NULL,
  `page_cover_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `bio` longtext COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `twitter_id` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_token` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649F67B9AF6` (`profile_pic_id`),
  UNIQUE KEY `UNIQ_8D93D64912B821C9` (`page_cover_id`),
  UNIQUE KEY `UNIQ_8D93D649B7970CF8` (`artist_id`),
  CONSTRAINT `FK_8D93D64912B821C9` FOREIGN KEY (`page_cover_id`) REFERENCES `media_upload` (`id`),
  CONSTRAINT `FK_8D93D649B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`),
  CONSTRAINT `FK_8D93D649F67B9AF6` FOREIGN KEY (`profile_pic_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','wasted_brain@yahoo.com','wasted_brain@yahoo.com',1,'mwpgyqc1jyso4ow40gcc8soo4ogss4g','$2y$13$mwpgyqc1jyso4ow40gcc8e9Zr7PzEySb0Ui0MoJSGOH18vD9BxW5G','2015-12-09 23:06:51',0,0,NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',0,NULL,'Test',0.00,'','',43,42,'2015-09-13 10:37:01','Joel Ruiz is a Creatives Commercial and Film Director. Writer. Aspiring swimmer. Joel has directed and produced countless corporate and commercial videos and feature works of the company. A graduate of the De La Salle University, he has received numerous awards and recognition for his short films since he began directing in 1998. His notable films include Mansyon, which won best short film at the 1st Cinemalaya Film Fest and Baby Angelo, full-length that was screened in various festivals in U.S., Europe and Asia.','NA','Manila',3,'','','Edit'),(2,'test','test','test@test.com','test@test.com',1,'1jrovg8atan4gkgwgs8goos84gckwgg','$2y$13$1jrovg8atan4gkgwgs8goelDqwzhLK/Fw98O8UnM7fIdlQGSD06vO',NULL,0,0,NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_EDITOR\";}',0,NULL,'Test',0.00,'','',NULL,NULL,'2015-09-13 10:37:01','','AR','',NULL,'','',''),(3,'test2','test2','test@test2.com','test@test2.com',1,'cwjbzftt33kswoo8cko0wkog0400ss8','$2y$13$cwjbzftt33kswoo8cko0weIf6P3yXuZNsfSv6iQ.ymhCz4mR6cJOq',NULL,0,0,NULL,NULL,NULL,'a:1:{i:0;s:14:\"ROLE_READ_ONLY\";}',0,NULL,'Test 2',0.00,'','',NULL,NULL,'2015-09-13 10:37:01','','','',NULL,'','',''),(5,'test3','test3','edit@test.com','edit@test.com',1,'hclfxhv1mwows0ws0kgw0kwcc8os4s4','$2y$13$hclfxhv1mwows0ws0kgw0eeLrNW.W9WDzB4XYDvAiaJBT6BVqV3hy',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'Edited',0.00,'','',NULL,NULL,'2015-09-13 10:37:01','','','',NULL,'','',''),(7,'939450096141153','939450096141153','kc@jankstudio.com','kc@jankstudio.com',1,'c5qcyuxmddwg888ogggg4o40gwk4s84','$2y$13$c5qcyuxmddwg888ogggg4eicmfCgmvI5DrVUbD.kjWNIKorRLZ3GW','2015-09-16 22:09:43',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,' ',0.00,'939450096141153','CAAI9FUEseBoBAMePnEF1IKO9DxZBLOEtA23ugBq5RZCPJeaTArzha49wbNUqLKQq7fa6iQ35gJR3WYFALmETegVJSXkdwIjXRAY1VprhIXCIW05aMhvwOMu',NULL,NULL,'2015-09-13 10:37:01','','','',NULL,'','',''),(8,'2653504026','2653504026',NULL,NULL,1,'fo3f4s2gpu04swosgkws448g8scww4s','$2y$13$fo3f4s2gpu04swosgkws4uSO.UN1Hxp2JdtkY12Y1vUQ5k2NR5qqy','2015-09-16 22:35:00',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,' ',0.00,NULL,NULL,NULL,NULL,'2015-09-16 22:34:59','','PH','Manila',NULL,'2653504026','2653504026-vOM4867bKExoQxbwmAs8pGa26okPpQLDLmqtpzy',''),(9,'10153477008739003','10153477008739003','wasted_brain@yahoo.com','wasted_brain@yahoo.com',1,'trypgruah6skosk4w08csosocgsg0ww','$2y$13$trypgruah6skosk4w08cseqfnn6nG.3P.s2V.wiZgE61zvj.fti.m','2015-09-17 08:53:16',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,' Test User',100.00,'10153477008739003','CAAI9FUEseBoBAAZCZCsnT3AWHPAOdyt1IzxhZAQIPTJQYvdhXlUQPq010btmDNXyvtJpJ74Hz9bsu4q04dGCotZC25uDTETqcQCfnlf2tNf4nqDFuwUGZB1',NULL,NULL,'2015-09-17 08:53:15','','PH','Manila',NULL,NULL,NULL,''),(10,'lino_brocka','lino_brocka','','',1,'29xh99wk1ydcwok8008og08wo480ck4','$2y$13$29xh99wk1ydcwok8008ogut4iM9HZc1jgjfw5CLBqzAPmG0siFCmK',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'Lino Brocka',0.00,NULL,NULL,NULL,NULL,'2015-10-19 02:00:10','','PH','Manila',5,NULL,NULL,'');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_award`
--

DROP TABLE IF EXISTS `user_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `year` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `placement` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6899D5BFA76ED395` (`user_id`),
  KEY `IDX_6899D5BF71F7E88B` (`event_id`),
  KEY `IDX_6899D5BF29C1004E` (`video_id`),
  CONSTRAINT `FK_6899D5BF29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_6899D5BF71F7E88B` FOREIGN KEY (`event_id`) REFERENCES `award_event` (`id`),
  CONSTRAINT `FK_6899D5BFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_award`
--

LOCK TABLES `user_award` WRITE;
/*!40000 ALTER TABLE `user_award` DISABLE KEYS */;
INSERT INTO `user_award` VALUES (9,1,1,4,'2005','Test Award','Winner');
/*!40000 ALTER TABLE `user_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_faves`
--

DROP TABLE IF EXISTS `user_faves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_faves` (
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`video_id`,`user_id`),
  KEY `IDX_30F2B54829C1004E` (`video_id`),
  KEY `IDX_30F2B548A76ED395` (`user_id`),
  CONSTRAINT `FK_30F2B54829C1004E` FOREIGN KEY (`video_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_30F2B548A76ED395` FOREIGN KEY (`user_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_faves`
--

LOCK TABLES `user_faves` WRITE;
/*!40000 ALTER TABLE `user_faves` DISABLE KEYS */;
INSERT INTO `user_faves` VALUES (1,3),(1,4);
/*!40000 ALTER TABLE `user_faves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_follow`
--

DROP TABLE IF EXISTS `user_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_follow` (
  `user_id` int(11) NOT NULL,
  `follow_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`follow_id`),
  KEY `IDX_D665F4DA76ED395` (`user_id`),
  KEY `IDX_D665F4D8711D3BC` (`follow_id`),
  CONSTRAINT `FK_D665F4D8711D3BC` FOREIGN KEY (`follow_id`) REFERENCES `follow` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D665F4DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_follow`
--

LOCK TABLES `user_follow` WRITE;
/*!40000 ALTER TABLE `user_follow` DISABLE KEYS */;
INSERT INTO `user_follow` VALUES (1,10),(9,2);
/*!40000 ALTER TABLE `user_follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_list`
--

DROP TABLE IF EXISTS `user_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3E49B4D1A76ED395` (`user_id`),
  CONSTRAINT `FK_3E49B4D1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_list`
--

LOCK TABLES `user_list` WRITE;
/*!40000 ALTER TABLE `user_list` DISABLE KEYS */;
INSERT INTO `user_list` VALUES (1,1,'2015-10-19 06:28:49','Rename Test'),(2,1,'2015-10-19 17:37:15','New Shelf');
/*!40000 ALTER TABLE `user_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_list_video`
--

DROP TABLE IF EXISTS `user_list_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_list_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `target_class` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23388C33DAE168B` (`list_id`),
  KEY `IDX_23388C329C1004E` (`video_id`),
  KEY `IDX_23388C3A76ED395` (`user_id`),
  CONSTRAINT `FK_23388C329C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_23388C33DAE168B` FOREIGN KEY (`list_id`) REFERENCES `user_list` (`id`),
  CONSTRAINT `FK_23388C3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_list_video`
--

LOCK TABLES `user_list_video` WRITE;
/*!40000 ALTER TABLE `user_list_video` DISABLE KEYS */;
INSERT INTO `user_list_video` VALUES (2,1,4,'2015-11-18 15:03:53',1,'2015-11-18 15:03:53','Pluckt\\AdminBundle\\Entity\\Video','4'),(3,1,6,'2015-11-18 15:05:27',1,'2015-11-18 15:05:27','Pluckt\\AdminBundle\\Entity\\Video','6');
/*!40000 ALTER TABLE `user_list_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vcategory`
--

DROP TABLE IF EXISTS `vcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6DE68593DA5256D` (`image_id`),
  CONSTRAINT `FK_6DE68593DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vcategory`
--

LOCK TABLES `vcategory` WRITE;
/*!40000 ALTER TABLE `vcategory` DISABLE KEYS */;
INSERT INTO `vcategory` VALUES (5,'Horror',22),(6,'Comedy',19),(7,'Drama',21),(8,'Action',18),(9,'Documentary',20);
/*!40000 ALTER TABLE `vcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `media_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `english_title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `rating_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `minutes` smallint(6) NOT NULL,
  `year_produced` smallint(6) NOT NULL,
  `production_company` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `synopsis` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `tags` longtext COLLATE utf8_unicode_ci NOT NULL,
  `website_url` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `aspect_ratio_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `poster_id` int(11) DEFAULT NULL,
  `background_id` int(11) DEFAULT NULL,
  `country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7CC7DA2C5BB66C05` (`poster_id`),
  UNIQUE KEY `UNIQ_7CC7DA2CC93D69EA` (`background_id`),
  CONSTRAINT `FK_7CC7DA2C5BB66C05` FOREIGN KEY (`poster_id`) REFERENCES `media_upload` (`id`),
  CONSTRAINT `FK_7CC7DA2CC93D69EA` FOREIGN KEY (`background_id`) REFERENCES `media_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (2,'Test 2','sdfsdfsdfsd','sdfsdfsdfsd','','','GP',0,0,'','','','','16-9',NULL,NULL,'',0),(3,'Baby Angelo','632dd06f87637da0','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque corporis, amet excepturi. Temporibus nostrum natus officia quasi quae recusandae nihil, in dolores praesentium ad amet, dignissimos unde quos similique impedit.','Baby Angelo','Baby Angelo','R-18',84,2013,'Viva','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque corporis, amet excepturi. Temporibus nostrum natus offic','tag, another tag, some other tag','none','16-9',23,36,'DZ',0),(4,'Mansyon','','Danny and Jonas are two petty crooks who dream of moving into the big scene. When their carefully-planned kidnapping goes askew, Danny and Jonas find themselves in a tangle neither of them can handle.','Mansyon','Mansyon','PG',84,2005,'Arkeofilms','Danny and Jonas are two petty crooks who dream of moving into the big scene. When their carefully-planned kidnapping goe','','none','16-9',37,38,'PH',1),(5,'3','','','','','0',0,0,'','','','','',NULL,NULL,'',0),(6,'Maynila sa mga Kuko ng Liwanag','','Júlio Madiaga, a \'provinciano\', arrives in Manila to search for Ligaya, his loved one.','Maynila sa mga Kuko ng Liwanag','Manila in the Claws of Light','R-16',123,1975,'','Júlio Madiaga, a \'provinciano\', arrives in Manila to search for Ligaya, his loved one.','','','16-9',50,NULL,'PH',0),(7,'Insiang','','The first Filipino film to show at the Cannes Film Festival is set in the slums of Manila. A beautiful girl gets raped by her mother\'s lover, and then learns how to exact revenge.','Insiang','Insiang','GP',95,1976,'','The first Filipino film to show at the Cannes Film Festival is set in the slums of Manila. A beautiful girl gets raped b','','','16-9',51,NULL,'PH',0),(8,'Orapronobis','','In 1985, in the obscure town of Dolores, the Orapronobis, a cult under the leadership of Kumander Kontra (Roco), murders a foreign priest who gave the last rites to an alleged rebel, who was also executed by the same group.','Orapronobis ','Fight for Us','GP',94,1989,'','In 1985, in the obscure town of Dolores, the Orapronobis, a cult under the leadership of Kumander Kontra (Roco), murders','','','16-9',52,NULL,'PH',0);
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_artist_role`
--

DROP TABLE IF EXISTS `video_artist_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_artist_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `artist_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C955BD5429C1004E` (`video_id`),
  KEY `IDX_C955BD54B7970CF8` (`artist_id`),
  CONSTRAINT `FK_C955BD5429C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_C955BD54B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_artist_role`
--

LOCK TABLES `video_artist_role` WRITE;
/*!40000 ALTER TABLE `video_artist_role` DISABLE KEYS */;
INSERT INTO `video_artist_role` VALUES (50,2,3,2),(56,6,5,1),(57,7,5,1),(58,8,5,1),(65,3,3,1),(68,4,3,1);
/*!40000 ALTER TABLE `video_artist_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_award`
--

DROP TABLE IF EXISTS `video_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `year` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `placement` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_539274529C1004E` (`video_id`),
  KEY `IDX_539274571F7E88B` (`event_id`),
  CONSTRAINT `FK_539274529C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  CONSTRAINT `FK_539274571F7E88B` FOREIGN KEY (`event_id`) REFERENCES `award_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_award`
--

LOCK TABLES `video_award` WRITE;
/*!40000 ALTER TABLE `video_award` DISABLE KEYS */;
INSERT INTO `video_award` VALUES (1,4,1,'2013','Best Director','Winner'),(2,4,2,'2015','Best Male Actor','Winner');
/*!40000 ALTER TABLE `video_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_category`
--

DROP TABLE IF EXISTS `video_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_category` (
  `vcat_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`vcat_id`,`video_id`),
  KEY `IDX_AECE2B7D45BB328F` (`vcat_id`),
  KEY `IDX_AECE2B7D29C1004E` (`video_id`),
  CONSTRAINT `FK_AECE2B7D29C1004E` FOREIGN KEY (`video_id`) REFERENCES `vcategory` (`id`),
  CONSTRAINT `FK_AECE2B7D45BB328F` FOREIGN KEY (`vcat_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_category`
--

LOCK TABLES `video_category` WRITE;
/*!40000 ALTER TABLE `video_category` DISABLE KEYS */;
INSERT INTO `video_category` VALUES (2,5),(3,7),(4,7),(4,8),(6,7),(7,7),(8,7),(8,8);
/*!40000 ALTER TABLE `video_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_comment`
--

DROP TABLE IF EXISTS `video_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_comment` (
  `video_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  PRIMARY KEY (`video_id`,`comment_id`),
  KEY `IDX_7199BBC129C1004E` (`video_id`),
  KEY `IDX_7199BBC1F8697D13` (`comment_id`),
  CONSTRAINT `FK_7199BBC129C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_7199BBC1F8697D13` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_comment`
--

LOCK TABLES `video_comment` WRITE;
/*!40000 ALTER TABLE `video_comment` DISABLE KEYS */;
INSERT INTO `video_comment` VALUES (3,5),(3,6),(3,7),(6,8);
/*!40000 ALTER TABLE `video_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_follow`
--

DROP TABLE IF EXISTS `video_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_follow` (
  `video_id` int(11) NOT NULL,
  `follow_id` int(11) NOT NULL,
  PRIMARY KEY (`video_id`,`follow_id`),
  KEY `IDX_5063E4BD29C1004E` (`video_id`),
  KEY `IDX_5063E4BD8711D3BC` (`follow_id`),
  CONSTRAINT `FK_5063E4BD29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_5063E4BD8711D3BC` FOREIGN KEY (`follow_id`) REFERENCES `follow` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_follow`
--

LOCK TABLES `video_follow` WRITE;
/*!40000 ALTER TABLE `video_follow` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_gallery`
--

DROP TABLE IF EXISTS `video_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_gallery` (
  `upload_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`upload_id`,`video_id`),
  KEY `IDX_A2C69197CCCFBA31` (`upload_id`),
  KEY `IDX_A2C6919729C1004E` (`video_id`),
  CONSTRAINT `FK_A2C6919729C1004E` FOREIGN KEY (`video_id`) REFERENCES `media_upload` (`id`),
  CONSTRAINT `FK_A2C69197CCCFBA31` FOREIGN KEY (`upload_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_gallery`
--

LOCK TABLES `video_gallery` WRITE;
/*!40000 ALTER TABLE `video_gallery` DISABLE KEYS */;
INSERT INTO `video_gallery` VALUES (3,55),(3,57),(3,58),(3,59);
/*!40000 ALTER TABLE `video_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_review`
--

DROP TABLE IF EXISTS `video_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_review` (
  `video_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  PRIMARY KEY (`video_id`,`review_id`),
  KEY `IDX_4114210B29C1004E` (`video_id`),
  KEY `IDX_4114210B3E2E969B` (`review_id`),
  CONSTRAINT `FK_4114210B29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_4114210B3E2E969B` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_review`
--

LOCK TABLES `video_review` WRITE;
/*!40000 ALTER TABLE `video_review` DISABLE KEYS */;
INSERT INTO `video_review` VALUES (3,2),(3,3);
/*!40000 ALTER TABLE `video_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_sub_lang`
--

DROP TABLE IF EXISTS `video_sub_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_sub_lang` (
  `video_id` int(11) NOT NULL,
  `lang_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`video_id`,`lang_id`),
  KEY `IDX_BE1D291F29C1004E` (`video_id`),
  CONSTRAINT `FK_BE1D291F29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_sub_lang`
--

LOCK TABLES `video_sub_lang` WRITE;
/*!40000 ALTER TABLE `video_sub_lang` DISABLE KEYS */;
INSERT INTO `video_sub_lang` VALUES (3,'english'),(3,'francais'),(3,'japanese'),(3,'korean'),(4,'english'),(4,'tagalog'),(6,'tagalog'),(7,'tagalog'),(8,'tagalog');
/*!40000 ALTER TABLE `video_sub_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_vocal_lang`
--

DROP TABLE IF EXISTS `video_vocal_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_vocal_lang` (
  `video_id` int(11) NOT NULL,
  `lang_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`video_id`,`lang_id`),
  KEY `IDX_BD3DE72629C1004E` (`video_id`),
  CONSTRAINT `FK_BD3DE72629C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_vocal_lang`
--

LOCK TABLES `video_vocal_lang` WRITE;
/*!40000 ALTER TABLE `video_vocal_lang` DISABLE KEYS */;
INSERT INTO `video_vocal_lang` VALUES (3,'bicolano'),(3,'cebuano'),(3,'english'),(3,'ilokano'),(3,'tagalog'),(4,'tagalog'),(6,'tagalog'),(7,'tagalog'),(8,'tagalog');
/*!40000 ALTER TABLE `video_vocal_lang` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-10  3:08:28
