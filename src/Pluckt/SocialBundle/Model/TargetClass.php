<?php

namespace Pluckt\SocialBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class TargetClass extends SelectOption
{
    public static function getOptionsHash()
    {
        return [
            'Pluckt\AdminBundle\Entity\Video' => 'video',
            'Pluckt\AdminBundle\Entity\Artist' => 'artist',
            'Pluckt\UserBundle\Entity\User' => 'user',
        ];
    }
}
