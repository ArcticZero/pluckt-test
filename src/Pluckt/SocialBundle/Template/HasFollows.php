<?php

namespace Pluckt\SocialBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\SocialBundle\Entity\Follow;

trait HasFollows
{
    /** @ORM\ManyToMany(targetEntity="Pluckt\SocialBundle\Entity\Follow") */
    protected $follows;

    protected function initHasFollows()
    {
        $this->follows = new ArrayCollection();
    }

    public function addFollow(Follow $follow)
    {
        $follow->setTargetClass(get_class($this))
            ->setTargetID($this->getID());

        $this->follows[] = $follow;
        return $this;
    }

    public function getFollows()
    {
        return $this->follows;
    }
}


