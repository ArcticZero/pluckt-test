<?php

namespace Pluckt\SocialBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\SocialBundle\Entity\Review;

trait HasReviews
{
    /** @ORM\ManyToMany(targetEntity="Pluckt\SocialBundle\Entity\Review") */
    protected $reviews;

    protected function initHasReviews()
    {
        $this->reviews = new ArrayCollection();
    }

    public function addReview(Review $review)
    {
        // add self to review
        $review->setTargetClass(get_class($this))
            ->setTargetID($this->getID());

        $this->reviews[] = $review;
        return $this;
    }

    public function getReviews()
    {
        return $this->reviews;
    }

    public function getReviewTally()
    {
        $tally = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];

        foreach ($this->reviews as $review)
        {
            $tally[$review->getRating()]++;
        }

        return $tally;
    }

    public function getAvgRating()
    {
        $ratings = [];

        foreach ($this->reviews as $review)
        {
            $ratings[] = $review->getRating();
        }

        return count($ratings) > 0 ? round(array_sum($ratings) / count($ratings), 1) : 0;
    }


}
