<?php

namespace Pluckt\SocialBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\SocialBundle\Entity\Comment;

trait HasComments
{
    /** @ORM\ManyToMany(targetEntity="Pluckt\SocialBundle\Entity\Comment") */
    protected $comments;

    protected function initHasComments()
    {
        $this->comments = new ArrayCollection();
    }

    public function addComment(Comment $comment)
    {
        // add self to comment
        $comment->setTargetClass(get_class($this))
            ->setTargetID($this->getID());

        $this->comments[] = $comment;
        return $this;
    }

    public function getComments()
    {
        return $this->comments;
    }
}
