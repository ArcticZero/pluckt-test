<?php

namespace Pluckt\SocialBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\SocialBundle\Model\TargetClass;

trait SocialUser
{
    /** @ORM\OneToMany(targetEntity="Pluckt\SocialBundle\Entity\Follow", mappedBy="user") */
    protected $my_follows;

    /** @ORM\OneToMany(targetEntity="Pluckt\SocialBundle\Entity\Review", mappedBy="user") */
    protected $my_reviews;

    /** @ORM\OneToMany(targetEntity="Pluckt\SocialBundle\Entity\Comment", mappedBy="user") */
    protected $my_comments;

    protected $following_hash;

    protected function initSocialUser()
    {
        $this->following_hash = null;
    }

    public function getMyFollows()
    {
        return $this->my_follows;
    }

    public function getMyReviews()
    {
        return $this->my_reviews;
    }

    public function getMyComments()
    {
        return $this->my_comments;
    }

    protected function getFollowingHash()
    {
        if ($this->following_hash != null)
            return $this->following_hash;

        // generate hash
        $this->following_hash = [];
        foreach ($this->my_follows as $fol)
        {
            $type = TargetClass::getLabel($fol->getTargetClass());
            $id = $fol->getTargetID();

            $this->following_hash[$type][$id] = $fol;
        }


        return $this->following_hash;
    }

    public function isFollowing($type, $id)
    {
        $hash = $this->getFollowingHash();

        if (isset($hash[$type][$id]) && $hash[$type][$id] != null)
            return true;

        return false;
    }
}
