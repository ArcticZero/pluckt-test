<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Entity\UserListVideo;

/**
 * @ORM\Entity
 */
class LogUserListVideo extends ActivityLog
{
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\AdminBundle\Entity\UserListVideo")
     * @ORM\JoinColumn(name="user_list_video_id", referencedColumnName="id")
     */
    protected $user_list_video;

    public function setUserListVideo(UserListVideo $ulv)
    {
        $this->user_list_video = $ulv;
        return $this;
    }

    public function getUserListVideo()
    {
        return $this->user_list_video;
    }

    public function isUserListVideo()
    {
        return true;
    }

    public function getLink()
    {
        return $this->user_list_video;
    }
}
