<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Pluckt\UserBundle\Entity\User;
use Pluckt\AdminBundle\Model\TimeAgo;

/**
 * @ORM\Entity
 * @ORM\Table(name="activity_log")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_id", type="string")
 * @ORM\DiscriminatorMap({
 *  "comment" = "LogComment",
 *  "review" = "LogReview",
 *  "follow" = "LogFollow",
 *  "userlistvideo" = "LogUserListVideo",
 *  "fnpage" = "LogFNPage"
 * })
 */
class ActivityLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    protected $target;

    public function __construct()
    {
        $this->date_create = new DateTime();
        $this->target = null;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function isComment()
    {
        return false;
    }

    public function isReview()
    {
        return false;
    }

    public function isFollow()
    {
        return false;
    }

    public function isUserListVideo()
    {
        return false;
    }

    public function isFNPage()
    {
        return false;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getDateElapsed()
    {
        return TimeAgo::get($this->date_create);
    }

    public function getLink()
    {
        return null;
    }

    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }
}
