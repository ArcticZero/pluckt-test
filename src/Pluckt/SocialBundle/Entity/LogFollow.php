<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LogFollow extends ActivityLog
{
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\SocialBundle\Entity\Follow")
     * @ORM\JoinColumn(name="follow_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $follow;

    public function setFollow(Follow $follow)
    {
        $this->follow = $follow;
        return $this;
    }

    public function getFollow()
    {
        return $this->follow;
    }

    public function isFollow()
    {
        return true;
    }

    public function getLink()
    {
        return $this->follow;
    }

}
