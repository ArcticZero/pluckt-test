<?php

namespace Pluckt\SocialBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Pluckt\UserBundle\Entity\User;

/**
 * @ORM\MappedSuperclass
 */
class BaseComment extends BaseSocial
{
    /** @ORM\Column(type="text") */
    protected $message;

    public function __construct()
    {
        parent::__construct();
        $this->message = '';
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
