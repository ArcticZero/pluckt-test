<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LogComment extends ActivityLog
{
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\SocialBundle\Entity\Comment")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id")
     */
    protected $comment;

    public function setComment(Comment $comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function isComment()
    {
        return true;
    }

    public function getLink()
    {
        return $this->comment;
    }
}
