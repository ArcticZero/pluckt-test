<?php

namespace Pluckt\SocialBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Pluckt\UserBundle\Entity\User;
use Pluckt\AdminBundle\Model\TimeAgo;

/**
 * @ORM\MappedSuperclass
 */
class BaseSocial
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    // target object's class and id, so we can retrieve it later
    /** @ORM\Column(type="string", length=120) */
    protected $target_class;

    /** @ORM\Column(type="string", length=100) */
    protected $target_id;

    protected $target;

    public function __construct()
    {
        $this->date_create = new DateTime();
        $this->target_class = null;
        $this->target_id = null;

        $this->target = null;
    }

    public function getID()
    {
        return $this->id;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getTimeElapsed()
    {
        return TimeAgo::get($this->date_create);
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setTargetClass($class)
    {
        $this->target_class = $class;
        return $this;
    }

    public function getTargetClass()
    {
        return $this->target_class;
    }

    public function setTargetID($id)
    {
        $this->target_id = $id;
        return $this;
    }

    public function getTargetID()
    {
        return $this->target_id;
    }

    // have to do this manually so we don't need to tie the entity manager to the entity
    public function setTarget($target)
    {
        $this->target = $target;
    }

    public function getTarget()
    {
        return $this->target;
    }
}
