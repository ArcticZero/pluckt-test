<?php

namespace Pluckt\SocialBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="review")
 */
class Review extends BaseComment
{
    /** @ORM\Column(type="string", length=100) */
    protected $title;

    /** @ORM\Column(type="integer") */
    protected $rating;

    public function __construct()
    {
        parent::__construct();
        $this->title = '';
        $this->rating = 0;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function getRating()
    {
        return $this->rating;
    }
}
