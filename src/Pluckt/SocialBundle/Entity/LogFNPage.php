<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Entity\FNPage;

/**
 * @ORM\Entity
 */
class LogFNPage extends ActivityLog
{
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\AdminBundle\Entity\FNPage")
     * @ORM\JoinColumn(name="fn_page_id", referencedColumnName="id")
     */
    protected $fn_page;

    public function setFNPage(FNPage $fn_page)
    {
        $this->fn_page = $fn_page;
        return $this;
    }

    public function getFNPage()
    {
        return $this->fn_page;
    }

    public function isFNPage()
    {
        return true;
    }

    public function getLink()
    {
        return $this->fn_page;
    }

}
