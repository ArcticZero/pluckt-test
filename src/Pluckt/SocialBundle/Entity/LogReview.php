<?php

namespace Pluckt\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LogReview extends ActivityLog
{
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\SocialBundle\Entity\Review")
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id")
     */
    protected $review;

    public function setReview(Review $review)
    {
        $this->review = $review;
        return $this;
    }

    public function getReview()
    {
        return $this->review;
    }

    public function isReview()
    {
        return true;
    }

    public function getLink()
    {
        return $this->review;
    }

}
