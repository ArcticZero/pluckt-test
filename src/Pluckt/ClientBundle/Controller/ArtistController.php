<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Model\Country;

class ArtistController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $artists = $em->getRepository('PlucktAdminBundle:Artist')->findAll();

        // featured artist
        $f_artist = $em->getRepository('PlucktAdminBundle:Artist')
            ->findOneBy(['featured' => true]);


        $params = [
            'artists' => $artists,
            'feat_artist' => $f_artist,
            'atypes' => ArtistType::getOptionsHash(),
            'countries' => Country::getOptionsHash(),
        ];

        return $this->render('PlucktClientBundle:Artist:index.html.twig', $params);
    }
}
