<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pluckt\AdminBundle\Model\Country;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pluckt\SocialBundle\Entity\Follow;
use Pluckt\SocialBundle\Entity\LogFollow;

class UserController extends Controller
{
    public function profileViewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('PlucktUserBundle:User')
            ->find($id);

        $me = $this->getUser();

        // update log targets
        $logs = $user->getActivityLog();
        $activity_log = [];
        foreach ($logs as $log)
        {
            $social = $log->getLink();
            $t_id = $social->getTargetID();
            $t_class = $social->getTargetClass();
            $target = $em->getRepository($t_class)->find($t_id);
            $log->setTarget($target);
            $activity_log[] = $log;
        }

        $params = [
            'user' => $user,
            'counter' => $this->get('pluckt.counter.social'),
            'activity_log' => $activity_log,
        ];

        if($me && $me->getId() == $user->getId()) {
            $params['countries'] = Country::getOptionsHash();
        }

        return $this->render('PlucktClientBundle:User:profile.html.twig', $params);
    }

    public function uploadAction($user_id, $upload_type)
    {
        // TODO: check permission before changing

        $em = $this->getDoctrine()->getManager();
        $um = $this->get('zulu_media');

        // get user
        $user = $em->getRepository('PlucktUserBundle:User')
            ->find($user_id);

        if ($user == null)
            return new JsonResponse([]);

        // handle upload
        $file = $this->getRequest()->files->get('file');
        $upload = $um->addFile($file);

        if ($upload == null)
            return new JsonResponse([]);

        // save to user profile
        if ($upload_type == 'cover')
        {
            $user->setPageCover($upload);
        }
        else if ($upload_type == 'profile')
        {
            $user->setProfilePic($upload);
        }
        else
            return new JsonResponse([]);

        // flush
        $em->flush();

        // return needed info
        $res = array(
            'id' => $upload->getID(),
            'filename' => $upload->getFilename(),
            'url' => $upload->getURL()
        );
        return new JsonResponse($res);
    }

    public function ajaxEditFieldAction($user_id, $field_type)
    {
        // TODO: check permission before changing

        $em = $this->getDoctrine()->getManager();
        $data = $this->getRequest()->request->all();

        // get user
        $user = $em->getRepository('PlucktUserBundle:User')
            ->find($user_id);


        // figure out what to edit
        switch ($field_type)
        {
            case 'name':
                $user->setName($data['first_name']);
                $user->setLastName($data['last_name']);
                break;
            case 'location':
                $user->setCity($data['city']);
                $user->setCountry($data['country']);
                break;
            case 'bio':
                if ($user->isArtist())
                    $user->getArtist()->setQuote($data['quote']);
                $user->setBio($data['bio']);
                break;
        }

        // flush
        $em->flush();

        return new JsonResponse([]);
    }

    protected function findEntity($id)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('PlucktUserBundle:User')
            ->find($id);
    }

    public function followAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findEntity($id);
        $user = $this->getUser();

        if ($entity == null)
            throw new \Exception('No entity found');

        // follow
        $follow = new Follow();
        $follow->setUser($user);

        $entity->addFollow($follow);

        // log it
        $log = new LogFollow();
        $log->setUser($user)
            ->setFollow($follow);

        // notifications
        // get users who are following this user
        $follows = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'target_class' => 'Pluckt\UserBundle\Entity\User',
                'target_id' => $id
            ]);
        // add notifications
        foreach ($follows as $fol)
        {
            $target_user = $fol->getUser();
            $target_user->addNotification($log);
        }


        // counter
        $counter = $this->get('pluckt.counter.social');
        $counter->add('follow-in', $entity)
            ->add('follow-out', $user);


        $em->persist($follow);
        $em->persist($log);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    public function unfollowAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $entity = $this->findEntity($id);

        // find follow
        $follow = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'user' => $this->getUser(),
                'target_class' => 'Pluckt\UserBundle\Entity\User',
                'target_id' => $id
            ]);

        // not found
        if ($follow == null)
            throw new \Exception('No follow found');

        // remove follow
        foreach ($follow as $fol)
            $em->remove($fol);

        // note: no log for unfollow

        // counter
        $counter = $this->get('pluckt.counter.social');
        $counter->remove('follow-in', $entity)
            ->remove('follow-out', $user);

        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    public function ordersAction()
    {
        $user = $this->getUser();
        $purchases = $user->getPurchases();

        $params = ['orders' => $purchases];

        return $this->render('PlucktClientBundle:User:orders.html.twig', $params);
    }
}
