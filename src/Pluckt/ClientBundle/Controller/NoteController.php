<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\Criteria;

use Pluckt\SocialBundle\Entity\Comment;
use Pluckt\SocialBundle\Entity\LogComment;

use Symfony\Component\HttpFoundation\JsonResponse;

class NoteController extends Controller
{
    public function indexAction($movie = false)
    {
        $em = $this->getDoctrine()->getManager();

        if ($movie)
        {
            $movie = $em->getRepository('PlucktAdminBundle:Video')
                ->find($movie);

            if ($movie)
            {
                $notes = $movie->getFieldNotes();

                $featured_criteria = Criteria::create()
                    ->where(Criteria::expr()->eq("featured", "1"))
                    ->orderBy(array("date_create" => Criteria::DESC))
                    ->setMaxResults(1);

                $featured = $notes->matching($featured_criteria);

                $plain_criteria = Criteria::create()
                    ->where(Criteria::expr()->eq("featured", "0"))
                    ->orderBy(array("date_create" => Criteria::DESC));

                $plain = $notes->matching($plain_criteria);
            }
            else
            {
                // TODO: if movie isn't found
                die('Movie not found!');
            }
        }
        else
        {
            $featured = $em->getRepository('PlucktAdminBundle:FNPage')
                ->findBy(
                    array('featured' => 1),
                    array('date_create' => 'DESC')
                );

            $plain = $em->getRepository('PlucktAdminBundle:FNPage')
                ->findBy(
                    array('featured' => 0),
                    array('date_create' => 'DESC')
                );
        }

        $featured_array = array();
        $plain_array = array();  

        foreach ($featured as $row)
            $featured_array[] = $row;

        foreach ($plain as $row)
            $plain_array[] = $row;

        $featured_note = !empty($featured_array) ? $featured_array[0] : false;

        if (!empty($featured_array))
            $featured_note = $featured_array[0];
        else if (!empty($plain_array))
            $featured_note = array_shift($plain_array);
        else
            $featured_note = false;

        $second_note = array_shift($plain_array);

        $params = [
            'title' => 'Field Notes',
            'notes' => $plain_array,
            'featured_note' => $featured_note,
            'second_note' => $second_note,
        ];

        return $this->render('PlucktClientBundle:Note:index.html.twig', $params);
    }

    public function readViewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $note = $this->findNote($id);

        // TODO: if note isn't found

        $params = [
            'note' => $note
        ];

        return $this->render('PlucktClientBundle:Note:read.html.twig', $params);
    }

    /*
    public function commentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $note = $this->findMovie($id);

        if ($note == null)
            throw new \Exception('No note found.');

        $data = $this->getRequest()->request->all();

        // new comment
        $comment = new Comment();
        $comment->setMessage($data['message'])
            ->setUser($this->getUser());

        $note->addComment($comment);

        // log it
        $log = new LogComment();
        $log->setUser($this->getUser())
            ->setComment($comment);

        $em->persist($comment);
        $em->persist($log);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    */
    protected function findNote($id)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('PlucktAdminBundle:FNPage')
            ->find($id);
    }
}
