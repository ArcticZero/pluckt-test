<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Model\Country;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $params = [];

        $em = $this->getDoctrine()->getEntityManager();

        $auth_checker = $this->get('security.authorization_checker');
        if ($auth_checker->isGranted('ROLE_USER'))
        {
            $notifications = [];
            $user = $this->getUser();
            $logs = $user->getNotifications();
            foreach ($logs as $log)
            {
                $social = $log->getLink();
                $t_id = $social->getTargetID();
                $t_class = $social->getTargetClass();
                $target = $em->getRepository($t_class)->find($t_id);
                $log->setTarget($target);
                $notifications[] = $log;
            }

            $params = [
                'notifications' => $notifications
            ];
            return $this->render('PlucktClientBundle:Default:index_auth.html.twig', $params);
        }

        return $this->render('PlucktClientBundle:Default:index.html.twig', $params);
    }

    public function indexAuthAction()
    {
        $params = [];

        return $this->render('PlucktClientBundle:Default:index_auth.html.twig', $params);
    }

    public function blankAction()
    {
        $params = [];

        return $this->render('PlucktClientBundle:Default:blank.html.twig', $params);
    }

    /*
    public function moviesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $genres = $em->getRepository('PlucktAdminBundle:VideoCategory')
            ->findBy([], ['name' => 'ASC']);

        $params = [
            'genres' => $genres
        ];

        return $this->render('PlucktClientBundle:Default:movies.html.twig', $params);
    }
    */

    /*
    public function artistsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $artists = $em->createQueryBuilder()
            ->select('u')
            ->from('PlucktUserBundle:User', 'u')
            ->where('u.artist_id IS NOT NULL')
            ->orderBy('u.last_name', 'ASC');

        $params = [
            'artists' => $artists,
            'countries' => Country::getOptionsHash()
        ];

        return $this->render('PlucktClientBundle:Default:artists.html.twig', $params);
    }
    */

    public function festivalsAction() {
        return $this->render('PlucktClientBundle:Default:festivals.html.twig');
    }

    public function staticPageAction($page)
    {
        switch ($page)
        {
            case 'terms':
                $title = 'Terms of Service';
                break;
            case 'privacy':
                $title = 'Privacy Policy';
                break;
            case 'corporate':
                $title = 'About pluckt media';
                break;
            case 'about':
                $title = 'What is ' . '<img class="inline" src="" title="Pluckt" alt="Pluckt">' . '?';
        }

        $params = [
            'page' => $page,
            'title' => $title
        ];

        return $this->render('PlucktClientBundle:Default:static/block.html.twig', $params);
    }
}
