<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Pluckt\SocialBundle\Entity\Comment;
use Pluckt\SocialBundle\Entity\Review;
use Pluckt\SocialBundle\Entity\Follow;
use Pluckt\SocialBundle\Entity\LogComment;
use Pluckt\SocialBundle\Entity\LogReview;
use Pluckt\SocialBundle\Entity\LogFollow;
use Pluckt\AdminBundle\Model\Country;

use Symfony\Component\HttpFoundation\JsonResponse;

class MovieController extends Controller
{
    protected function redirectReferer()
    {
        $source_url = $this->getRequest()->headers->get('referer');
        return $this->redirect($source_url);
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $counter = $this->get('pluckt.counter.social');

        // genres
        $genres = $em->getRepository('PlucktAdminBundle:VideoCategory')
            ->findBy([], ['name' => 'ASC']);

        // featured movies
        $f_movie = $em->getRepository('PlucktAdminBundle:Video')
            ->findOneBy(['featured' => true]);

        // curated lists
        $clists = $em->getRepository('PlucktAdminBundle:CuratedList')
            ->findBy(['flag_featured' => true]);
        $picks = $em->getRepository('PlucktAdminBundle:CuratedList')
            ->findBy(['flag_pick' => true]);

        // most talked
        $top_talks = $counter->getTop('talk', 'video', 'week', 1);
        if (!isset($top_talks[0]))
            $top_talk = null;
        else
            $top_talk = $em->getRepository('PlucktAdminBundle:Video')
                ->find($top_talks[0]);

        // field notes
        $notes = $em->getRepository('PlucktAdminBundle:FNPage')
                ->findAll();

        $notes_array = [];
        foreach ($notes as $note)
            $notes_array[] = $note;

        $featured = array_shift($notes_array);
        $second = array_shift($notes_array);

        $params = [
            'genres' => $genres,
            'feat_movie' => $f_movie,
            'clists' => $clists,
            'picks' => $picks,
            'counter' => $counter,
            'top_talk' => $top_talk,
            'featured_note' => $featured,
            'second_note' => $second
        ];

        return $this->render('PlucktClientBundle:Movie:index.html.twig', $params);
    }

    public function listViewAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();

        if($id)
        {
            $category = $em->getRepository('PlucktAdminBundle:VideoCategory')
                ->find($id);

            $videos = $category->getVideos();
        }
        else
        {
            $videos = $em->getRepository('PlucktAdminBundle:Video')->findAll();
        }

        $params = [
            'genre' => $category,
            'movies' => $videos,
            'countries' => Country::getOptionsHash(),
        ];

        return $this->render('PlucktClientBundle:Movie:list.html.twig', $params);
    }

    protected function getVideoActivityLog($id)
    {
        $em = $this->getDoctrine()->getManager();

        $logs = $em->getRepository('PlucktSocialBundle:ActivityLog')
            ->findBy(
                [],
                ['date_create' => 'desc']
            );

        $my_logs = [];
        $counter = 0;
        $limit = 10;
        foreach($logs as $l)
        {
            $t_class = $l->getLink()->getTargetClass();
            $t_id = $l->getlink()->getTargetID();
            if ($t_class == 'Pluckt\AdminBundle\Entity\Video' && $t_id == $id)
            {
                $target = $em->getRepository($t_class)->find($t_id);
                $l->setTarget($target);

                $my_logs[] = $l;
                $counter++;

                if ($counter == $limit)
                    return $my_logs;
            }
        }

        return $my_logs;
    }

    public function detailViewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $this->findMovie($id);

        $following = $movie->getFollows()->count();

        switch ($following)
        {
            case 0:
                $followText = 'Nobody is following this movie yet.<br><a href="' . $this->generateUrl('pluckt_client_movie_follow', ['id' => $movie->getID()]) . '" class="follow-link">Follow</a> this movie.';
                break;
            case 1:
                $followText = '<strong><em>' . $following . '</em></strong> person is following this movie.';
                break;
            default:
                $followText = $following . ' people are following this movie.';
        }

        // field notes
        $featured = $em->getRepository('PlucktAdminBundle:FNPage')
                ->findOneBy(
                    ['video' => $id],
                    ['date_create' => 'desc']
                );

        /*

        $notes_array = [];
        foreach ($notes as $note)
            $notes_array[] = $note;

        $featured = array_shift($notes_array);
        */

        // activities
        $logs = $this->getVideoActivityLog($id);


        // TODO: if movie isn't found

        $params = [
            'movie' => $movie,
            'following' => $followText,
            'featured_note' => $featured,
            'logs' => $logs,
        ];

        return $this->render('PlucktClientBundle:Movie:details.html.twig', $params);
    }

    public function playerViewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $this->findMovie($id);

        // Got access?
        if (!$this->getUser()->hasVideoAccess($movie->getID())) {
            return $this->redirect($this->generateUrl('pluckt_client_movie_details', array('id' => $movie->getID())), 301);
        }

        // TODO: if movie isn't found

        // field notes
        $notes = $em->getRepository('PlucktAdminBundle:FNPage')
                ->findAll();

        $notes_array = [];
        foreach ($notes as $note)
            $notes_array[] = $note;

        $featured = array_shift($notes_array);
        $second = array_shift($notes_array);

        // other movies by director
        if ($movie->getDirector())
        {
            $vq = $em->getRepository('PlucktAdminBundle:VideoArtistRole')->createQueryBuilder('va')
                    ->leftJoin('va.video', 'v')
                    ->leftJoin('va.artist', 'a')
                    ->where('a.id = :artist_id AND va.artist_type_id = :artist_type_id AND v.id != :video_id')
                    ->setParameter('artist_id', $movie->getDirector()->getID())
                    ->setParameter('artist_type_id', 1)
                    ->setParameter('video_id', $movie->getID())
                    ->getQuery();

            $video_ids = $vq->getResult();
        }
        else
        {
            $video_ids = [];
        }

        $other_vids = [];

        if (count($video_ids) > 0)
        {
            foreach ($video_ids as $vid) {
                $other_vids[] = $vid->getVideo();
            }
        }

        //die($vq->getSql());
        //die(count($video_ids) . "a");

        $params = [
            'movie' => $movie,
            'featured_note' => $featured,
            'second_note' => $second,
            'other_vids' => $other_vids
        ];

        return $this->render('PlucktClientBundle:Movie:player.html.twig', $params);
    }

    public function notesViewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $this->findMovie($id);

        // TODO: if movie isn't found

        $params = [
            'movie' => $movie
        ];

        return $this->render('PlucktClientBundle:Movie:notes.html.twig', $params);
    }

    public function commentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $this->findMovie($id);

        if ($movie == null)
            throw new \Exception('No movie found.');

        $data = $this->getRequest()->request->all();

        // new comment
        $comment = new Comment();
        $comment->setMessage($data['message'])
            ->setUser($this->getUser());

        $movie->addComment($comment);

        // log it
        $log = new LogComment();
        $log->setUser($this->getUser())
            ->setComment($comment);

        // counter
        $counter = $this->get('pluckt.counter.social');
        $counter->add('comment', $movie)
            ->add('talk', $movie);

        // notifications
        $this->addNotifications($log);

        $em->persist($comment);
        $em->persist($log);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    protected function addNotifications($log)
    {
        $em = $this->getDoctrine()->getManager();

        // get users who are following this user
        $follows = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'target_class' => 'Pluckt\UserBundle\Entity\User',
                'target_id' => $this->getUser()->getID()
            ]);
        // add notifications
        foreach ($follows as $fol)
        {
            $target_user = $fol->getUser();
            $target_user->addNotification($log);
        }
    }

    public function reviewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $this->findMovie($id);

        if ($movie == null)
            throw new \Exception('No movie found.');

        $data = $this->getRequest()->request->all();

        // new review
        $review = new Review();
        $review->setMessage($data['message'])
            ->setUser($this->getUser())
            ->setRating($data['rating'])
            ->setTitle($data['title']);

        $movie->addReview($review);

        // log it
        $log = new LogReview();
        $log->setUser($this->getUser())
            ->setReview($review);

        // counter
        $counter = $this->get('pluckt.counter.social');
        $counter->add('review', $movie)
            ->add('talk', $movie);

        // notifications
        $this->addNotifications($log);

        $em->persist($review);
        $em->persist($log);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    protected function findMovie($id)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('PlucktAdminBundle:Video')
            ->find($id);
    }

    protected function findEntity($id)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('PlucktAdminBundle:Video')
            ->find($id);
    }

    public function followAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findEntity($id);

        if ($entity == null)
            throw new \Exception('No entity found');

        // follow
        $follow = new Follow();
        $follow->setUser($this->getUser())
            ->setTargetClass('Pluckt\AdminBundle\Entity\Video')
            ->setTargetID($id);

        $entity->addFollow($follow);

        // log it
        $log = new LogFollow();
        $log->setUser($this->getUser())
            ->setFollow($follow);

        // notifications
        /*
        // get users who are following this movie
        $follows = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'target_class' => 'Pluckt\UserBundle\Entity\Video',
                'target_id' => $id
            ]);
        // add notifications
        foreach ($follows as $fol)
        {
            $target_user = $fol->getUser();
            $target_user->addNotification($log);
        }
        */


        /*
        // get users who are following this user
        $follows = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'target_class' => 'Pluckt\UserBundle\Entity\User',
                'target_id' => $this->getUser()->getID()
            ]);
        // add notifications
        foreach ($follows as $fol)
        {
            $target_user = $fol->getUser();
            $target_user->addNotification($log);
        }
        */

        // notifications
        $this->addNotifications($log);


        $em->persist($follow);
        $em->persist($log);
        $em->flush();

        return $this->redirectReferer();
    }

    public function unfollowAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        // find follow
        $follow = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'user' => $this->getUser(),
                'target_class' => 'Pluckt\AdminBundle\Entity\Video',
                'target_id' => $id
            ]);

        // not found
        if ($follow == null)
            throw new \Exception('No follow found');

        // remove follow
        foreach ($follow as $fol)
            $em->remove($fol);

        // note: no log for unfollow

        $em->flush();

        return $this->redirectReferer();
    }

    public function favoriteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $video = $this->findEntity($id);
        if ($video == null)
            throw new \Exception('No entity found');

        $user = $this->getUser();

        $user->addFavoriteVideo($video);

        $em->flush();

        return $this->redirectReferer();
    }

}
