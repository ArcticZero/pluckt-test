<?php

namespace Pluckt\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Pluckt\SocialBundle\Entity\Comment;
use Pluckt\SocialBundle\Entity\Review;

use Pluckt\AdminBundle\Entity\UserList;
use Pluckt\AdminBundle\Entity\UserListVideo;

use Symfony\Component\HttpFoundation\JsonResponse;
use Pluckt\SocialBundle\Entity\LogUserListVideo;

class UserListController extends Controller
{
    protected function redirectReferer()
    {
        $source_url = $this->getRequest()->headers->get('referer') . $this->getRequest()->request->get('hash');
        return $this->redirect($source_url);
    }

    public function addAction()
    {
        $user = $this->getUser();

        $data = $this->getRequest()->request->all();

        $em = $this->getDoctrine()->getManager();

        $list = new UserList();
        $list->setUser($user)
            ->setName($data['shelf_name']);

        $em->persist($list);
        $em->flush();

        return $this->redirectReferer();
    }

    public function addMovieAction($id, $movie_id)
    {
        // TODO: check if user owns the list

        $em = $this->getDoctrine()->getManager();

        // check if it's already saved in list
        $video_exist = $em->getRepository('PlucktAdminBundle:UserListVideo')
            ->findOneBy([
                'user_list' => $id,
                'video' => $movie_id
            ]);

        if ($video_exist != null)
            return $this->redirectReferer();
            

        // find list
        $list = $em->getRepository('PlucktAdminBundle:UserList')
            ->find($id);

        // find movie
        $movie = $em->getRepository('PlucktAdminBundle:Video')
            ->find($movie_id);

        // TODO: check if list exists
        // TODO: check if movie exists

        // ulv
        $video = new UserListVideo();
        $video->setVideo($movie)
            ->setUser($this->getUser())
            ->setList($list);

        // log
        $log = new LogUserListVideo();
        $log->setUserListVideo($video)
            ->setUser($this->getUser());

        // get users who are following this user
        $follows = $em->getRepository('PlucktSocialBundle:Follow')
            ->findBy([
                'target_class' => 'Pluckt\UserBundle\Entity\User',
                'target_id' => $this->getUser()->getID()
            ]);
        // add notifications
        foreach ($follows as $fol)
        {
            $target_user = $fol->getUser();
            $target_user->addNotification($log);
        }


        $em->persist($video);
        $em->persist($log);
        $em->flush();

        return $this->redirectReferer();
    }

    public function delMovieAction($id, $movie_id)
    {
        // TODO: check if user owns the list

        $em = $this->getDoctrine()->getManager();
        $response = new JsonResponse();

        // check if it's already saved in list
        $video = $em->getRepository('PlucktAdminBundle:UserListVideo')
            ->findOneBy([
                'user_list' => $id,
                'video' => $movie_id
            ]);

        if ($video == null)
        {
            $response->setData(array(
                'error' => 'The specified movie does not exist on this shelf.'
            ));
        }
        else
        {
            $em->remove($video);
            $em->flush();

            $response->setData(array(
                'success' => true
            ));
        }   

        return $response;
    }


    public function renameAction($id)
    {
        $data = $this->getRequest()->request->all();
        $em = $this->getDoctrine()->getManager();

        $ulist = $em->getRepository('PlucktAdminBundle:UserList')
            ->find($id);

        $ulist->setName($data['shelf_name']);

        $em->flush();

        return $this->redirectReferer();
    }

    public function getMoviesAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $ulist = $em->getRepository('PlucktAdminBundle:UserList')
            ->find($id);

        $contents = $ulist->getVideos();
        $list = [];
        $movies = [];

        $shelf = [
            'ID' => $ulist->getID(),
            'name' => $ulist->getName(),
            'totalVideos' => $ulist->getVideos()->count(),
            'runtime' => $ulist->getTotalMinutes(),
            'lastUpdated' => time(), # change to actual update date
        ];

        foreach($contents as $row) {
            $video = $row->getVideo();

            $movie = [
                'ID' => $video->getID(),
                'image' => $video->getPoster()->getURL(),
                'title' => $video->getTitle(),
                'synopsis' => $video->getDescription(),
                'director' => $video->getDirector()->getUser()->getFullName(),
                'country' => $video->getDirector()->getBaseCountry(),
                'classRating' => $video->getRatingID(),
                'year' => $video->getYearProduced(),
                'runtime' => $video->getMinutes(),
                'genre' => $video->getCategoriesText(),
                'rating' => $video->getAvgRating(),
                'notes' => $video->getFieldNotes()->count()
            ];

            $movies[] = $movie;
        }

        $response = new JsonResponse();
        $response->setData(array(
            'shelf' => $shelf,
            'movies' => $movies
        ));

        return $response;
    }
}
