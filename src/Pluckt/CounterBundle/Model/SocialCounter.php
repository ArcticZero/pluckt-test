<?php

namespace Pluckt\CounterBundle\Model;

use DateTime;
use Exception;
use Predis\Client as PredisClient;

class SocialCounter
{
    protected $redis;
    protected $prefix;

    public function __construct($redis = null, $prefix = 'pluckt')
    {
        if ($redis != null)
            $this->redis = $redis;
        else
        {
            $this->redis = new PredisClient();
        }

        $this->prefix = $prefix;
    }

/*
    public function setObjectType($otype)
    {
        $this->object_type = $otype;
        return $this;
    }

    // add
    public function addFollow($id, $count = 1)
    {
        return $this->add('follow', $id, $count);
    }

    public function addComment($id, $count = 1)
    {
        return $this->add('comment', $id, $count);
    }

    public function addReview($id, $count = 1)
    {
        return $this->add('review', $id, $count);
    }

    public function addShare($id, $count = 1)
    {
        return $this->add('share', $id, $count);
    }

    // remove
    public function removeFollow($id, $count = 1)
    {
        return $this->remove('follow', $id, $count);
    }

    public function removeComment($id, $count = 1)
    {
        return $this->remove('comment', $id, $count);
    }

    public function removeReview($id, $count = 1)
    {
        return $this->remove('review', $id, $count);
    }

    public function removeShare($id, $count = 1)
    {
        return $this->remove('share', $id, $count);
    }

    // top
    public function topFollow($period, $max = 5)
    {
        return $this->getTop('follow', $period, $max);
    }

    public function topComment($period, $max = 5)
    {
        return $this->getTop('comment', $period, $max);
    }

    public function topReview($period, $max = 5)
    {
        return $this->getTop('review', $period, $max);
    }

    public function topShare($period, $max = 5)
    {
        return $this->getTop('share', $period, $max);
    }

    // score
    public function scoreFollow($period, $id)
    {
        return $this->getScore('follow', $period, $id);
    }

    public function scoreComment($period, $id)
    {
        return $this->getScore('comment', $period, $id);
    }

    public function scoreReview($period, $id)
    {
        return $this->getScore('review', $period, $id);
    }

    public function scoreShare($period, $id)
    {
        return $this->getScore('share', $period, $id);
    }
*/


    protected function getPeriodID($period, DateTime $date)
    {
        switch ($period)
        {
            case 'year':
                return $date->format('Y');
            case 'month':
                return $date->format('Ym');
            case 'week':
                return $date->format('YW');
            case 'day':
                return $date->format('Ymd');
            case 'all':
                return 'all';
        }

        throw new Exception('Invalid period specified.');
    }

    protected function generateKey($prefix, $otype, $type, $period)
    {
        // period should be year, month, week, day or all

        // autogenerate period id based on current date/time
        $date = new DateTime();
        $period_id = $this->getPeriodID($period, $date);

        return $prefix . '-' . $otype . '-' . $type . '-' . $period . '-' . $period_id;
    }

    protected function getAllPeriods()
    {
        return ['year', 'month', 'week', 'day', 'all'];
    }

    protected function getObjectType($object)
    {
        $class = get_class($object);
        // error_log($class);
        switch ($class)
        {
            case 'Pluckt\UserBundle\Entity\User':
                return 'user';
            case 'Pluckt\AdminBundle\Entity\Video':
                return 'video';
        }

        return 'unknown';
    }

    public function add($type, $object, $count = 1)
    {
        $object_type = $this->getObjectType($object);
        $id = $object->getID();
        $periods = $this->getAllPeriods();
        foreach ($periods as $period)
        {
            $key = $this->generateKey($this->prefix, $object_type, $type, $period);
            $this->redis->zincrby($key, $count, $id);
        }

        return $this;
    }

    public function remove($type, $object, $count = 1)
    {
        $ncount = $count * -1;
        return $this->add($type, $object, $ncount);
    }

    public function getTop($type, $object_type, $period, $max)
    {
        $key = $this->generateKey($this->prefix, $object_type, $type, $period);
        return $this->redis->zrevrange($key, 0, $max);
    }

    public function getScore($type, $object, $period)
    {
        $object_type = $this->getObjectType($object);
        $id = $object->getID();
        $key = $this->generateKey($this->prefix, $object_type, $type, $period);
        $score = $this->redis->zscore($key, $id);

        if (!$score)
            return 0;

        return $score;
    }
}
