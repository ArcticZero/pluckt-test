<?php

namespace Pluckt\CounterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PlucktCounterBundle:Default:index.html.twig', array('name' => $name));
    }
}
