$(document).ready(function() {
	// countdown (test date only, my birthday)
	var releaseDate = new Date("Jan 01 2016 00:00:00");

	var html = '<div class="timer-days"><div class="time">{dnn}</div><div class="caption">Days</div></div><div class="timer-hours"><div class="time">{hnn}</div><div class="caption">Hours</div></div><div class="timer-minutes"><div class="time">{mnn}</div><div class="caption">Minutes</div></div><div class="timer-seconds"><div class="time">{snn}</div><div class="caption">Seconds</div></div>';

	$('.timer').countdown({
		until: releaseDate,
		layout: html
	});

	// genre slider
	$("#genres .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 210,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#genre-nav"),
		customDirectionNav: $("#genre-nav button")
	});

	// recommendations slider
	$("#recommendations .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 119,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#recommendations-nav"),
		customDirectionNav: $("#recommendations-nav button")
	});

	// click on recommendations slide
	$("#recommendations .flexslider li a").click(function(e) {
		var box = $("#recommendations-slide-info");
		var bigposter = $("#recommendations-poster");

		var info = $(this).siblings('.slide-info');
		var poster = $(this).find('img').attr('src');
		var title = $(this).find('img').attr('alt');

		$(this).closest('.flexslider').find('.slide').removeClass('active');
		$(this).closest('.slide').addClass('active');

		bigposter.attr('src', poster.replace('-sm', '-xl'));
		bigposter.attr('alt', title);

		box.html(info.html());
		e.preventDefault();
	});

	$("#recommendations .flexslider li:first-child > a").click();

	// staff picks slider
	$("#staff-picks .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 119,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#staff-picks-nav"),
		customDirectionNav: $("#staff-picks-nav button")
	});

	// click on staff picks slide
	$("#staff-picks .flexslider li a").click(function(e) {
		var box = $("#staff-picks-slide-info");
		var bigposter = $("#staff-picks-poster");
		var bigposterlink = $(bigposter).closest('a');

		var info = $(this).siblings('.slide-info');
		var poster = $(this).find('img').attr('src');
		var movieurl = $(this).find('img').closest('a').prop('href');
		var title = $(this).find('img').attr('alt');

		$(this).closest('.flexslider').find('.slide').removeClass('active');
		$(this).closest('.slide').addClass('active');

		bigposter.attr('src', poster.replace('-sm', '-xl'));
		bigposter.attr('alt', title);

		bigposterlink.prop('href', movieurl);

		box.html(info.html());
		e.preventDefault();
	});

	$("#staff-picks .flexslider li:first-child > a").click();
});