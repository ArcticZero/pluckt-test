$(document).ready(function() {
	// awards slider
	$("#awards .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 330,
		itemHeight: 130,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#awards-nav"),
		customDirectionNav: $("#awards-nav button")
	});

	// user activities button
	$("#btn-user-activities").click(function() {
		$('body').scrollTop(0);
		$(".content-menu a[href='#activities']").click();
	});
});
