$(document).ready(function() {
	// filters
	$(".list-filter").change(function() {
		filterList($(this));
	});

	$("#movie-name").keyup(function() {
		filterList($(this));
	});
});

function filterList(el) {
	var letterBlocks = $("#movies .shelf-row").find(".row-letter");
	var blocks = $("#movies .shelf-row").find(".shelf");
	var country = $("#movie-country").val();
	var name = $("#movie-name").val();
	var letters = [];

	$.each(blocks, function(index, block) {
		var currentCountry = $(block).find('.movie-flag').data('id');
		var currentName = $(block).find('.movie-name').html();

		if((currentCountry != country && country !== '') ||
			(currentName.toLowerCase().indexOf(name.toLowerCase()) == -1) && name.length > 2) {
			if($(this).is(":visible")) {
				$(this).hide();
			}
		} else {
			var firstLetter = currentName.slice(0, 1);

			if(!$(this).is(":visible")) {
				$(this).show();
			}
		
			if(letters.indexOf(firstLetter) == -1) {
				letters.push(firstLetter.toUpperCase());
			}	
		}
	});

	$.each(letterBlocks, function(index, block) {
		var letter = $(this).find('h3').html();

		if(letters.indexOf(letter) == -1) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}