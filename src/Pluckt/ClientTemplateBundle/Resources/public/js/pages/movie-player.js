function movie_player_init(uri) {
	//var uri = "net=00000012|trk=202.55.90.213:8476|ip=202.55.90.214:8476|sid=632dd06f87637da0|nom=name|brt=4000|typ=mp4|susr=iain01|spwd=iain01|";
	// var uri = "net=00000012|trk=202.55.90.213:8476|ip=202.55.90.214:8476|sid=632dd06f87637da0|nom=name|brt=4000|typ=mp4|";
    var xsize = 1170;
    var ysize = 658;
    var phtml = '';
    var hasPlayer = false;

    uri += "|nom=name|brt=4000|typ=mp4|";

    if(-1 != navigator.userAgent.toLowerCase().indexOf('trident')) { //indexOf("MSIE")){ // if we are using IE
		hasPlayer = true;
		//if(0){ //is_ie){
		// document.writeln is needed to get around the 'activex activation' patent troll nonsense that microsoft didn't want to pay $500M to licence
		phtml += '<object id="Tuner" width="' + xsize + '" height="' + ysize + '" classid="clsid:5474709A-83FB-4425-BCC7-56FECEDDE59D" codebase="http://streamer.cloudcast.sg/cloudcastactivex.cab">';
		phtml += '<param name="Uri" value="' + uri + '"> '; // the uri to tune, as a param
		phtml += '</object>';
		//document.write('<object id="Tuner" width="480" height="380" classid="clsid:5474709A-83FB-4425-BCC7-56FECEDDE59D" ');
		//document.write('codebase="cloudcastactivex.cab"> '); // #Version=-1,-1,-1,-1"> ');
		//document.write('<param name="Uri" value="g=00000012|k=184.105.216.55:8476|s=0000000000000010|n=DZRH%20TV|b=290|t=wmv|"> ');
		//document.write('</object> ');
	} else { // if we are not using IE. This presumes user is using firefox. Chrome is supposed to use the same plugins as firefox, not tested yet
		var found = detectPlugin("CloudCast");

		if(found == 1 || found == -1) { // found the plugin, or browser is so old that we can't check, in which case assume it's there, although it isn't likely to work
			hasPlayer = true;
			phtml += '<object id="Tuner" type="application/cloudcast-plugin" width="' + xsize + '" height="' + ysize + '" >';
			phtml += '</object>';
			// this is messy but needed, the uri must be passed by a function call, using param in object doesn't work on ff
			phtml += '<scr'+'ipt type="text/javascript">'; // nested self modifying script(!)
			phtml += 'Tuner.Uri="'+uri+'";';
			phtml += '</scr'+'ipt>'; // if we don't split this script end tag, it is seen as the end tag of the outer script
		}
	}

	if(hasPlayer) {
		$("#player").html(phtml);
	} else {
		$("#player").find('.install-player').removeClass('hide');
	}

	// recommendations slider
	$("#recommendations .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 119,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#recommendations-nav"),
		customDirectionNav: $("#recommendations-nav button")
	});

	// click on recommendations slide
	$("#recommendations .flexslider li a").click(function(e) {
		var box = $("#recommendations-slide-info");
		var bigposter = $("#recommendations-poster");

		var info = $(this).siblings('.slide-info');
		var poster = $(this).find('img').attr('src');
		var title = $(this).find('img').attr('alt');

		$(this).closest('.flexslider').find('.slide').removeClass('active');
		$(this).closest('.slide').addClass('active');

		bigposter.attr('src', poster.replace('-sm', '-xl'));
		bigposter.attr('alt', title);

		box.html(info.html());
		e.preventDefault();
	});

	// submit comment form
	$("#on-page-comment-form").submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var block = $.parseHTML($("#comment-model").html());
		var blockbox = $("#comments .review-blocks");
		var message = $(form).find('[name="message"]');

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                $(block).find('.portrait-md').prop('href', uinfo.profile_url);
                $(block).find('.portrait-md').find('img').prop('src', uinfo.pic_url).prop('title', uinfo.full_name).prop('alt', uinfo.full_name);
                $(block).find('.user-name').html(uinfo.full_name);
                $(block).find('.user-role').html(uinfo.role);
                $(block).find('.review-time').html("1s");
                $(block).find('.review-body p').html(message.val());

                if(uinfo.is_artist) {
                	$(block).find('.badge-star').removeClass('hide');
                }

                $(block).prependTo(blockbox);
                $(message).val("");

                $('html, body').stop().animate({
                    scrollTop: blockbox.offset().top - 80
                }, 1000, 'easeOutExpo');
            },
            error: function(data) {
                alert('Problem encountered while adding comment.');
            }
        });
	});

	// view all comments
	$("#btn-more-comments").click(function() {
		$("#comments .review-blocks .review").hide().removeClass('hide').fadeIn('fast');
		$(this).hide();
	});

	// expanding review box
	$('#on-page-comment-form textarea').expanding();

	$("#recommendations .flexslider li:first-child > a").click();
}

function detectPlugin(name) {
	if(navigator.plugins && navigator.plugins.length > 0) {
		for(i=0; i < navigator.plugins.length; i++) {  // for each plugin...
			if(navigator.plugins[i].name.indexOf(name) >= 0) { // just check the name, not the description
				return 1; // found
			}   
		}

		return 0;     // not found
	}

	return -1;      // can't even look
}
