$(document).ready(function() {
	// edit buttons
	$(".profile-line").hover(function() {
		$(this).find('.profile-controls').css('visibility', 'visible');
	}, function() {
		$(this).find('.profile-controls').css('visibility', 'hidden');
	});

	// popovers
	$("#name-btn").popover({
		html: true,
		placement: 'top',
		content: $("#popover-name").html(),
		trigger: 'manual'
	}).click(function(e) {
		var fields = $(this).popover('show').parent().find('input, select');

		e.preventDefault();
		fields.first().focus();
		
		$.each(fields, function(index, field) {
			$(field).val(pdata[$(field).attr('name')]);
		});
	});

	$("#location-btn").popover({
		html: true,
		placement: 'top',
		content: $("#popover-location").html(),
		title: 'Select your location',
		trigger: 'manual'
	}).click(function(e) {
		var fields = $(this).popover('show').parent().find('input, select');

		e.preventDefault();
		fields.first().focus();
		
		$.each(fields, function(index, field) {
			$(field).val(pdata[$(field).attr('name')]);
		});
	});

	$("#bio-btn").popover({
		html: true,
		placement: 'left',
		content: $("#popover-bio").html(),
		trigger: 'manual'
	}).click(function(e) {
		var fields = $(this).popover('show').parent().find('textarea');

		e.preventDefault();
		fields.first().focus();
		
		$.each(fields, function(index, field) {
			$(field).val(pdata[$(field).attr('name')]);
		});

		fields.expanding();
	});

	$(".shelf-name-btn").popover({
		html: true,
		placement: 'top',
		content: $("#popover-shelf-name").html(),
		trigger: 'manual'
	}).click(function(e) {
		var parent = $(this).popover('show').parent();
		var fields = parent.find('input');
		var form = parent.find('form');

		e.preventDefault();
		form.attr('action', $(this).data('form-action'));
		fields.first().focus().val($(this).data('value'));
	});

	// close popovers
	$(document).on("click", ".btn-close-popover", function() {
		$(this).closest('.profile-line').find('.profile-controls').popover('hide');
	});

	// save inline edits
	$(document).on('submit', ".inline-edit", function(e) {
		var form = $(this);
		var name = $(this).data('field');
		var separator = $(this).data('separator');
		var fields = $(this).find('input, select, textarea');
		var box = $("#user-" + name);
		var btn = $(this).find(".action-button");

		var bgc = btn.css('background-color');
		var fgc1 = box.css('color');
		var fgc2 = btn.css('color');

		var alert = $(this).find('.alert');

		e.preventDefault();

		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					alert.removeClass('hide').show().find('.errors').html(error);
				} else {
					var values = [];

					$.each(fields, function(index, field) {
						var fn = $(field).prop('name');
						var fv = $(field).val();

						if(separator !== "") {
							values.push($(field).prop("tagName") == 'SELECT' ? $(field).find('option:selected').text() : fv);

							box.css('background-color', bgc).css('color', fgc2).animate({ backgroundColor: 'transparent', color: fgc1 }, 700);
						} else {
							box = $("#user-" + fn);
							fgc1 = box.css('color');
							box.html(fv).css('background-color', bgc).css('color', fgc2).animate({ backgroundColor: 'transparent', color: fgc1 }, 700);
						}

						pdata[fn] = fv;

						if(fn == 'country') {
							$("#user-flag").attr('src', flag_path + fv + '.png');
						}
					});

					if(separator !== "") {
						box.html(values.join(separator));
					}

					$(this).closest('.profile-line').find('.profile-controls').popover('hide');
					alert.hide();
				}
			}
		});

		// debug code below
		var values = [];

		console.log(box.css('color'));

		$.each(fields, function(index, field) {
			var fn = $(field).prop('name');
			var fv = $(field).val();

			if(separator !== "") {
				values.push($(field).prop("tagName") == 'SELECT' ? $(field).find('option:selected').text() : fv);

				box.css('background-color', bgc).css('color', fgc2).animate({ backgroundColor: 'transparent', color: fgc1 }, 700);
			} else {
				box = $("#user-" + fn);
				fgc1 = box.css('color');
				box.html(fv).css('background-color', bgc).css('color', fgc2).animate({ backgroundColor: 'transparent', color: fgc1 }, 700);
			}

			pdata[fn] = fv;

			if(fn == 'country') {
				$("#user-flag").attr('src', flag_path + fv + '.png');
			}
		});

		if(separator !== "") {
			box.html(values.join(separator));
		}
		
		// end debug code

		$(this).closest('.profile-line').find('.profile-controls').popover('hide');
	});

	// uploads
	uploadBox("#profile-pic");
	uploadBox("#cover-photo", true, true);
});

function uploadBox(dropzone, is_box, is_bg) {
	var btn = dropzone + "-btn";
	var bar = dropzone + "-progress";

	if(typeof box === "undefined") {
		box = dropzone + "-box";
	} else {
		box = dropzone;
	}

    if(typeof is_bg === "undefined") {
        upload_path = upload_profile_path;
    } else {
        upload_path = upload_cover_path;
    }

	$(dropzone).dropzone({
		url: upload_path,
		maxFilesize: 2,
		previewTemplate: $(box).html(),
		thumbnailWidth: 270,
		thumbnailHeight: 270,
		maxFiles: 1,
		clickable: btn,
		acceptedFiles: 'image/*',
		init: function() {
			var dz = this;

            this.on('success', function(file, res) {
            	if(typeof is_bg === "undefined") {
            		$(dropzone).attr('src', res.url);
            	} else {
            		$(dropzone).css('background-image', "url('" + res.url + "')");
            	}

            	dz.removeFile(file);
            });

            this.on('uploadprogress', function(file, progress) {
            	$(box).find("[data-dz-uploadprogress]").css('width', progress + "%");
            });

            this.on('sending', function(file) {
            	$(bar).css('visibility', 'visible');
            });

            this.on('complete', function(file) {
            	$(bar).css('visibility', 'hidden');
            });
        }
	});
}
