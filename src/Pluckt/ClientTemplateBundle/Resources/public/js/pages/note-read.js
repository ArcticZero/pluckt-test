$(document).ready(function() {
	// thumbnail slider
	$("#thumbs .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 100,
		itemMargin: 15,
		controlNav: false,
		controlsContainer: $("#thumbs-nav"),
		customDirectionNav: $("#thumbs-nav button"),
		asNavFor: '#main-slider-image .flexslider'
	});

	// main image slider
	$('#main-slider-image .flexslider').flexslider({
	    animation: "slide",
	    animationLoop: false,
	    controlNav: false,
	    directionNav: false,
	    slideshow: false,
	    sync: '#thumbs .flexslider'
	});

	// click on thumbnail
	$("#thumbs").on('click', 'a', function() {
		console.log('test');
		$("#thumbs").find('li').removeClass('active');
		$(this).closest('li').addClass('active');
	});

	// expanding review box
	$('#note-comment-form textarea').expanding();
});