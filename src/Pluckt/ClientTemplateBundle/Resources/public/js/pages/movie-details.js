$(document).ready(function() {
	// thumbnail slider
	$("#thumbs .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 160,
		itemMargin: 15,
		controlNav: false,
		controlsContainer: $("#thumbs-nav"),
		customDirectionNav: $("#thumbs-nav button")
	});

	// view full photos
	$('.gallery-item').venobox();

	// awards slider
	$("#awards .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 424,
		itemHeight: 150,
		itemMargin: 0,
		controlNav: false,
		controlsContainer: $("#awards-nav"),
		customDirectionNav: $("#awards-nav button")
	});

	// cast slider
	$("#cast .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 180,
		itemMargin: 0,
		controlNav: false,
		controlsContainer: $("#cast-nav"),
		customDirectionNav: $("#cast-nav button")
	});

	// crew slider
	$("#crew .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 180,
		itemMargin: 0,
		controlNav: false,
		controlsContainer: $("#crew-nav"),
		customDirectionNav: $("#crew-nav button")
	});

	// recommendations slider
	$("#recommendations .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 119,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#recommendations-nav"),
		customDirectionNav: $("#recommendations-nav button")
	});

	// click on recommendations slide
	$("#recommendations .flexslider li a").click(function(e) {
		var box = $("#recommendations-slide-info");
		var bigposter = $("#recommendations-poster");

		var info = $(this).siblings('.slide-info');
		var poster = $(this).find('img').attr('src');
		var title = $(this).find('img').attr('alt');

		$(this).closest('.flexslider').find('.slide').removeClass('active');
		$(this).closest('.slide').addClass('active');

		bigposter.attr('src', poster.replace('-sm', '-xl'));
		bigposter.attr('alt', title);

		box.html(info.html());
		e.preventDefault();
	});

	// curator ratings histogram
	var ctoprating = 0;

	$("#curator-ratings .bar").each(function() {
		var tally = $(this).data('tally');
		ctoprating = tally > ctoprating ? tally : ctoprating;
	});

	$("#curator-ratings .bar").each(function() {
		var tally = $(this).data('tally');
		$(this).animate({ width: ((tally / ctoprating) * 100).toString() + "%" }, 1000).find('span').html(tally.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	});

	// user ratings histogram
	var utoprating = 0;

	$("#user-ratings .bar").each(function() {
		var tally = $(this).data('tally');
		utoprating = tally > utoprating ? tally : utoprating;
	});

	$("#user-ratings .bar").each(function() {
		var tally = $(this).data('tally');
		$(this).animate({ width: ((tally / utoprating) * 100).toString() + "%" }, 1000).find('span').html(tally.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	});

	// submit review form
	$("#on-page-review-form").submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var block = $.parseHTML($("#review-model").html());
		var blockbox = $("#reviews .review-blocks");
		var title = $(form).find('[name="title"]');
		var message = $(form).find('[name="message"]');
		var rating = $(form).find('[type="radio"]:checked');

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                $(block).find('.portrait-md').prop('href', uinfo.profile_url);
                $(block).find('.portrait-md').find('img').prop('src', uinfo.pic_url).prop('title', uinfo.full_name).prop('alt', uinfo.full_name);
                $(block).find('.user-name').html(uinfo.full_name);
                $(block).find('.user-role').html(uinfo.role);
                $(block).find('.review-time').html("1s");
                $(block).find('.review-title').html(title.val());
                $(block).find('.review-body p').html(message.val());
                $(block).find('[type="radio"][value="' + rating.val() + '"]').prop('checked', true);
                $(block).find('[type="radio"]').prop('name', "review-item-" + Date.now());

                if(uinfo.is_artist) {
                	$(block).find('.badge-star').removeClass('hide');
                }

                $(block).prependTo(blockbox);
                $(title).val("");
                $(message).val("");
                $(rating).prop('checked', false);

                $('html, body').stop().animate({
                    scrollTop: blockbox.offset().top - 80
                }, 1000, 'easeOutExpo');
            },
            error: function(data) {
                alert('Problem encountered while adding review.');
            }
        });
	});

	// view all reviews
	$("#btn-more-reviews").click(function() {
		$("#reviews .review-blocks .review").hide().removeClass('hide').fadeIn('fast');
		$(this).hide();
	});

	// product link details
	$(".product-link").click(function() {
		verb = $(this).data('verb');

		$("#payment-action").html(verb);
		$("#payment-price").html('$' + $(this).data('price') + " USD");
		$("#payment-form").prop('action', purchase_paths[verb]);
	});

	// submit payment form
	$("#payment-form").submit(function() {
		if(confirm("Are you sure you want to make this purchase?")) {
			$.post($(this).prop('action'), function(response) {
				if (response.status == true) {
					alert('Your purchase has been made succesfully!');
					location.reload();
				}
			});
		}

		return false;
	});

	// expanding review box
	$('#on-page-review-form textarea').expanding();

	$("#recommendations .flexslider li:first-child > a").click();

	// view more modal
    $('.btn-view-more').click(function() {
    	var block;
    	var container = $("#modal-user-blocks");

    	container.html("");

    	$('#view-all-users-title').html($(this).data('title'));

    	$(followers).each(function(index, user) {
    		block = $('#modal-user-block-template').clone();

    		block.prop('id', "").removeClass('hide');
    		block.find('.user-profile-link').prop('href', user.page);
    		block.find('.portrait-box img').prop('src', user.portrait).prop('title', user.fullName).prop('alt', user.fullName);
    		block.find('.user-name').html(user.fullName);
    		block.find('.user-role').html(user.role);
    		block.find('.user-location').html(user.location + (user.country ? ' <img src="' + user.flag + '" alt="' + user.country + '" title="' + user.country + '">' : ''));

    		if (user.isArtist == 1) {
    			block.find('.portrait-box').append('<div class="badge-star"></div>');
    		}

    		container.append(block);
    		//console.log(container.html());
    	});
    });
});