var initial = true;

$(document).ready(function() {
    // display shelf contents
    $(document).on('click', '.covers .grid-item', function(e) {
        var shelf = $(this).closest('.shelf');
        var path = $(this).data('contents-path');
        var shelfDetails = $("#shelf-details");
        var shelfContents = $("#shelf-contents");
        var html = $('<div />');

        e.preventDefault();

        if(shelf.hasClass('active')) return;

        $("#shelf-list").find('.shelf').removeClass('active');
        shelf.addClass('active');

        $.post(path, function(response) {
            shelfDetails.find('.shelf-title').html('<h3>' + response.shelf.name + '</h3>');
            shelfDetails.data('id', response.shelf.ID);
            $("#shelf-video-count").html(response.shelf.totalVideos + " Movie" + (response.shelf.totalVideos != 1 ? 's' : ''));
            $("#shelf-total-runtime").html(response.shelf.runtime + " min");
            //$("#shelf-last-updated").html(response.shelf.lastUpdated);

            if(response.movies.length > 0) {
                $.each(response.movies, function(index, movie) {
                    var poster = $.parseHTML($("#poster-model").html());
                    
                    $(poster).find('.movie-poster').attr('data-id', movie.ID);
                    $(poster).find('.poster-image').attr('src', movie.image);
                    $(poster).find('.movie-description').html(movie.synopsis);
                    $(poster).find('.movie-play-path').attr('href', play_path.replace('/0/', '/' + movie.ID + '/'));
                    $(poster).find('.movie-title').html(movie.title);
                    $(poster).find('.user-name').html(movie.director);
                    $(poster).find('.movie-flag').attr('src', $(poster).find('.movie-flag').attr('src').replace('PH.png', movie.country + '.png'));
                    $(poster).find('.class-rating').html(movie.classRating);
                    $(poster).find('.year').html(movie.year);
                    $(poster).find('.runtime').html(movie.runtime + " min");
                    $(poster).find('.genre').html(movie.genre);
                    $(poster).find('[type="radio"][value="' + Math.floor(parseFloat(movie.rating)) + '"]').attr('checked', true);
                    $(poster).find('[type="radio"]').attr('name', "shelf-item-" + movie.ID);
                    $(poster).find('.movie-note-count').html(movie.notes + " Note" + (movie.notes.count != 1 ? 's' : ''));
                    $(poster).find('.movie-info-path').attr('href', movie_path.replace('/0/', '/' + movie.ID + '/'))

                    html.append(poster);
                });

                shelfContents.html(html[0].outerHTML);
                shelfDetails.removeClass('hide');

                if(!initial) {
                    $('html, body').stop().animate({
                        scrollTop: shelfDetails.offset().top - 80
                    }, 1000, 'easeOutExpo');
                } else {
                    initial = false;
                }
                
            } else {
                shelfContents.html("");
                shelfDetails.addClass('hide');
            }
        });
    });

    // remove from shelf
    $(document).on('click', '.remove-from-shelf', function(e) {
        var movie = $(this).closest('.movie-poster');
        var shelfDetails = $("#shelf-details");

        var movieID = movie.data('id');
        var movieName = movie.find('.movie-name').html();
        var shelfID = shelfDetails.data('id');
        var shelfName = shelfDetails.find('.shelf-title h3').html();

        var shelf = $("#shelf-list").find('.shelf[data-id="' + shelfID + '"]');
        var covers = shelf.find('.cover-stack');

        var runtime = parseInt(movie.find('.runtime').html().replace(' min', ''));
        var totalVideosHtml = $("#shelf-video-count").html();
        var totalVideos = parseInt(totalVideosHtml.substring(0, totalVideosHtml.indexOf(' ')));
        var totalRuntimeHtml = $("#shelf-total-runtime").html();
        var totalRuntime = parseInt(totalRuntimeHtml.replace(' min', ''));
        var newTotalRuntime = totalRuntime - runtime;

        e.preventDefault();

        if(confirm('Remove ' + movieName + ' from the shelf "' + shelfName + '"?')) {
            var path = del_path.replace(0, shelfID).replace(1, movieID);

            $.post(path, function(response) {
                if(response.error) {
                    alert(response.error);
                } else {
                    movie.parent().fadeOut('fast', function() {
                        $(this).parent().remove();

                        totalVideos--;

                        shelf.find('img[data-id="' + movieID + '"]').fadeOut('fast', function() {
                            $(this).remove();
                        });

                        if(covers.find('img').length == 2) {
                            covers.addClass('v2');
                        } else {
                            covers.removeClass('v2');
                        }

                        if(totalVideos > 0) {
                            $("#shelf-video-count").html(totalVideos + " Movie" + (totalVideos != 1 ? 's' : ''));
                            $("#shelf-total-runtime").html(newTotalRuntime + " min");
                        } else {
                            $("#shelf-details").addClass('hide');
                            covers.html('<div class="shelf-placeholder">This shelf is empty</div>');
                            console.log(covers.html());
                        }
                    });
                }
            });
        }
    });
});

function slideShelves() {
	// shelves slider
	$("#shelf-list .flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 262,
		itemHeight: 193,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#shelves-nav"),
		customDirectionNav: $("#shelves-nav button"),
        slideshow: false,
        start: function() {
            $(document).find('#shelf-list .grid-item:first').trigger('click');
        }
	});
}
