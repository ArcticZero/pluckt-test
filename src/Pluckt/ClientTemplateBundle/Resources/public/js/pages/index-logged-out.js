$(document).ready(function() {
	// movie slider
	$("#movie-slider .flexslider").flexslider({
		animation: "slide",
		controlNav: false,
		controlsContainer: $("#movie-slider-nav"),
		customDirectionNav: $("#movie-slider-nav a"),
		slideshowSpeed: 5000,
		video: true,
		start: function() {
			$('.flex-active-slide').find('video').get(0).play();
		},
		after: function() {
			$('.flex-active-slide').find('video').get(0).play();
		}
	});

	// page down arrow
	$("#page-down").click(function(e) {
		$('html, body').stop().animate({
	        scrollTop: $("#features").offset().top
	    }, 1000, 'easeInOutExpo');

		e.preventDefault();
	});

	// datepicker boxes
	$("#signup-birthdate").combodate({
		minYear: 1900,
		maxYear: moment().format('YYYY'),
		format: "YYYY-MM-DD",
		template: "YYYY MMMM D",
		customClass: 'form-control round inline',
		smartDays: true
	});

	// hide login box when showing forgot password
	$("#forgot-password").on('show.bs.modal', function() {
		$("#login").modal('hide');
	});

    /*
	// submit modal form
	$(".modal form").submit(function(e) {
		var form = $(this);
  		var loading = form.closest(".modal-content").find(".loading-pane");
  		var notice = form.find(".form-notice");
  		var noreload = form.data('noreload');

  		// stop form from submitting
  		e.preventDefault();

  		// show loader
  		loading.show();

  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', notice);
				} else if(noreload) {
					status(response.title, response.body, 'alert-success');
				} else {
					location.reload();
				}

				loading.addClass('hide');
			}
  		});
	});
    */

    // init scrollmagic
    var sm = new ScrollMagic.Controller();

    // initial css
    $(".block-bg").css('top', '-100%');

    // quote animations
    for(i = 1; i <= 4; i++) {
    	var uq = '#user-quote-' + i;
    	var qt = TweenMax.fromTo($(uq), 0.50, { css: { opacity: 0, y: -50, force3D: true }, immediateRender: true, ease: Quad.easeInOut }, { css: { opacity: 1, y: 0 }, ease: Quad.easeInOut });
    	
    	new ScrollMagic.Scene({
    		offset: (100 * i) - 100,
			triggerElement: "#quotes"
		})
		.setTween(qt)
		.addTo(sm);
    }

    // feature animations
    for(i = 1; i <= 5; i++) {
    	var feat = "#features-img-" + i;
    	var ex = $(feat).data('x');
    	var ey = $(feat).data('y');
    	var ft = TweenMax.fromTo($(feat), 0.50, { css: { opacity: 0, x: -50, y: 0, force3D: true }, immediateRender: true }, { css: { opacity: 1, x: ex, y: ey } });

    	new ScrollMagic.Scene({
    		offset: 200,
			triggerElement: "#curators"
		})
		.setTween(ft)
		.addTo(sm);
    }

    // cloudcast animations
    for(i = 1; i <= 2; i++) {
    	var pane = "#cloudcast-pane-" + i;
    	var px = $(pane).data('x');
    	var ct = TweenMax.fromTo($(pane), 0.50, { css: { opacity: 0, x: px, force3D: true }, immediateRender: true }, { css: { opacity: 1, x: "0%" } });

    	new ScrollMagic.Scene({
    		offset: 250,
			triggerElement: "#cloudcast"
		})
		.setTween(ct)
		.addTo(sm);
    }

    // pin backgrounds
    new ScrollMagic.Scene({
    		triggerElement: "#curators",
    		duration: $(window).height() * 2,
    		offset: ($(window).height() / 2) * -1
    	})
		.setTween("#curators > .block-bg", {top: "100%", ease: Linear.easeNone})
		.addTo(sm);

	new ScrollMagic.Scene({
    		triggerElement: "#cloudcast",
    		duration: $(window).height() * 2,
    		offset: ($(window).height() / 2) * -1
    	})
		.setTween("#cloudcast > .block-bg", {top: "100%", ease: Linear.easeNone})
		.addTo(sm);
});
