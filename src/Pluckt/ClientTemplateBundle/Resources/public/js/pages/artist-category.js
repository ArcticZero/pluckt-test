$(document).ready(function() {
	// grid view
	$("#btn-grid-view").click(function() {
		if(!$(this).hasClass('active')) {
			$(this).addClass('active');
			$("#btn-list-view").removeClass('active');
			$("#artists").find('.person-row').fadeOut('fast').switchClass('list-view', 'grid-view', { duration: 0, complete: function() { $(this).fadeIn('fast'); } });
		}
	});

	// list view
	$("#btn-list-view").click(function() {
		if(!$(this).hasClass('active')) {
			$(this).addClass('active');
			$("#btn-grid-view").removeClass('active');
			$("#artists").find('.person-row').fadeOut('fast').switchClass('grid-view', 'list-view', { duration: 0, complete: function() { $(this).fadeIn('fast'); } });
		}
	});

	// filters
	$(".list-filter").change(function() {
		filterList($(this));
	});

	$("#artist-name").keyup(function() {
		filterList($(this));
	});
});

function filterList(el) {
	var letterBlocks = $("#artists .person-row").find(".row-letter");
	var blocks = $("#artists .person-row").find(".person-column");
	var type = $("#artist-type").val();
	var country = $("#artist-country").val();
	var name = $("#artist-name").val();
	var letters = [];

	$.each(blocks, function(index, block) {
		var currentTypes = ($(block).find('.user-role').data('ids') + '').split(",");
		var currentCountry = $(block).find('.user-flag').data('id');
		var currentName = $(block).find('.user-name').html();
		var currentLastName = $(block).find('.user-name').data('last-name');

		if((currentTypes.indexOf(type) == -1 && type !== '') ||
			(currentCountry != country && country !== '') ||
			(currentName.toLowerCase().indexOf(name.toLowerCase()) == -1) && name.length > 2) {
			if($(this).is(":visible")) {
				$(this).hide();
			}
		} else {
			var firstLetter = currentLastName.slice(0, 1);

			if(!$(this).is(":visible")) {
				$(this).show();
			}
		
			if(letters.indexOf(firstLetter) == -1) {
				letters.push(firstLetter.toUpperCase());
			}	
		}
	});

	$.each(letterBlocks, function(index, block) {
		var letter = $(this).find('h3').html();

		if(letters.indexOf(letter) == -1) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}