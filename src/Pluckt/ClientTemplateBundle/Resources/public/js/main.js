$(document).ready(function() {
	// follow
    $('.btn-follow').click(function() {
        var button = $(this);
        var uid = button.data('uid');

        $.ajax({
            url: $(this).data('url'),
            type: 'get',
            dataType: 'json',
            success: function(data) {
                button.addClass('hidden');
                button.siblings('.btn-unfollow').removeClass('hidden');
                $(".btn-follow[data-uid=" + uid + "]").addClass('hidden');
                $(".btn-unfollow[data-uid=" + uid + "]").removeClass('hidden');
            },
            error: function(data) {
            }
        });
        return false;
    });

    // unfollow
    $('.btn-unfollow').click(function() {
        var button = $(this);
        var uid = button.data('uid');

        $.ajax({
            url: $(this).data('url'),
            type: 'get',
            dataType: 'json',
            success: function(data) {
                button.addClass('hidden');
                button.siblings('.btn-follow').removeClass('hidden');
                $(".btn-unfollow[data-uid=" + uid + "]").addClass('hidden');
                $(".btn-follow[data-uid=" + uid + "]").removeClass('hidden');
            },
            error: function(data) {
            }
        });
        return false;
    });

	// toggle search box
	$("#btn-search").click(function(e) {
		var querybox = $(this).siblings("input[name='q']");
		var hname = $("#header-user-name");

		if($(window).width() > 320) {
			if(querybox.hasClass('squeeze')) {
				hname.hide();
				querybox.attr('readonly', false).removeClass('squeeze').focus();
				$(this).addClass('active');
				e.preventDefault();
			} else {
				if(querybox.val().length === 0) {
					querybox.attr('readonly', true).blur().addClass('squeeze');
					hname.delay(150).fadeIn();

					$(this).removeClass('active');
					e.preventDefault();
				}
			}
		}
	});

	// hide input placeholders on focus
	$("input[type], textarea").focus(function() {
		var ph = $(this).attr('placeholder');
		$(this).data('placeholder', ph).attr('placeholder', "");
	}).blur(function() {
		var ph = $(this).data('placeholder');
		$(this).attr('placeholder', ph).data('placeholder', "");
	});

	// handle enter key for comment box input (shift+enter for new line)
	$('.comment-box form').keyup(function(e) {
	    if(e.keyCode == 13 && !e.shiftKey) {
	    	$(this).submit();
	    }
	});

	// expanding textareas for comment boxes
	$('.comment-box textarea:visible').expanding();

	// content menu in-page links
	$(".content-menu a").click(function(e) {
		var page = $(this).attr('href') + '-page';
		var li = $(this).closest('li');
		var callback = $(this).data('callback');

		if(!li.hasClass('active')) {
			$('body').scrollTop(0);
			$(".content-menu li").removeClass('active');
			li.addClass('active');

			$(".page").fadeOut('fast').addClass('hide');
			$(page).hide().removeClass('hide').fadeIn('fast');

			if(callback) {
				$.globalEval(callback);
			}

			window.location.hash = $(this).attr('href');

			$(page).find('.comment-box textarea:visible').expanding();
		}

		e.preventDefault();
	});

	// hover on shelf/grid movie poster
	$(document).on("mouseover", ".movie-poster .poster, .movie-poster .overlay-white", function() {
		var box = $(this).closest('.movie-poster');
		var overlay = box.find('.overlay-white');
		box.addClass('active');
		overlay.stop().fadeIn(250);
	}).on("mouseleave", ".movie-poster .poster, .movie-poster .overlay-white", function() {
		var box = $(this).closest('.movie-poster');
		var overlay = box.find('.overlay-white');
		box.removeClass('active');
		overlay.stop().fadeOut(250);
	});

	// tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// display status message
	function status(title, body, type, box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	}

	// hash handler
    if(window.location.hash) {
        var hash = window.location.hash;
        var modal = $(".modal" + hash);
        var link = $(".content-menu a[href='" + hash + "']");
        
        if(modal.length) {
        	$(hash).modal('toggle');
        } else if(link.length) {
        	link.click();
        }
    }

    // movie comment and review
    $('.social-link').click(function() {
        $('.social-image').attr('src', $(this).data('image'));
        $('.social-image').attr('alt', $(this).data('title'));

        $('.social-title').html($(this).data('title'));
        $('.social-name').html($(this).data('name'));

        $('.social-flag').attr('src', $(this).data('flag-image'));
        $('.social-flag').attr('alt', $(this).data('country'));
        $('.social-flag').attr('title', $(this).data('country'));

        $('#comment-form').attr('action', $(this).data('comment-action'));
        $('#review-form').attr('action', $(this).data('review-action'));

        $('#comment-form, #review-form').find('input[type="text"], textarea').val("");
        $('#comment-form, #review-form').find('input[type="radio"]').prop('checked', false);
        
        return true;
    });

    $('.comment-submit').click(function() {
        var form = $('#comment-form');
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                $('#comment').modal('hide');
                alert('Comment added.');
            },
            error: function(data) {
                alert('Problem encountered while adding comment.');
            }
        });
        return false;
    });

    $('.review-submit').click(function() {
        var form = $('#review-form');
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                $('#review').modal('hide');
                alert('Review added.');
            },
            error: function(data) {
                alert('Problem encountered while adding review.');
            }
        });
        return false;
    });

    // slimscroll for portrait modal
    $(".person-list").slimScroll({
    	height: '320px',
    	railVisible: true
    });

    // load page hash in add shelf form
    $("#add-shelf").on('show.bs.modal', function(e) {
    	$(this).find("[name='hash']").val(window.location.hash);
    });
});
