<?php

namespace Pluckt\TemplateBundle\Model;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Exception;

abstract class CrudController extends Controller
{
    // stuff to replace in child class
    protected function getBaseTemplate()
    {
        // replace this with your base template alias
        return 'PlucktTempalateBundle:Default';
    }

    protected function newInstance()
    {
        // replace this with the crud object instance
        return new stdClass();
    }

    protected function find($id)
    {
        // replace with method to find the crud object
        return null;
    }

    protected function fetchList()
    {
        // replace with method to get the index listing of crud object
        return array();
    }

    protected function initFormParams()
    {
        // initializes form params for add / edit form
        return array();
    }

    protected function validate($o, $data)
    {
        // validate fields, throw exception if a field is wrong
        return true;
    }

    protected function update($o, $data)
    {
        // replace with method that updates the crud object $o based on arguments $data
    }

    protected function getRoutePrefix()
    {
        // prefix for routing
        return '';
    }

    protected function getTransPrefix()
    {
        // prefix for translation key
        return '';
    }

    // utility stuff
    protected function getEntityManager()
    {
        // assumes single doctrine entity manager
        return $this->getDoctrine()->getManager();
    }

    protected function setActiveMenu(&$menu_struct, $active)
    {
        foreach ($menu_struct as $index => $menu)
        {
            // active
            if ($index == $active)
            {
                // error_log('active - ' . $index);
                $menu_struct[$index]['active'] = true;
                return true;
            }

            // recurse through children
            if (isset($menu['children']))
            {
                if ($this->setActiveMenu($menu_struct[$index]['children'], $active))
                    $menu_struct[$index]['active'] = true;
            }
        }

        return false;
    }

    protected function getMenuStructure($active)
    {
        $menu =  [
            'dashboard' => [
                'label' => 'Dashboard',
                'link' => $this->generateUrl('pluckt_admin_index'),
                'active' => false,
                'children' => [],
            ],
            'database' =>  [
                'label' => 'Database',
                'link' => '#',
                'active' => false,
                'children' => [
                    'user' => [
                        'label' => 'Users',
                        'link' => $this->generateUrl('pluckt_admin_user_index'),
                        'active' => false,
                        'children' => [],
                    ],
                    'artist' => [
                        'label' => 'Artists',
                        'link' => $this->generateUrl('pluckt_admin_artist_index'),
                        'active' => false,
                        'children' => [],
                    ],
                    'event' => [
                        'label' => 'Awarding Events',
                        'link' => $this->generateUrl('pluckt_admin_event_index'),
                        'active' => false,
                        'children' => [],
                    ],
                ],
            ],
            'content' => [
                'label' => 'Content',
                'link' => '#',
                'active' => false,
                'children' => [
                    'video_category' => [
                        'label' => 'Video Categories',
                        'link' => $this->generateUrl('pluckt_admin_vcat_index'),
                        'active' => false,
                        'children' => [],
                    ],
                    'video' => [
                        'label' => 'Videos',
                        'link' => $this->generateUrl('pluckt_admin_video_index'),
                        'active' => false,
                        'children' => [],
                    ],
                    'field_note' => [
                        'label' => 'Field Notes',
                        'link' => $this->generateUrl('pluckt_admin_fnpage_index'),
                        'active' => false,
                        'children' => [],
                    ],
                ],
            ],
            'curation' => [
                'label' => 'Curation',
                'link' => '#',
                'active' => false,
                'children' => [
                    'curated_list' => [
                        'label' => 'Curated Lists',
                        'link' => $this->generateUrl('pluckt_admin_clist_index'),
                        'active' => false,
                        'children' => [],
                    ],
                    'user_list' => [
                        'label' => 'User Lists',
                        'link' => '#',
                        'active' => false,
                        'children' => [],
                    ],
                ],
            ],
        ];

        $this->setActiveMenu($menu, $active);

        return $menu;
    }

    protected function getActiveMenu()
    {
        return '';
    }

    protected function getPostParameters()
    {
        // return post params
        return $this->getRequest()->request->all();
    }

    protected function addFlash($type, $message)
    {
        // flash messages
        $this->get('session')
            ->getFlashBag()
            ->add($type, $message);
    }

    protected function getTrans($key)
    {
        // translator
        $trans = $this->get('translator');
        return $trans->trans($this->getTransPrefix() . $key);
    }

    protected function checkFind($id)
    {
        // TODO: use translation key for message
        $o = $this->find($id);
        if ($o == null)
            throw new Exception('Could not find object.');

        return $o;
    }


    // actions
    public function indexAction()
    {
        $params = array(
            'menu_structure' => $this->getMenuStructure($this->getActiveMenu()),
            'objects' => $this->fetchList()
        );

        return $this->render($this->getBaseTemplate() . ':index.html.twig', $params);
    }

    public function addFormAction()
    {
        $params = $this->initFormParams();

        $params['o'] = $this->newInstance();
        $params['menu_structure'] = $this->getMenuStructure($this->getActiveMenu());

        return $this->render($this->getBaseTemplate() . ':add.html.twig', $params);
    }

    public function addSubmitAction()
    {
        $em = $this->getEntityManager();
        $data = $this->getPostParameters();

        try
        {
            $o = $this->newInstance();
            $this->validate($o, $data);
            $this->update($o, $data);

            $em->persist($o);
            $em->flush();

            $this->addFlash('success', $this->getTrans('.msg.add.success'));
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
            $this->addFlash('error', $this->getTrans('.msg.add.error'));
        }


        return $this->redirect($this->generateUrl($this->getRoutePrefix() . '_index'));
    }

    public function editFormAction($id)
    {
        $params = $this->initFormParams();

        try
        {
            $o = $this->checkFind($id);
            $params['o'] = $o;
            $params['menu_structure'] = $this->getMenuStructure($this->getActiveMenu());

            return $this->render($this->getBaseTemplate() . ':edit.html.twig', $params);
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
            $this->addFlash('error', $this->getTrans('.msg.find.error'));
        }

        return $this->redirect($this->generateUrl($this->getRoutePrefix() . '_index'));
    }

    public function editSubmitAction($id)
    {
        $em = $this->getEntityManager();
        $data = $this->getPostParameters();

        try
        {
            $o = $this->checkFind($id);

            $this->validate($o, $data);
            $this->update($o, $data);

            $em->flush();

            $this->addFlash('success', $this->getTrans('.msg.edit.success'));
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
            $this->addFlash('error', $this->getTrans('.msg.edit.error'));
        }

        return $this->redirect($this->generateUrl($this->getRoutePrefix() . '_edit_form', array('id' => $id)));
    }

    public function deleteAction($id)
    {
        $em = $this->getEntityManager();
        
        try
        {
            $o = $this->checkFind($id);

            $em->remove($o);
            $em->flush();

            $this->addFlash('success', $this->getTrans('.msg.delete.success'));
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
            $this->addFlash('error', $this->getTrans('.msg.delete.error'));
        }


        return $this->redirect($this->generateUrl($this->getRoutePrefix() . '_index'));
    }
}
