<?php

namespace Pluckt\TemplateBundle\Model;

abstract class SelectOption
{
    public static function isValid($id)
    {
        $hash = static::getOptionsHash();
        if (isset($hash[$id]))
            return true;

        return false;
    }

    public static function getLabel($id)
    {
        $hash = static::getOptionsHash();
        if (isset($hash[$id]))
            return $hash[$id];

        return 'Unknown';
    }
}
