<?php

namespace Pluckt\APIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pluckt\AdminBundle\Entity\Purchase;
use Pluckt\AdminBundle\Entity\VideoAccess;
use DateTime;
use DateInterval;

class VideoController extends Controller
{
    protected function getBuyAmount($id)
    {
        return 5.99;
    }

    protected function getRentAmount($id)
    {
        return 1.99;
    }

    protected function getRentInterval()
    {
        return new DateInterval('P2D');
    }

    protected function purchase($id, $type, $action, $due, $expire)
    {
        $user = $this->getUser();

        // check wallet amount
        if ($user->getWalletAmount() <= $due)
        {
            $data = [
                'status' => false,
                'message' => 'Not enough wallet credits to ' . $action . ' this movie.',
            ];

            return $data;
        }

        // get video
        $em = $this->getDoctrine()->getEntityManager();
        $video = $em->getRepository('PlucktAdminBundle:Video')->find($id);
        if ($video == null)
        {
            $data = [
                'status' => false,
                'message' => 'Could not find movie.',
            ];
            return $data;
        }

        // store transaction
        $pur = new Purchase();
        $pur->setUser($user)
            ->setProductType($type)
            ->setVideo($video)
            ->setQuantity(1)
            ->setAmount($due);
        $em->persist($pur);

        // decrement wallet
        $user->decWalletAmount($due);

        // add access
        $access = new VideoAccess();
        $access->setUser($user)
            ->setVideo($video)
            ->setDateExpire($expire);
        $em->persist($access);

        $em->flush();

        $data = [
            'status' => true,
            'message' => 'Movie ' . $action . ' successful.',
        ];
        return $data;
    }

    public function buyAction($id)
    {
        $due = $this->getBuyAmount($id);
        $expire = new DateTime('2030-01-01');
        $data = $this->purchase($id, 'buy', 'buy', $due, $expire);
        $response = new JsonResponse($data);

        return $response;
    }

    public function rentAction($id)
    {
        $due = $this->getRentAmount($id);
        $date = new DateTime();
        $expire = $date->add($this->getRentInterval());
        $data = $this->purchase($id, 'rent', 'rent', $due, $expire);
        $response = new JsonResponse($data);

        return $response;
    }

    public function indexAction($name)
    {
        return $this->render('PlucktAPIBundle:Default:index.html.twig', array('name' => $name));
    }
}
