// curated list form stuff
function init_video_form() {
    // add button
    $('.btn-video-add').click(function() {
        var html = $('#video-row tbody').html();
        $('#video-table tbody').append(html);
        return false;
    });

    // remove button
    $('#video-table').on('click', '.btn-video-remove', function() {
        $(this).closest('tr').remove();
        return false;
    });

    // up button
    $('#video-table').on('click', '.btn-video-up', function() {
        var row = $(this).closest('tr');
        row.prev().before(row);
        return false;
    });

    // down button
    $('#video-table').on('click', '.btn-video-down', function() {
        var row = $(this).closest('tr');
        row.next().after(row);
        return false;
    });
}
