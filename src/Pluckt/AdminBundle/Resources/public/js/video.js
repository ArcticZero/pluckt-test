// artist form stuff
function init_artist_form() {
    // add button
    $('.btn-artist-add').click(function() {
        var html = $('#artist-row tbody').html();
        $('#artist-role-table tbody').append(html);
        return false;
    });

    // remove button
    $('#artist-role-table').on('click', '.btn-artist-remove', function() {
        $(this).closest('tr').remove();
        return false;
    });
}
