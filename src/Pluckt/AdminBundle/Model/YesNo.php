<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class YesNo extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            'yes' => 'Yes',
            'no' => 'No',
        );
    }
}
