<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class VocalLanguage extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            'tagalog' => 'Tagalog',
            'english' => 'English',
            'bicolano' => 'Bicolano',
            'cebuano' => 'Cebuano',
            'ilokano' => 'Ilokano',
            'kapampangan' => 'Kapampangan',
            'ilonggo' => 'Ilonggo',
            'waray-waray' => 'Waray-waray',
        );
    }
}
