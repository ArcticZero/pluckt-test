<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class Rating extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            'GP' => 'General Patronage',
            'PG' => 'Parental Guidance',
            'R-13' => 'Restricted-13',
            'R-16' => 'Restricted-16',
            'R-18' => 'Restricted-18',
            'SPG' => 'Strong Parental Guidance',
        );
    }
}
