<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class ArtistType extends SelectOption
{
    const DIRECTOR          = 1;
    const PRODUCER          = 2;
    const WRITER            = 3;
    const ACTOR             = 4;
    const CINEMATOGRAPHER   = 5;

    public static function getOptionsHash()
    {
        return array(
            self::ACTOR => 'Actor',
            self::CINEMATOGRAPHER => 'Cinematographer',
            self::DIRECTOR => 'Director',
            self::PRODUCER => 'Producer',
            self::WRITER => 'Writer',
        );
    }
}
