<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class SubLanguage extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            'tagalog' => 'Tagalog',
            'english' => 'English',
            'espanol' => 'Español',
            'deutsch' => 'Deutsch',
            'francais' => 'Francais',
            'japanese' => 'Japanese',
            'korean' => 'Korean',
        );
    }
}
