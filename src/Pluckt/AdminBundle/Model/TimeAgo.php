<?php

namespace Pluckt\AdminBundle\Model;

use DateTime;

class TimeAgo
{
    public static function get(DateTime $date)
    {
        $diff = $date->diff(new Datetime('now'));

        if ($diff->format("%m") > 0)
        {
            $time_ago = $date->format("n/j g:i A");
        }
        else if (($t = $diff->format("%d")) > 0)
        {
            $time_ago = $t . 'd';
        }
        else if (($t = $diff->format("%h")) > 0)
        {
            $time_ago = $t . 'h';
        }
        else if (($t = intval($diff->format("%i"))) > 0)
        {
            $time_ago = $t . 'm';
        }
        else
        {
            $time_ago = intval($diff->format("%s")) . 's';
        }

        return $time_ago;
    }
}
