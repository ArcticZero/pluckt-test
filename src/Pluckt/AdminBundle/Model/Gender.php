<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class Gender extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            'male' => 'Male',
            'female' => 'Female',
        );
    }
}
