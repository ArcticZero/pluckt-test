<?php

namespace Pluckt\AdminBundle\Model;

use Pluckt\TemplateBundle\Model\SelectOption;

class AspectRatio extends SelectOption
{
    public static function getOptionsHash()
    {
        return array(
            '16-9' => '16:9 (Widescreen)',
            '4-3' => '4:3',
        );
    }
}
