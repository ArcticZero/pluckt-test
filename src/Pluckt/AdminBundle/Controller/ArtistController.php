<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Template\CanFeatureController;
use Pluckt\AdminBundle\Entity\Artist;
use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Model\Country;
use Pluckt\AdminBundle\Model\Gender;

use Exception;
use DateTime;

class ArtistController extends Controller
{
    use CanFeatureController;

    public function __construct()
    {
        $this->setFeaturedRepository('PlucktAdminBundle:Artist');
        $this->setFeaturedViewPrefix('pluckt_admin_artist_');
    }

    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:Artist';
    }

    protected function newInstance()
    {
        return new Artist();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:Artist')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:Artist')->findAll();
    }

    protected function initFormParams()
    {
        return array(
            'country_opts' => Country::getOptionsHash(),
            'atype_opts' => ArtistType::getOptionsHash(),
            'gender_opts' => Gender::getOptionsHash(),
        );
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_artist';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.artist';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getEntityManager();

        $o->setName($data['name'])
            ->setAKA($data['aka'])
            ->setGender($data['gender'])
            ->setBirthday(new DateTime($data['birthday']))
            ->setBio($data['bio'])
            ->setNationality($data['nationality'])
            ->setBirthCity($data['birth_city'])
            ->setBirthCountry($data['birth_country'])
            ->setBaseCity($data['base_city'])
            ->setBaseCountry($data['base_country'])
            ->setQuote($data['quote'])
            ->setSchools($data['schools'])
            ->setTrivia($data['trivia'])
            ->setAgentInfo($data['agent_info'])
            ->setFacebookURL($data['facebook_url'])
            ->setTwitterURL($data['twitter_url'])
            ->setCompanies($data['companies'])
            ->setLinks($data['links'])
            ->setProjects($data['projects'])
            ->setTraining($data['training'])
            ->setFilmography($data['filmography'])
            ->setAwards($data['awards']);

        // images
        if (isset($data['profile_pic_id']) && $data['profile_pic_id'] > 0)
        {
            $upload_id = $data['profile_pic_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setProfilePic($upload);
        }
        if (isset($data['page_cover_id']) && $data['page_cover_id'] > 0)
        {
            $upload_id = $data['page_cover_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setPageCover($upload);
        }

        // have to do this since artist type uses artist id as primary key
        if ($o->getID() == null)
        {
            $em->persist($o);
            $em->flush();
        }

        // TODO: figure out why doctrine array collection clear doesn't work
        // NOTE: workaround for clearing the associations
        $types = $o->getTypes();
        foreach ($types as $t)
            $em->remove($t);
        $em->flush();

        if (isset($data['type_id']))
        {
            foreach ($data['type_id'] as $type_id)
                $o->addTypeID($type_id);
        }
    }

    protected function getActiveMenu()
    {
        return 'artist';
    }
}
