<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\UserBundle\Entity\User;
use Exception;
use Pluckt\AdminBundle\Model\Country;
use Pluckt\AdminBundle\Entity\UserAward;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:User';
    }

    protected function newInstance()
    {
        return new User();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktUserBundle:User')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktUserBundle:User')->findAll();
    }

    protected function initFormParams()
    {
        $em = $this->getEntityManager();

        // artists
        $artist_opts = [
            0 => '[ Not Linked ]'
        ];
        $artists = $em->getRepository('PlucktAdminBundle:Artist')
            ->findAll();
        foreach ($artists as $artist)
            $artist_opts[$artist->getID()] = $artist->getName();

        // videos
        $video_opts = [];
        $videos = $em->getRepository('PlucktAdminBundle:Video')->findAll();
        foreach ($videos as $video)
            $video_opts[$video->getID()] = $video->getName();

        // award events
        $event_opts = [];
        $events = $em->getRepository('PlucktAdminBundle:AwardEvent')->findAll();
        foreach ($events as $event)
            $event_opts[$event->getID()] = $event->getName();


        return array(
            'role_opts' => [
                'ROLE_NORMAL_USER' => 'User',
                'ROLE_READ_ONLY' => 'Read Only Admin',
                'ROLE_CREATIVE' => 'Creative',
                'ROLE_EDITOR' => 'Editor',
                'ROLE_SUPER_ADMIN' => 'Administrator'
            ],
            'enabled_opts' => [
                0 => 'Disabled',
                1 => 'Enabled'
            ],
            'country_opts' => Country::getOptionsHash(),
            'artist_opts' => $artist_opts,
            'video_opts' => $video_opts,
            'event_opts' => $event_opts,
        );
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_user';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.user';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        // TODO: check username

        // TODO: check email

        // check if new and password is specified
        if ($o->getID() == null && empty($data['pass1']))
            throw new Exception('No password specified.');

        // check password match
        if (!empty($data['pass1']))
        {
            if ($data['pass1'] != $data['pass2'])
                throw new Exception('Passwords do not match.');
        }
        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getEntityManager();

        $o->setUsername($data['username'])
            ->setName($data['name'])
            ->setEmail($data['email'])
            ->setBio($data['bio'])
            ->setCity($data['city'])
            ->setCountry($data['country'])
            ->setEnabled($data['enabled'])
            ->setWalletAmount($data['wallet_amount']);

        // password update?
        // check if new
        if ($o->getID() == null)
            $o->setPlainPassword($data['pass1']);
        else if (!empty($data['pass1']))
            $o->setPlainPassword($data['pass1']);

        // roles
        if (isset($data['roles']))
        {
            $o->clearRoles();
            foreach ($data['roles'] as $role)
                $o->addRole($role);
        }

        // artist
        if ($data['artist_id'] > 0)
        {
            $artist = $em->getRepository('PlucktAdminBundle:Artist')->find($data['artist_id']);
            if ($artist != null)
                $o->setArtist($artist);
        }
        else
            $o->setArtist(null);

        // images
        if (isset($data['profile_pic_id']) && $data['profile_pic_id'] > 0)
        {
            $upload_id = $data['profile_pic_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setProfilePic($upload);
        }
        if (isset($data['page_cover_id']) && $data['page_cover_id'] > 0)
        {
            $upload_id = $data['page_cover_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setPageCover($upload);
        }

        // password update for fos
        $um = $this->container->get('fos_user.user_manager');
        $um->updatePassword($o);

        // awards clear
        foreach ($o->getAwards() as $aw)
            $em->remove($aw);

        // flush for awards removal
        $em->persist($o);
        $em->flush();

        // awards
        if (isset($data['aw_event_id']))
        {
            foreach ($data['aw_event_id'] as $index => $event_id)
            {
                $video_id = $data['aw_video_id'][$index];
                $event = $em->getRepository('PlucktAdminBundle:AwardEvent')->find($event_id);
                $video = $em->getRepository('PlucktAdminBundle:Video')->find($video_id);

                $ua = new UserAward();
                $ua->setUser($o)
                    ->setEvent($event)
                    ->setYear($data['aw_year'][$index])
                    ->setName($data['aw_name'][$index])
                    ->setPlacement($data['aw_placement'][$index])
                    ->setVideo($video);

                $em->persist($ua);
            }
        }
    }

    protected function getActiveMenu()
    {
        return 'user';
    }
}
