<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController;

// TODO: extend from template bundle controller
class DashboardController extends CrudController
{
    public function indexAction()
    {
        $menu = $this->getMenuStructure('dashboard');

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT COUNT(u.id) FROM PlucktAdminBundle:Artist u');
        $artist_count = $query->getSingleScalarResult();

        $query = $em->createQuery('SELECT COUNT(u.id) FROM PlucktAdminBundle:Video u');
        $video_count = $query->getSingleScalarResult();

        $params = [
            'menu_structure' => $this->getMenuStructure('dashboard'),
            'video_count' => $video_count,
            'artist_count' => $artist_count,
        ];
        return $this->render('PlucktAdminBundle:Dashboard:index.html.twig', $params);
    }
}
