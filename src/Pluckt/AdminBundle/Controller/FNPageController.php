<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Template\CanFeatureController;
use Pluckt\AdminBundle\Entity\FNPage;
use Pluckt\SocialBundle\Entity\LogFNPage;
use Exception;

class FNPageController extends Controller
{
    use CanFeatureController;

    public function __construct()
    {
        $this->setFeaturedRepository('PlucktAdminBundle:FNPage');
        $this->setFeaturedViewPrefix('pluckt_admin_fnpage_');
    }

    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:FNPage';
    }

    protected function newInstance()
    {
        return new FNPage();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:FNPage')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:FNPage')->findAll();
    }

    protected function initFormParams()
    {
        $em = $this->getDoctrine()->getManager();

        $video_opts = [];
        $videos = $em->getRepository('PlucktAdminBundle:Video')->findAll();
        foreach ($videos as $v)
            $video_opts[$v->getID()] = $v->getName();

        $user_opts = [];
        $users = $em->getRepository('PlucktUserBundle:User')->findAll();
        foreach ($users as $u)
            $user_opts[$u->getID()] = $u->getFullName();

        return [
            'video_opts' => $video_opts,
            'user_opts' => $user_opts,
        ];
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_fnpage';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.fnpage';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getDoctrine()->getManager();

        $date_create = new \DateTime($data['date_create']);

        $o->setName($data['name'])
            ->setDateCreate($date_create)
            ->setTemplate($data['template'])
            ->setDescription($data['description'])
            ->setBody($data['body'])
            ->setUser($this->getUser());

        // video
        $video = $em->getRepository('PlucktAdminBundle:Video')->find($data['video_id']);
        if ($video != null)
            $o->setVideo($video);

        // user
        $user = $em->getRepository('PlucktUserBundle:User')->find($data['user_id']);
        if ($user != null)
            $o->setUser($user);

        // image
        if (isset($data['image_id']) && $data['image_id'] > 0)
        {
            $upload_id = $data['image_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setImage($upload);
        }

        // check if new
        if ($o->getID() == 0)
        {
            // log it
            $log = new LogFNPage();
            $log->setUser($this->getUser())
                ->setFNPage($o);

            // get users who are following this movie
            $follows = $em->getRepository('PlucktSocialBundle:Follow')
                ->findBy([
                    'target_class' => 'Pluckt\UserBundle\Entity\Video',
                    'target_id' => $video->getID(),
                ]);
            // add notifications
            foreach ($follows as $fol)
            {
                $target_user = $fol->getUser();
                $target_user->addNotification($log);
            }

            $em->persist($log);

            error_log('new');
        }
    }

    protected function getActiveMenu()
    {
        return 'field_note';
    }
}
