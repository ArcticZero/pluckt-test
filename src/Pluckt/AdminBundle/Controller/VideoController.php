<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Model\Rating;
use Pluckt\AdminBundle\Model\VocalLanguage;
use Pluckt\AdminBundle\Model\SubLanguage;
use Pluckt\AdminBundle\Model\AspectRatio;
use Pluckt\AdminBundle\Entity\Video;
use Pluckt\AdminBundle\Entity\VideoArtistRole;
use Pluckt\AdminBundle\Entity\VVocalLanguage;
use Pluckt\AdminBundle\Entity\VSubLanguage;
use Pluckt\AdminBundle\Model\Country;
use Pluckt\AdminBundle\Entity\VideoAward;

use Pluckt\AdminBundle\Template\CanFeatureController;

use Exception;
use DateTime;

class VideoController extends Controller
{
    use CanFeatureController;

    public function __construct()
    {
        $this->setFeaturedRepository('PlucktAdminBundle:Video');
        $this->setFeaturedViewPrefix('pluckt_admin_video_');
    }

    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:Video';
    }

    protected function newInstance()
    {
        return new Video();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        $video = $em->getRepository('PlucktAdminBundle:Video')->find($id);

        error_log(print_r($video->getDateRelease(), true));

        return $video;
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:Video')->findAll();
    }

    protected function initFormParams()
    {
        $em = $this->getEntityManager();

        // video categories
        $vcat_opts = array();
        $vcats = $em->getRepository('PlucktAdminBundle:VideoCategory')->findAll();
        foreach ($vcats as $vc)
            $vcat_opts[$vc->getID()] = $vc->getName();

        // ratings
        $rating_opts = Rating::getOptionsHash();

        // artists
        $artist_opts = array();
        $artists = $em->getRepository('PlucktAdminBundle:Artist')->findAll();
        foreach ($artists as $a)
            $artist_opts[$a->getID()] = $a->getName();

        // artist roles / types
        $role_opts = ArtistType::getOptionsHash();

        // vocal langs
        $vlang_opts = VocalLanguage::getOptionsHash();

        // sub langs
        $slang_opts = SubLanguage::getOptionsHash();

        // aspect ratio
        $ratio_opts = AspectRatio::getOptionsHash();

        // award events
        $event_opts = [];
        $events = $em->getRepository('PlucktAdminBundle:AwardEvent')->findAll();
        foreach ($events as $event)
            $event_opts[$event->getID()] = $event->getName();


        return array(
            'vcat_opts' => $vcat_opts,
            'artist_opts' => $artist_opts,
            'role_opts' => $role_opts,
            'rating_opts' => $rating_opts,
            'vlang_opts' => $vlang_opts,
            'slang_opts' => $slang_opts,
            'ratio_opts' => $ratio_opts,
            'country_opts' => Country::getOptionsHash(),
            'event_opts' => $event_opts,
        );
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_video';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.video';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getEntityManager();

        $o->setName($data['name'])
            ->setMediaID($data['media_id'])
            ->setTitle($data['title'])
            ->setEnglishTitle($data['english_title'])
            ->setDescription($data['description'])
            ->setRatingID($data['rating_id'])
            ->setMinutes($data['mins'])
            ->setYearProduced($data['year'])
            ->setProductionCompany($data['company'])
            ->setWebsiteURL($data['website'])
            ->setAspectRatioID($data['ratio_id'])
            ->setSynopsis($data['synopsis'])
            ->setTags($data['tags'])
            ->setCountry($data['country']);

        // release date
        $o->setDateRelease(new DateTime($data['date_release']));

        // categories
        $o->clearCategories();
        if (isset($data['vcat_id']))
        {
            foreach ($data['vcat_id'] as $vc_id)
            {
                $vc = $em->getRepository('PlucktAdminBundle:VideoCategory')->find($vc_id);
                if ($vc != null)
                    $o->addCategory($vc);
            }
        }

        // artist roles
        // $o->clearArtistRoles();
        foreach ($o->getArtistRoles() as $ar)
            $em->remove($ar);

        if (isset($data['artist_id']))
        {
            foreach ($data['artist_id'] as $index => $artist_id)
            {
                $role_id = $data['type_id'][$index];
                $artist = $em->getRepository('PlucktAdminBundle:Artist')->find($artist_id);
                if ($artist != null)
                {
                    $ar = new VideoArtistRole();
                    $ar->setArtist($artist)
                        ->setArtistRoleID($role_id);
                    $o->addArtistRole($ar);
                }
            }
        }

        // clear vocal and sub languages
        foreach ($o->getVocalLanguages() as $vl)
            $em->remove($vl);
        foreach ($o->getSubLanguages() as $sl)
            $em->remove($sl);

        // clear gallery
        $o->clearGallery();

        // clear awards
        foreach ($o->getAwards() as $aw)
            $em->remove($aw);


        $em->flush();

        $em->persist($o);
        $em->flush();

        // vocal languages
        if (isset($data['vocal_lang']))
        {
            foreach ($data['vocal_lang'] as $vlang_id)
            {
                $vlang = new VVocalLanguage();
                $vlang->setLanguageID($vlang_id);

                $o->addVocalLanguage($vlang);
                $em->persist($vlang);
            }
        }

        // sub languages
        if (isset($data['sub_lang']))
        {
            foreach ($data['sub_lang'] as $slang_id)
            {
                $slang = new VSubLanguage();
                $slang->setLanguageID($slang_id);

                $o->addSubLanguage($slang);
                $em->persist($slang);
            }
        }

        // poster
        if (isset($data['poster_id']) && $data['poster_id'] > 0)
        {
            // find
            $poster_id = $data['poster_id'];
            $poster = $em->getRepository('ZuluMediaBundle:Upload')->find($poster_id);
            $o->setPoster($poster);
        }

        // background
        if (isset($data['background_id']) && $data['background_id'] > 0)
        {
            // find
            $back_id = $data['background_id'];
            $background = $em->getRepository('ZuluMediaBundle:Upload')->find($back_id);
            $o->setBackground($background);
        }

        // gallery
        if (isset($data['gallery']))
        {
            foreach ($data['gallery'] as $g_upload_id)
            {
                $g_upload = $em->getRepository('ZuluMediaBundle:Upload')->find($g_upload_id);
                $o->addGallery($g_upload);
            }
        }

        // awards
        if (isset($data['aw_event_id']))
        {
            foreach ($data['aw_event_id'] as $index => $event_id)
            {
                $event = $em->getRepository('PlucktAdminBundle:AwardEvent')->find($event_id);

                $ua = new VideoAward();
                $ua->setVideo($o)
                    ->setEvent($event)
                    ->setYear($data['aw_year'][$index])
                    ->setName($data['aw_name'][$index])
                    ->setPlacement($data['aw_placement'][$index]);

                $em->persist($ua);
            }
        }

    }

    protected function getActiveMenu()
    {
        return 'video';
    }
}
