<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Entity\AwardEvent;
use Exception;

class AwardEventController extends Controller
{
    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:AwardEvent';
    }

    protected function newInstance()
    {
        return new AwardEvent();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:AwardEvent')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:AwardEvent')->findAll();
    }

    protected function initFormParams()
    {
        return [
        ];
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_event';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.event';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getDoctrine()->getManager();

        $o->setName($data['name'])
            ->setDescription($data['desc']);

        // image
        if (isset($data['logo_id']) && $data['logo_id'] > 0)
        {
            $upload_id = $data['logo_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setLogo($upload);
        }
    }

    protected function getActiveMenu()
    {
        return 'event';
    }
}
