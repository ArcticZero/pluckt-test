<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Entity\VideoCategory;
use Exception;

class VideoCategoryController extends Controller
{
    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:VideoCategory';
    }

    protected function newInstance()
    {
        return new VideoCategory();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:VideoCategory')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:VideoCategory')->findAll();
    }

    protected function initFormParams()
    {
        return array();
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_vcat';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.vcat';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getDoctrine()->getManager();

        $o->setName($data['name']);

        // image
        if (isset($data['image_id']) && $data['image_id'] > 0)
        {
            $upload_id = $data['image_id'];
            $upload = $em->getRepository('ZuluMediaBundle:Upload')->find($upload_id);
            $o->setImage($upload);
        }

    }

    protected function getActiveMenu()
    {
        return 'video_category';
    }
}
