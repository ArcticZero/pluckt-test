<?php

namespace Pluckt\AdminBundle\Controller;

use Pluckt\TemplateBundle\Model\CrudController as Controller;
use Pluckt\AdminBundle\Entity\CuratedList;
use Pluckt\AdminBundle\Entity\CuratedListVideo;
use Exception;

class CuratedListController extends Controller
{
    protected function getBaseTemplate()
    {
        return 'PlucktAdminBundle:CuratedList';
    }

    protected function newInstance()
    {
        return new CuratedList();
    }

    protected function find($id)
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:CuratedList')->find($id);
    }

    protected function fetchList()
    {
        $em = $this->getEntityManager();
        return $em->getRepository('PlucktAdminBundle:CuratedList')->findAll();
    }

    protected function initFormParams()
    {
        $em = $this->getEntityManager();

        $videos = $em->getRepository('PlucktAdminBundle:Video')->findAll();
        $v_opts = array();
        foreach ($videos as $v)
            $v_opts[$v->getID()] = $v->getName();

        return array(
            'video_opts' => $v_opts,
        );
    }

    protected function getRoutePrefix()
    {
        return 'pluckt_admin_clist';
    }

    protected function getTransPrefix()
    {
        return 'pluckt.clist';
    }

    protected function validate($o, $data)
    {
        // TODO: check if we have all required fields

        return true;
    }

    protected function update($o, $data)
    {
        $em = $this->getEntityManager();

        $o->setName($data['name']);

        foreach ($o->getVideos() as $clv)
            $em->remove($clv);

        if (isset($data['video_id']))
        {
            $order_id = 0;

            foreach ($data['video_id'] as $vid)
            {
                $video = $em->getRepository('PlucktAdminBundle:Video')->find($vid);
                if ($video != null)
                {
                    $order_id++;
                    $clv = new CuratedListVideo();
                    $clv->setVideo($video)
                        ->setOrderID($order_id);
                    $o->addVideo($clv);
                }
            }
        }
    }

    protected function getActiveMenu()
    {
        return 'curated_list';
    }

    public function featureAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $clist = $this->find($id);
        $clist->setFeatured(true);

        $em->flush();
        
        return $this->redirect($this->generateUrl('pluckt_admin_clist_index'));
    }

    public function unfeatureAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $clist = $this->find($id);
        $clist->setFeatured(false);

        $em->flush();

        return $this->redirect($this->generateUrl('pluckt_admin_clist_index'));
    }

    public function pickAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $clist = $this->find($id);
        $clist->setPick(true);

        $em->flush();
        
        return $this->redirect($this->generateUrl('pluckt_admin_clist_index'));
    }

    public function unpickAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $clist = $this->find($id);
        $clist->setPick(false);

        $em->flush();

        return $this->redirect($this->generateUrl('pluckt_admin_clist_index'));
    }
}
