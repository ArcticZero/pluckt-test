<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Entity\AType;
use Pluckt\AdminBundle\Model\Country;

use Pluckt\AdminBundle\Template\CanFeature;

use Zulu\MediaBundle\Entity\Upload;

use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="artist")
 */
class Artist
{
    use CanFeature;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=80) */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="AType", mappedBy="artist", cascade={"persist", "remove"}, indexBy="type_id")
     */
    protected $types;

    /** @ORM\OneToMany(targetEntity="VideoArtistRole", mappedBy="artist") */
    protected $roles;

    /** @ORM\Column(type="string", length=80) */
    protected $aka;

    /** @ORM\Column(type="string", length=10) */
    protected $gender;

    /** @ORM\Column(type="date") */
    protected $birthday;

    /** @ORM\Column(type="text") */
    protected $bio;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="profile_pic_id", referencedColumnName="id")
     */
    protected $profile_pic;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="page_cover_id", referencedColumnName="id")
     */
    protected $page_cover;

    /** @ORM\Column(type="string", length=3) */
    protected $nationality;

    /** @ORM\Column(type="string", length=80) */
    protected $birth_city;

    /** @ORM\Column(type="string", length=3) */
    protected $birth_country;

    /** @ORM\Column(type="string", length=80) */
    protected $base_city;

    /** @ORM\Column(type="string", length=3) */
    protected $base_country;

    /** @ORM\Column(type="text") */
    protected $quote;

    /** @ORM\Column(type="text") */
    protected $schools;

    /** @ORM\Column(type="text") */
    protected $trivia;

    /** @ORM\Column(type="text") */
    protected $agent_info;

    /** @ORM\Column(type="string", length=80) */
    protected $facebook_url;

    /** @ORM\Column(type="string", length=80) */
    protected $twitter_url;

    /** @ORM\Column(type="text") */
    protected $companies;

    /** @ORM\Column(type="text") */
    protected $links;

    /** @ORM\Column(type="text") */
    protected $projects;

    /** @ORM\Column(type="text") */
    protected $training;

    /** @ORM\Column(type="text") */
    protected $filmography;

    /** @ORM\Column(type="text") */
    protected $awards;

    /** @ORM\OneToOne(targetEntity="Pluckt\UserBundle\Entity\User", mappedBy="artist") */
    protected $user;


    public function __construct()
    {
        $this->initCanFeature();

        $this->id = null;
        $this->name = '';
        $this->types = new ArrayCollection();
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addTypeID($type_id)
    {
        $type = new AType();
        $type->setArtist($this)
            ->setTypeID($type_id);

        $this->types[$type_id] = $type;

        return $this;
    }

    public function getTypes()
    {
        return $this->types;
    }

    public function getTypesText()
    {
        $types_text = [];
        foreach ($this->types as $atype)
            $types_text[] = ArtistType::getLabel($atype->getTypeID());

        return implode(', ', $types_text);
    }

    public function clearTypeIDs()
    {
        error_log('clearing artist types');
        $this->types->clear();
        return $this;
    }

    public function getTypeIDs()
    {
        $types = array();
        foreach ($this->types as $atype)
            $types[] = $atype->getTypeID();

        return $types;
    }

    public function getTypeIDHash()
    {
        $types = array();
        foreach ($this->types as $atype)
            $types[$atype->getTypeID()] = ArtistType::getLabel($atype->getTypeID());

        return $types;
    }

    public function setAKA($aka)
    {
        $this->aka = $aka;
        return $this;
    }

    public function getAKA()
    {
        return $this->aka;
    }

    /*
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }
    */

    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getGenderLabel()
    {
        return Gender::getLabel($this->gender);
    }

    public function setBirthday(DateTime $date)
    {
        $this->birthday = $date;
        return $this;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
        return $this;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function setBirthCity($city)
    {
        $this->birth_city = $city;
        return $this;
    }

    public function getBirthCity()
    {
        return $this->birth_city;
    }

    public function setBirthCountry($country)
    {
        $this->birth_country = $country;
        return $this;
    }

    public function getBirthCountry()
    {
        return $this->birth_country;
    }

    public function getBirthCountryLabel()
    {
        return Country::getLabel($this->birth_country);
    }

    public function setBaseCity($city)
    {
        $this->base_city = $city;
        return $this;
    }

    public function getBaseCity()
    {
        return $this->base_city;
    }

    public function setBaseCountry($country)
    {
        $this->base_country = $country;
        return $this;
    }

    public function getBaseCountry()
    {
        return $this->base_country;
    }

    public function getBaseCountryLabel()
    {
        return Country::getLabel($this->base_country);
    }

    public function setQuote($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    public function getQuote()
    {
        return $this->quote;
    }

    public function setSchools($schools)
    {
        $this->schools = $schools;
        return $this;
    }

    public function getSchools()
    {
        return $this->schools;
    }

    public function setTrivia($trivia)
    {
        $this->trivia = $trivia;
        return $this;
    }

    public function getTrivia()
    {
        return $this->trivia;
    }

    public function setAgentInfo($info)
    {
        $this->agent_info = $info;
        return $this;
    }

    public function getAgentInfo()
    {
        return $this->agent_info;
    }

    public function setFacebookURL($url)
    {
        $this->facebook_url = $url;
        return $this;
    }

    public function getFacebookURL()
    {
        return $this->facebook_url;
    }

    public function setTwitterURL($url)
    {
        $this->twitter_url = $url;
        return $this;
    }

    public function getTwitterURL()
    {
        return $this->twitter_url;
    }

    public function setCompanies($companies)
    {
        $this->companies = $companies;
        return $this;
    }

    public function getCompanies()
    {
        return $this->companies;
    }

    public function setLinks($links)
    {
        $this->links = $links;
        return $this;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function setProjects($projects)
    {
        $this->projects = $projects;
        return $this;
    }

    public function getProjects()
    {
        return $this->projects;
    }

    public function setTraining($training)
    {
        $this->training = $training;
        return $this;
    }

    public function getTraining()
    {
        return $this->training;
    }

    public function setFilmography($filmography)
    {
        $this->filmography = $filmography;
        return $this;
    }

    public function getFilmography()
    {
        return $this->filmography;
    }

    public function setAwards($awards)
    {
        $this->awards = $awards;
        return $this;
    }

    public function getAwards()
    {
        return $this->awards;
    }

    public function setProfilePic(Upload $upload)
    {
        $this->profile_pic = $upload;
        return $this;
    }

    public function getProfilePic()
    {
        return $this->profile_pic;
    }

    public function setPageCover(Upload $upload)
    {
        $this->page_cover = $upload;
        return $this;
    }

    public function getPageCover()
    {
        return $this->page_cover;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getUser()
    {
        return $this->user;
    }
}
