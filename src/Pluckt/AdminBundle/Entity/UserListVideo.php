<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasVideo;
use Pluckt\SocialBundle\Entity\BaseSocial;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="user_list_video",
 * )
 */
class UserListVideo extends BaseSocial
{
    use HasVideo;

    /**
     * @ORM\ManyToOne(targetEntity="UserList", cascade={"persist"})
     * @ORM\JoinColumn(name="list_id", referencedColumnName="id")
     */
    protected $user_list;

    /** @ORM\Column(type="datetime") */
    protected $date_added;

    public function __construct()
    {
        $this->date_added = new DateTime();
        parent::__construct();
    }

    public function setList(UserList $list)
    {
        $this->user_list = $list;
        return $this;
    }

    public function getList()
    {
        return $this->user_list;
    }

    public function getDateAdded()
    {
        return $this->date_added;
    }

    public function setVideo($video)
    {
        $this->video = $video;
        $this->setTargetID($video->getID());
        $this->setTargetClass(get_class($video));
        return $this;
    }
}
