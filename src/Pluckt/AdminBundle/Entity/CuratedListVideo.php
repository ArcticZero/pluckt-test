<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasVideo;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="curated_list_video",
 *      indexes={@ORM\Index(name="order_id_idx", columns={"order_id"})}
 * )
 */
class CuratedListVideo
{
    use HasID;
    use HasVideo;

    /**
     * @ORM\ManyToOne(targetEntity="CuratedList", cascade={"persist"})
     * @ORM\JoinColumn(name="list_id", referencedColumnName="id")
     */
    protected $curated_list;

    /** @ORM\Column(type="integer") */
    protected $order_id;

    public function setList(CuratedList $list)
    {
        $this->curated_list = $list;
        return $this;
    }

    public function getList()
    {
        return $this->curated_list;
    }

    public function setOrderID($order_id)
    {
        $this->order_id = $order_id;
        return $this;
    }

    public function getOrderID()
    {
        return $this->order_id;
    }
}
