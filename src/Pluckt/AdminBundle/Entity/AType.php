<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Entity\Artist;
use Pluckt\AdminBundle\Model\ArtistType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="artist_type")
 */
class AType
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="types", cascade={"persist"})
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id")
     */
    protected $artist;

    /** 
     * @ORM\Id
     * @ORM\Column(type="smallint") 
     */
    protected $type_id;

    public function setArtist(Artist $artist)
    {
        $this->artist = $artist;
        return $this;
    }

    public function getArtist()
    {
        return $this->artist;
    }

    public function setTypeID($id)
    {
        $this->type_id = $id;
        return $this;
    }

    public function getTypeID()
    {
        return $this->type_id;
    }

    public function getTypeLabel()
    {
        return ArtistType::getLabel($this->type_id);
    }
}
