<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasName;

/**
 * @ORM\Entity
 * @ORM\Table(name="curated_list")
 */
class CuratedList
{
    use HasID;
    use HasName;

    /**
     * @ORM\OneToMany(targetEntity="CuratedListVideo", mappedBy="curated_list", cascade={"persist"})
     */
    protected $videos;

    /** @ORM\Column(type="boolean") */
    protected $flag_featured;

    /** @ORM\Column(type="boolean") */
    protected $flag_pick;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->flag_featured = false;
        $this->flag_pick = false;
    }

    public function clearVideos()
    {
        $this->videos->clear();
        return $this;
    }

    public function addVideo(CuratedListVideo $video)
    {
        $video->setList($this);
        $this->videos[] = $video;
        return $this;
    }

    public function getVideos()
    {
        return $this->videos;
    }

    public function setFeatured($flag = true)
    {
        $this->flag_featured = $flag;
        return $this;
    }

    public function isFeatured()
    {
        if ($this->flag_featured)
            return true;

        return false;
    }

    public function setPick($flag = true)
    {
        $this->flag_pick = $flag;
        return $this;
    }

    public function isPick()
    {
        if ($this->flag_pick)
            return true;

        return false;
    }
}
