<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Model\ArtistType;

/**
 * @ORM\Entity
 * @ORM\Table(name="video_artist_role")
 */
class VideoArtistRole
{
    use HasID;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /**
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="roles")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id")
     */
    protected $artist;

    /** @ORM\Column(type="smallint") */
    protected $artist_type_id;


    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setArtist(Artist $artist)
    {
        $this->artist = $artist;
        return $this;
    }

    public function getArtist()
    {
        return $this->artist;
    }

    public function setArtistRoleID($id)
    {
        $this->artist_type_id = $id;
        return $this;
    }

    public function getArtistRoleID()
    {
        return $this->artist_type_id;
    }

    public function getArtistRoleLabel()
    {
        return ArtistType::getLabel($this->artist_type_id);
    }

}
