<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasAward;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="user_award",
 * )
 */
class UserAward
{
    use HasID;
    use HasAward;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;



    public function __construct()
    {
        $this->user = null;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setVideo($video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }
}
