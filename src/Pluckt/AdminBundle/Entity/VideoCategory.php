<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zulu\MediaBundle\Entity\Upload;

/**
 * @ORM\Entity
 * @ORM\Table(name="vcategory")
 */
class VideoCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=80) */
    protected $name;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;

    /**
     * @ORM\ManyToMany(targetEntity="Video", mappedBy="categories")
     */
    protected $videos;


    public function __construct()
    {
        $this->id = null;
        $this->name = '';
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setImage(Upload $image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getVideos()
    {
        return $this->videos;
    }
}
