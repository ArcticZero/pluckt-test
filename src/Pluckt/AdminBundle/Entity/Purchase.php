<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\UserBundle\Entity\User;
use Pluckt\AdminBundle\Template\HasID;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="purchase")
 */
class Purchase
{
    use HasID;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /** @ORM\Column(type="string", length=30) */
    protected $product_type;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /** @ORM\Column(type="decimal", precision=8, scale=2) */
    protected $quantity;

    /** @ORM\Column(type="decimal", precision=8, scale=2) */
    protected $amount;

    /** @ORM\Column(type="json_array") */
    protected $meta;

    public function __construct()
    {
        // capture current date + time
        $this->date_create = new DateTime();
        $this->meta = [];
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function setProductType($type)
    {
        $this->product_type = $type;
        return $this;
    }

    public function getProductType()
    {
        return $this->product_type;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setQuantity($qty)
    {
        $this->quantity = $qty;
        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setAmount($amt)
    {
        $this->amount = $amt;
        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setMeta($meta)
    {
        $this->meta = $meta;
        return $this;
    }

    public function getMeta()
    {
        return $this->meta;
    }
}
