<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasName;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_list")
 */
class UserList
{
    use HasID;
    use HasName;

    /**
     * @ORM\OneToMany(targetEntity="UserListVideo", mappedBy="user_list", cascade={"persist"})
     */
    protected $videos;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->date_create = new DateTime();
    }

    public function clearVideos()
    {
        $this->videos->clear();
        return $this;
    }

    public function addVideo(UserListVideo $video)
    {
        $video->setList($this);
        $this->videos[] = $video;
        return $this;
    }

    public function getVideos()
    {
        return $this->videos;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTotalMinutes()
    {
        $mins = 0;
        foreach ($this->videos as $uv)
        {
            $mins += $uv->getVideo()->getMinutes();
        }

        return $mins;
    }
}
