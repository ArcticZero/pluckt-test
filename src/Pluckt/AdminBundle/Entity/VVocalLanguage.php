<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Entity\Video;
use Pluckt\AdminBundle\Model\VocalLanguage;

/**
 * @ORM\Entity
 * @ORM\Table(name="video_vocal_lang")
 */
class VVocalLanguage
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="vocal_langs", cascade={"persist"})
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /** 
     * @ORM\Id
     * @ORM\Column(type="string", length=30) 
     */
    protected $lang_id;

    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setLanguageID($lang_id)
    {
        $this->lang_id = $lang_id;
        return $this;
    }

    public function getLanguageID()
    {
        return $this->lang_id;
    }

    public function getLanguageLabel()
    {
        return VocalLanguage::getLabel($this->lang_id);
    }
}
