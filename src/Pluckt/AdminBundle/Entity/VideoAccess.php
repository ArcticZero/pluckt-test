<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\UserBundle\Entity\User;
use DateTime;
use Pluckt\AdminBundle\Template\HasID;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="video_access",
 * )
 */
class VideoAccess
{
    use HasID;
   
    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /** @ORM\Column(type="date") */
    protected $date_expire;

    public function __construct()
    {
        $this->date_create = new DateTime();
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setDateExpire(DateTime $date)
    {
        $this->date_expire = $date;
        return $this;
    }

    public function getDateExpire()
    {
        return $this->date_expire;
    }
}
