<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Pluckt\AdminBundle\Template\HasID;
use Zulu\MediaBundle\Entity\Upload;

/**
 * @ORM\Entity
 * @ORM\Table(name="award_event")
 */
class AwardEvent
{
    use HasID;

    /** @ORM\Column(type="string", length=120) */
    protected $name;

    /** @ORM\Column(type="text") */
    protected $description;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="logo_id", referencedColumnName="id")
     */
    protected $logo;

    public function __construct()
    {
        $this->name = '';
        $this->description = '';
        $this->logo = null;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setLogo(Upload $logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }
}
