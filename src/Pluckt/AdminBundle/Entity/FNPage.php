<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\CanFeature;

use Zulu\MediaBundle\Entity\Upload;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="fn_page")
 */
class FNPage
{
    use HasID;
    use CanFeature;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /** @ORM\Column(type="string", length=80) */
    protected $name;

    protected $posts;

    /** @ORM\Column(type="string", length=120) */
    protected $template;

    /** @ORM\Column(type="text") */
    protected $description;

    /** @ORM\Column(type="text") */
    protected $body;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="profile_pic_id", referencedColumnName="id")
     */
    protected $image;

    /**
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="field_notes", cascade={"persist"})
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /**
     * @ORM\ManyToOne(targetEntity="Pluckt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    public function __construct()
    {
        $this->initCanFeature();
        $this->date_create = new DateTime();
        $this->posts = new ArrayCollection();
    }

    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;
        return $this;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function clearPosts()
    {
        $this->posts->clear();
    }

    public function addPost($post)
    {
        $post->setPage($this);
        $this->posts[] = $post;

        return $this;
    }

    public function getPosts()
    {
        return $this->posts;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setImage(Upload $image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTargetID()
    {
        return $this->video->getID();
    }

    public function getTargetClass()
    {
        return 'Pluckt\AdminBundle\Entity\Video';
    }

    public function setTarget($target)
    {
        return $this;
    }

    public function getTarget()
    {
        return $this->video;
    }
}
