<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasCountry;
use Pluckt\SocialBundle\Template\HasComments;
use Pluckt\SocialBundle\Template\HasReviews;
use Pluckt\SocialBundle\Template\HasFollows;
use Pluckt\AdminBundle\Template\CanFeature;

use Pluckt\AdminBundle\Model\VocalLanguage;
use Pluckt\AdminBundle\Model\SubLanguage;
use Pluckt\AdminBundle\Model\ArtistType;
use Pluckt\AdminBundle\Model\Country;

use Zulu\MediaBundle\Entity\Upload;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="video")
 */
class Video
{
    use HasID;
    use HasComments;
    use HasReviews;
    use HasFollows;
    use CanFeature;

    /** @ORM\Column(type="string", length=120) */
    protected $title;

    /** @ORM\Column(type="string", length=120) */
    protected $english_title;

    /** @ORM\Column(type="string", length=120) */
    protected $name;

    /** @ORM\Column(type="string", length=200) */
    protected $media_id;

    /** @ORM\Column(type="text") */
    protected $description;

    /** @ORM\Column(type="string", length=10) */
    protected $rating_id;

    /**
     * @ORM\OneToMany(targetEntity="VVocalLanguage", mappedBy="video", cascade={"persist"}, indexBy="vlang_id")
     */
    protected $vocal_langs;

    /**
     * @ORM\OneToMany(targetEntity="VSubLanguage", mappedBy="video", cascade={"persist"}, indexBy="vsub_id")
     */
    protected $sub_langs;

    /** @ORM\Column(type="smallint") */
    protected $minutes;

    /** @ORM\Column(type="smallint") */
    protected $year_produced;

    /** @ORM\Column(type="string", length=80) */
    protected $production_company;

    /** @ORM\Column(type="string", length=120) */
    protected $synopsis;

    /** @ORM\Column(type="text") */
    protected $tags;

    /** @ORM\Column(type="string", length=10) */
    protected $aspect_ratio_id;

    /** @ORM\Column(type="string", length=120) */
    protected $website_url;

    /** 
     * @ORM\OneToMany(targetEntity="VideoArtistRole", mappedBy="video", cascade={"persist", "remove"})
     */
    protected $artist_roles;

    /**
     * @ORM\ManyToMany(targetEntity="VideoCategory", inversedBy="videos")
     * @ORM\JoinTable(name="video_category",
     *      joinColumns={@ORM\JoinColumn(name="vcat_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")}
     *      )
     */
    protected $categories;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="poster_id", referencedColumnName="id")
     */
    protected $poster;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="background_id", referencedColumnName="id")
     */
    protected $background;

    /** @ORM\Column(type="string", length=3) */
    protected $country;

    /**
     * @ORM\OneToMany(targetEntity="FNPage", mappedBy="video")
     */
    protected $field_notes;


    /**
     * @ORM\ManyToMany(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinTable(name="video_gallery",
     *      joinColumns={@ORM\JoinColumn(name="upload_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")}
     *      )
     */
    protected $gallery;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\VideoAward", mappedBy="video") */
    protected $awards;

    /** @ORM\Column(type="date") */
    protected $date_release;

    protected $role_hash;

    public function __construct()
    {
        $this->initHasComments();
        $this->initHasReviews();
        $this->initHasFollows();
        $this->initCanFeature();

        $this->artist_roles = new ArrayCollection();
        $this->categories = new ArrayCollection();

        $this->vocal_langs = new ArrayCollection();
        $this->sub_langs = new ArrayCollection();

        $this->field_notes = new ArrayCollection();
        $this->gallery = new ArrayCollection();

        $this->awards = new ArrayCollection();

        $this->date_release = new DateTime();

        $this->role_hash = null;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setMediaID($id)
    {
        $this->media_id = $id;
        return $this;
    }

    public function getMediaID()
    {
        return $this->media_id;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function clearArtistRoles()
    {
        $this->artist_roles->clear();
        return $this;
    }

    public function addArtistRole(VideoArtistRole $artist_role)
    {
        $artist_role->setVideo($this);
        $this->artist_roles[] = $artist_role;
        return $this;
    }

    public function getArtistRoles()
    {
        return $this->artist_roles;
    }

    public function clearCategories()
    {
        $this->categories->clear();
        return $this;
    }

    public function addCategory(VideoCategory $vcat)
    {
        $this->categories[] = $vcat;
        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function getCategoryHash()
    {
        $hash = array();
        foreach ($this->categories as $cat)
            $hash[$cat->getID()] = $cat->getName();

        return $hash;
    }

    public function getCategoriesText()
    {
        $cats = [];
        foreach ($this->categories as $cat)
            $cats[] = $cat->getName();

        return implode(', ', $cats);
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setEnglishTitle($title)
    {
        $this->english_title = $title;
        return $this;
    }

    public function getEnglishTitle()
    {
        return $this->english_title;
    }

    public function setRatingID($rating_id)
    {
        $this->rating_id = $rating_id;
        return $this;
    }

    public function getRatingID()
    {
        return $this->rating_id;
    }

    public function addVocalLanguage(VVocalLanguage $vvl)
    {
        $vvl->setVideo($this);
        $this->vocal_langs[] = $vvl;
        return $this;
    }

    public function clearVocalLanguages()
    {
        $this->vocal_langs->clear();
        return $this;
    }

    public function getVocalLanguages()
    {
        return $this->vocal_langs;
    }

    public function getVocalLanguageHash()
    {
        $langs = array();
        foreach ($this->vocal_langs as $vlang)
        {
            $id = $vlang->getLanguageID();
            $langs[$id] = VocalLanguage::getLabel($id);
        }

        return $langs;
    }

    public function getVocalLanguagesLabel()
    {
        $langs = $this->getVocalLanguageHash();
        return implode(', ', $langs);
    }

    public function addSubLanguage(VSubLanguage $vsl)
    {
        $vsl->setVideo($this);
        $this->sub_langs[] = $vsl;
        return $this;
    }

    public function clearSubLanguages()
    {
        $this->sub_langs->clear();
        return $this;
    }

    public function getSubLanguages()
    {
        return $this->sub_langs;
    }

    public function getSubLanguageHash()
    {
        $langs = array();
        foreach ($this->sub_langs as $slang)
        {
            $id = $slang->getLanguageID();
            $langs[$id] = SubLanguage::getLabel($id);
        }

        return $langs;
    }

    public function getSubLanguagesLabel()
    {
        $langs = $this->getSubLanguageHash();
        return implode(', ', $langs);
    }

    public function setMinutes($mins)
    {
        $this->minutes = $mins;
        return $this;
    }

    public function getMinutes()
    {
        return $this->minutes;
    }

    public function setYearProduced($year)
    {
        $this->year_produced = $year;
        return $this;
    }

    public function getYearProduced()
    {
        return $this->year_produced;
    }

    public function setProductionCompany($company)
    {
        $this->production_company = $company;
        return $this;
    }

    public function getProductionCompany()
    {
        return $this->production_company;
    }

    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
        return $this;
    }

    public function getSynopsis()
    {
        return $this->synopsis;
    }

    public function setAspectRatioID($ratio)
    {
        $this->aspect_ratio_id = $ratio;
        return $this;
    }

    public function getAspectRatioID()
    {
        return $this->aspect_ratio_id;
    }

    public function setWebsiteURL($url)
    {
        $this->website_url = $url;
        return $this;
    }

    public function getWebsiteURL()
    {
        return $this->website_url;
    }

    /*
    public function setAwards($awards)
    {
        $this->awards = $awards;
        return $this;
    }
    */

    public function getAwards()
    {
        return $this->awards;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setPoster(Upload $poster)
    {
        $this->poster = $poster;
        return $this;
    }

    public function getPoster()
    {
        return $this->poster;
    }

    public function setBackground(Upload $upload)
    {
        $this->background = $upload;
        return $this;
    }

    public function getBackground()
    {
        return $this->background;
    }

    protected function generateRoleHash()
    {
        if ($this->role_hash == null)
        {
            // initialize
            $this->role_hash = [
                'director' => null,
                'crew' => [],
                'cast' => []
            ];

            // populate
            foreach ($this->artist_roles as $role)
            {
                switch ($role->getArtistRoleID())
                {
                    // director
                    case ArtistType::DIRECTOR:
                        $this->role_hash['director'] = $role->getArtist();
                        $this->role_hash['crew'][] = $role;
                        break;
                    // cast
                    case ArtistType::ACTOR:
                        $this->role_hash['cast'][] = $role;
                        break;
                    // crew
                    default:
                        $this->role_hash['crew'][] = $role;
                        break;
                }
            }
        }
    }

    public function getDirector()
    {
        $this->generateRoleHash();

        return $this->role_hash['director'];
    }

    public function getCast()
    {
        $this->generateRoleHash();

        return $this->role_hash['cast'];
    }

    public function getCrew()
    {
        $this->generateRoleHash();

        return $this->role_hash['crew'];
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCountryLabel()
    {
        return Country::getLabel($this->country);
    }

    public function getFieldNotes()
    {
        return $this->field_notes;
    }

    public function addGallery(Upload $upload)
    {
        $this->gallery[] = $upload;
        return $this;
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function clearGallery()
    {
        $this->gallery->clear();
        return $this;
    }

    public function setDateRelease(DateTime $date)
    {
        $this->date_release = $date;
        return $this;
    }

    public function getDateRelease()
    {
        if ($this->date_release == null || $this->date_release->format('Y') == -1)
        {
            // quick fix for existing movies
            // if date release is null, then just set it to yesterday
            $date = new DateTime();
            $date->modify('-1 day');
            return $date;
        }

        return $this->date_release;
    }
}
