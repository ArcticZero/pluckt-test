<?php

namespace Pluckt\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasID;
use Pluckt\AdminBundle\Template\HasAward;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="video_award",
 * )
 */
class VideoAward
{
    use HasID;
    use HasAward;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;



    public function __construct()
    {
        $this->video = null;
    }

    public function setVideo($video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }
}
