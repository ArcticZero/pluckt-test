<?php

namespace Pluckt\AdminBundle\Template;

trait CanFeature
{
    /** @ORM\Column(type="boolean") */
    protected $featured;

    public function initCanFeature()
    {
        $this->featured = false;
    }

    public function setFeatured($featured = true)
    {
        if ($featured)
        {
            $this->featured = true;
            return $this;
        }

        $this->featured = false;
        return $this;
    }

    public function isFeatured()
    {
        if ($this->featured)
            return true;

        return false;
    }
}
