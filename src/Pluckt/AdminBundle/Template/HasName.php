<?php

namespace Pluckt\AdminBundle\Template;

trait HasName
{
    /** @ORM\Column(type="string", length=120) */
    protected $name;


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
}
