<?php

namespace Pluckt\AdminBundle\Template;

use Doctrine\ORM\Mapping as ORM;

trait HasID
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    public function getID()
    {
        return $this->id;
    }
}
