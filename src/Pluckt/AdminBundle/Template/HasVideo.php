<?php

namespace Pluckt\AdminBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Entity\Video;

trait HasVideo
{
    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    public function setVideo(Video $video)
    {
        $this->video = $video;
        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }
}
