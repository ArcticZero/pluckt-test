<?php

namespace Pluckt\AdminBundle\Template;

use Doctrine\ORM\Mapping as ORM;

trait HasWallet
{
    /** @ORM\Column(type="decimal", precision=8, scale=2) */
    protected $wallet_amount;

    public function initHasWallet()
    {
        $this->wallet_amount = 0;
    }

    public function setWalletAmount($amt)
    {
        $this->wallet_amount = $amt;
        return $this;
    }

    public function addWalletAmount($amt)
    {
        $this->wallet_amount += $amt;
        return $this;
    }

    public function decWalletAmount($amt)
    {
        // TODO: make our own exception
        // check that we don't go below 0
        if ($amt > $this->wallet_amount)
            throw new \Exception('Amount exceeds wallet contents.');

        $this->wallet_amount -= $amt;
        return $this;
    }

    public function getWalletAmount()
    {
        return $this->wallet_amount;
    }
}
