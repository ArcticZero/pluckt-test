<?php

namespace Pluckt\AdminBundle\Template;

use Doctrine\ORM\Mapping as ORM;

trait HasAward
{
    /**
     * @ORM\ManyToOne(targetEntity="AwardEvent")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $event;

    /** @ORM\Column(type="string", length=30) */
    protected $year;

    /** @ORM\Column(type="string", length=120) */
    protected $name;

    /** @ORM\Column(type="string", length=120) */
    protected $placement;


    public function setEvent($event)
    {
        $this->event = $event;
        return $this;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPlacement($p)
    {
        $this->placement = $p;
        return $this;
    }

    public function getPlacement()
    {
        return $this->placement;
    }


}
