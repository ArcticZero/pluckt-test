<?php

namespace Pluckt\AdminBundle\Template;

trait CanFeatureController
{
    protected $feat_repo;
    protected $feat_prefix;

    public function setFeaturedRepository($feat_repo)
    {
        $this->feat_repo = $feat_repo;
        return $this;
    }

    public function setFeaturedViewPrefix($feat_prefix)
    {
        $this->feat_prefix = $feat_prefix;
        return $this;
    }

    public function featureAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $video = $this->find($id);

        $feat_videos = $em->getRepository($this->feat_repo)
            ->findBy(['featured' => true]);

        foreach ($feat_videos as $f_video)
            $f_video->setFeatured(false);

        $video->setFeatured(true);

        $em->flush();

        return $this->redirect($this->generateUrl($this->feat_prefix . 'index'));
    }

    public function unfeatureAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $video = $this->find($id);

        $video->setFeatured(false);

        $em->flush();

        return $this->redirect($this->generateUrl($this->feat_prefix . 'index'));
    }
}
