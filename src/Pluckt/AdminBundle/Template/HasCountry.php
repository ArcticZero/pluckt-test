<?php

namespace Pluckt\AdminBundle\Template;

use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Model\Country;

trait HasCountry
{
    /** @ORM\Column(type="string", length=3) */
    protected $country_id;

    public function setCountryID($id)
    {
        $this->country_id = $id;
        return $this;
    }

    public function getCountryID()
    {
        return $this->country_id;
    }

    public function getCountryLabel()
    {
        return Country::getLabel($this->country_id);
    }
}
