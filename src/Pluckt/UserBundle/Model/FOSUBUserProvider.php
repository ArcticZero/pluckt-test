<?php

namespace Pluckt\UserBundle\Model;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseClass
{
    protected function generatePassword($length = 10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle($chars), 0, $length );
        return $password;
    }

    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        // on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'ID';
        $setter_token = $setter . 'Token';

        // "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) 
        {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        // connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();

        // search by username
        $user = $this->userManager->findUserBy([$this->getProperty($response) => $username]);

        /*
        // search by email
        if ($user == null)
            $user = $this->userManager->findUserBy(['email' => $email]);
        */

        $service = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'ID';
        $setter_token = $setter . 'Token';

        // when the user is registrating
        if ($user == null)
        {
            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            // I have set all requested data with the user's username
            // modify here with relevant data
            $password = $this->generatePassword();

            $user->setUsername($username);
            $user->setName($response->getFirstName() . ' ' . $response->getLastName());
            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);

            return $user;
        }

        // update id and access token
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        // if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        return $user;
    }
}
