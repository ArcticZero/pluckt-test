<?php

namespace Pluckt\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Pluckt\AdminBundle\Template\HasWallet;
use Zulu\MediaBundle\Entity\Upload;
use DateTime;
use Pluckt\AdminBundle\Model\Country;
use Pluckt\AdminBundle\Entity\Artist;
use Pluckt\SocialBundle\Template\HasFollows;
use Pluckt\SocialBundle\Template\SocialUser;
use Doctrine\Common\Collections\ArrayCollection;
use Pluckt\SocialBundle\Entity\ActivityLog;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true))
 * })
 */
class User extends BaseUser
{
    use HasWallet;
    use HasFollows;
    use SocialUser;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="datetime") */
    protected $date_create;

    /**
     * @ORM\Column(type="string", length=80)
     *
     * @Assert\NotBlank(message="Please enter your first name.", groups={"Registration", "Profile"})
    */
    protected $name;

    /**
     * @ORM\Column(type="string", length=80)
     *
     * @Assert\NotBlank(message="Please enter your last name.", groups={"Registration", "Profile"})
    */
    protected $last_name;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="profile_pic_id", referencedColumnName="id")
     */
    protected $profile_pic;

    /**
     * @ORM\OneToOne(targetEntity="Zulu\MediaBundle\Entity\Upload")
     * @ORM\JoinColumn(name="page_cover_id", referencedColumnName="id")
     */
    protected $page_cover;

    /** @ORM\Column(type="text") */
    protected $bio;

    /** @ORM\Column(type="string", length=3) */
    protected $country;

    /** @ORM\COlumn(type="string", length=80) */
    protected $city;

    /**
     * @ORM\OneToOne(targetEntity="Pluckt\AdminBundle\Entity\Artist")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id")
     */
    protected $artist;

    /** @ORM\Column(type="string", length=120, nullable=true) */
    protected $fb_id;

    /** @ORM\Column(type="string", length=120, nullable=true) */
    protected $fb_token;

    /** @ORM\Column(type="string", length=120, nullable=true) */
    protected $twitter_id;

    /** @ORM\Column(type="string", length=120, nullable=true) */
    protected $twitter_token;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\UserList", mappedBy="user") */
    protected $lists;

    /**
     * @ORM\ManyToMany(targetEntity="Pluckt\AdminBundle\Entity\Video", indexBy="id")
     * @ORM\JoinTable(name="user_faves",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    protected $favorite_videos;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\FNPage", mappedBy="user") */
    protected $notes;

    /** @ORM\OneToMany(targetEntity="Pluckt\SocialBundle\Entity\ActivityLog", mappedBy="user") */
    protected $logs;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\UserAward", mappedBy="user") */
    protected $awards;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\VideoAccess", mappedBy="user") */
    protected $video_accesses;

    /** @ORM\OneToMany(targetEntity="Pluckt\AdminBundle\Entity\Purchase", mappedBy="user") */
    protected $purchases;

    /**
     * @ORM\ManyToMany(targetEntity="Pluckt\SocialBundle\Entity\ActivityLog", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="user_notification",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="log_id", referencedColumnName="id")}
     *      )
     */
    protected $notifications;

    public function __construct()
    {
        parent::__construct();
        $this->initHasWallet();
        $this->initHasFollows();
        $this->initSocialUser();

        $this->name = '';
        $this->last_name = '';

        $this->date_create = new DateTime();
        $this->bio = '';
        $this->setEnabled(true);

        $this->country = 'PH';
        $this->city = 'Manila';

        $this->fb_id = null;
        $this->fb_token = null;
        $this->twitter_id = null;
        $this->twitter_token = null;

        $this->favorite_videos = new ArrayCollection();
        $this->logs = new ArrayCollection();
        $this->awards = new ArrayCollection();

        $this->notifications = new ArrayCollection();
    }

    public function getID()
    {
        return $this->id;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setLastName($name)
    {
        $this->last_name = $name;
        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getFullName() {
        return $this->name . " " . $this->last_name;
    }

    public function clearRoles()
    {
        $this->roles = array();
        return $this;
    }

    public function getRoleHash()
    {
        $role_hash = array();
        foreach ($this->roles as $role)
            $role_hash[$role] = $role;

        return $role_hash;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setProfilePic(Upload $upload)
    {
        $this->profile_pic = $upload;
        return $this;
    }

    public function getProfilePic()
    {
        return $this->profile_pic;
    }

    public function setPageCover(Upload $upload)
    {
        $this->page_cover = $upload;
        return $this;
    }

    public function getPageCover()
    {
        return $this->page_cover;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCountryLabel()
    {
        return Country::getLabel($this->country);
    }

    public function setArtist(Artist $artist = null)
    {
        $this->artist = $artist;
        return $this;
    }

    public function getArtist()
    {
        return $this->artist;
    }

    public function isArtist()
    {
        if ($this->artist == null)
            return false;

        return true;
    }

    // social stuff
    public function setFacebookID($fb_id)
    {
        $this->fb_id = $fb_id;
        return $this;
    }

    public function getFacebookID()
    {
        return $this->fb_id;
    }

    public function setFacebookToken($token)
    {
        $this->fb_token = $token;
        return $this;
    }

    public function getFacebookToken()
    {
        return $this->fb_token;
    }

    public function setTwitterID($id)
    {
        $this->twitter_id = $id;
        return $this;
    }

    public function getTwitterID()
    {
        return $this->twitter_id;
    }

    public function setTwitterToken($token)
    {
        $this->twitter_token = $token;
        return $this;
    }

    public function getTwitterToken()
    {
        return $this->twitter_token;
    }

    public function getLists()
    {
        return $this->lists;
    }

    public function addFavoriteVideo($video)
    {
        $this->favorite_videos[$video->getID()] = $video;
        return $this;
    }

    public function getFavoriteVideos()
    {
        return $this->favorite_videos;
    }

    public function isFavoriteVideo($video_id)
    {
        if (isset($this->favorite_videos[$video_id]))
            return true;

        return false;
    }

    public function getFavoriteVideosTotalMinutes()
    {   
        $total = 0;
        foreach ($this->favorite_videos as $faves)
            $total += $faves->getMinutes();

        return $total;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function getActivityLog()
    {
        return $this->logs;
    }

    public function getAwards()
    {
        return $this->awards;
    }

    public function getVideoAccesses()
    {
        return $this->video_accesses;
    }

    public function hasVideoAccess($video_id)
    {
        // TODO: improve this, it's brute force going through all related stuff
        foreach ($this->video_accesses as $access)
        {
            if ($access->getVideo()->getID() == $video_id)
                return true;
        }

        return false;
    }

    public function getPurchases()
    {
        return $this->purchases;
    }

    public function addNotification(ActivityLog $log)
    {
        $this->notifications[] = $log;
        return $this;
    }

    public function getNotifications()
    {
        return $this->notifications;
    }
}
