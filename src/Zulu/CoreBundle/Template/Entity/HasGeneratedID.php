<?php

namespace Zulu\CoreBundle\Template\Entity;

trait HasGeneratedID
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getID()
    {
        return $this->id;
    }

    protected function initHasGeneratedID()
    {
        // nothing to do
    }

    public function dataHasGeneratedID(&$data)
    {
        $data->id = $this->id;
    }
}

