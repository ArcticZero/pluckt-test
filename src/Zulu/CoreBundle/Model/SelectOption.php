<?php

namespace Zulu\CoreBundle\Model;

// class for html select tag options
class SelectOption
{
    // returns the options hash
    // options hash is an array where:
    //   array index - option value
    //   array value - option label
    public static function getOptionsHash()
    {
        return array();
    }

    // TODO: figure out if using getOptionsHash returning an array is optimal

    public static function isValid($id)
    {
        $hash = static::getOptionsHash();
        if (isset($hash[$id]))
            return true;

        return false;
    }

    public static function getLabel($id)
    {
        $hash = static::getOptionsHash();
        if (isset($hash[$id]))
            return $hash[$id];

        return 'Unknown';
    }

    public static function buildHash($arr, $id_method, $value_method)
    {
        $opts = [];
        foreach ($arr as $element)
            $opts[$element->$id_method()] = $element->$value_method();

        return $opts;
    }
}
