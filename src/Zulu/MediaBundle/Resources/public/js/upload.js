function dropzone_init(template_selector, upload_url, name) {
    var template = $(template_selector).html();
    var preview_selector = '#' + name + '-prev-container';
    var click_selector = '#' + name + '-dz-upload';
    var main_prefix = '#' + name + '-main-image';
    var dropzone_selector = '#' + name + '-dropzone';

    $(dropzone_selector).dropzone({
        url: upload_url,
        previewTemplate: template,
        previewsContainer: preview_selector,
        maxFilesize: 2,
        thumbnailWidth: 150,
        thumbnailHeight: 150,
        clickable: click_selector,
        acceptedFiles: 'image/*',
        init: function() {
            this.on('success', function(file, res) {
                // console.log(res);

                // image modal update
                // $(file.previewElement).find('.dzp-image').attr('href', res.url);
                // console.log($('#main-image'));
                $(main_prefix).attr('src', res.url);
                $(main_prefix + '-id').val(res.id);
                $(main_prefix + '-link').attr('href', res.url);

                // console.log(object_images);
            });

            this.on('complete', function(file) {
                // $(file.previewElement).find('.dzp-progress').remove();
                $(preview_selector).html('');
            });

            this.on('error', function(file) {
                var msg = $(file.previewElement).find('.dzp-error').html();
                alert(msg);
                $(file.previewElement).remove();
            });
        }
    });
}

function gallery_init(template_selector, upload_url, name) {
    var template = $(template_selector).html();
    var preview_selector = '#' + name + '-prev-container';
    var click_selector = '#' + name + '-dz-upload';
    var main_prefix = '#' + name + '-main-image';
    var dropzone_selector = '#' + name + '-dropzone';

    var container = '#' + name + '-gallery-container';

    $(dropzone_selector).dropzone({
        url: upload_url,
        previewTemplate: template,
        previewsContainer: preview_selector,
        maxFilesize: 2,
        thumbnailWidth: 150,
        thumbnailHeight: 150,
        clickable: click_selector,
        acceptedFiles: 'image/*',
        init: function() {
            this.on('success', function(file, res) {
                // console.log(res);

                // image modal update
                // $(file.previewElement).find('.dzp-image').attr('href', res.url);
                // console.log($('#main-image'));
                // add to gallery container
                var html = '<div class="col-md-4">';
                html += '<div style="width:200px; overflow:hidden;">';
                html += '<a class="dzp-image" href="' + res.url + '" data-lightbox="gallery-main-image">';
                html += '<img src="' + res.url + '" style="min-height:150px;max-height:150px;">';
                html += '<input type="hidden" name="' + name + '[]" value="' + res.id + '">';
                html += '</a>';
                html += '</div>';
                html += '<br>';
                html += '<button class="' + name + '-gallery-remove btn btn-primary" type="button">Remove</button>';
                html += '<br><br>';
                html += '</div>';
                $(container).append(html);
                /*
                $(main_prefix).attr('src', res.url);
                $(main_prefix + '-id').val(res.id);
                $(main_prefix + '-link').attr('href', res.url);
                */

                // console.log(object_images);
            });

            this.on('complete', function(file) {
                // $(file.previewElement).find('.dzp-progress').remove();
                $(preview_selector).html('');
            });

            this.on('error', function(file) {
                var msg = $(file.previewElement).find('.dzp-error').html();
                alert(msg);
                $(file.previewElement).remove();
            });
        }
    });

    $('#' + name + '-gallery-container').on('click', '.' + name + '-gallery-remove', function() {
        console.log('HERE');
        $(this).parent().remove();
    });
}
