<?php

namespace Zulu\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zulu\CoreBundle\Template\Entity\HasGeneratedID;
use Zulu\CoreBundle\Template\Entity\TrackCreate;

/**
 * @ORM\Entity
 * @ORM\Table(name="media_upload")
 */
class Upload
{
    use HasGeneratedID;
    use TrackCreate;

    const STATUS_NEW            = 1;
    const STATUS_LINKED         = 2;

    /** @ORM\Column(type="string", length=200, nullable=false) */
    protected $filename;

    /** @ORM\Column(type="string", length=200, nullable=false) */
    protected $url;

    /** @ORM\Column(type="integer") */
    protected $status;

    /** @ORM\Column(type="string", length=120, nullable=false) */
    protected $engine;

    /** @ORM\Column(type="json_array") */
    protected $data;

    public function __construct()
    {
        $this->initHasGeneratedID();
        $this->initTrackCreate();
        $this->status = self::STATUS_NEW;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setURL($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setEngine($engine)
    {
        $this->engine = $engine;
        return $this;
    }

    public function getEngine()
    {
        return $this->engine;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
}
