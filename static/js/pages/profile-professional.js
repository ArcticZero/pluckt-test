$(document).ready(function() {
	// awards slider
	$(".flexslider").flexslider({
		animation: "slide",
    	animationLoop: false,
		itemWidth: 330,
		itemHeight: 130,
		itemMargin: 30,
		controlNav: false,
		controlsContainer: $("#awards-nav"),
		customDirectionNav: $("#awards-nav button")
	});
});