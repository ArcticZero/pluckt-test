$(document).ready(function() {
	// movie slider
	$("#movie-slider .flexslider").flexslider({
		animation: "slide",
		controlNav: false,
		controlsContainer: $("#movie-slider-nav"),
		customDirectionNav: $("#movie-slider-nav a"),
		slideshowSpeed: 5000,
		video: true,
		start: function() {
			$('.flex-active-slide').find('video').get(0).play();
		},
		after: function() {
			$('.flex-active-slide').find('video').get(0).play();
		}
	});

	// page down arrow
	$("#page-down").click(function(e) {
		$('html, body').stop().animate({
	        scrollTop: $("#features").offset().top
	    }, 1000, 'easeInOutExpo');

		e.preventDefault();
	});

	// datepicker boxes
	$("#signup-birthdate").combodate({
		minYear: 1900,
		maxYear: moment().format('YYYY'),
		format: "YYYY-MM-DD",
		template: "YYYY MMMM D",
		customClass: 'form-control round inline',
		smartDays: true
	});

	// submit modal form
	$(".modal form").submit(function(e) {
		var form = $(this);
  		var loading = form.closest(".modal-content").find(".loading-pane");
  		var notice = form.find(".form-notice");
  		var noreload = form.data('noreload');

  		// stop form from submitting
  		e.preventDefault();

  		// show loader
  		loading.show();

  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', notice);
				} else if(noreload) {
					status(response.title, response.body, 'alert-success');
				} else {
					location.reload();
				}

				loading.addClass('hide');
			}
  		});
	});
});