$(document).ready(function() {
	// toggle search box
	$("#btn-search").click(function(e) {
		var querybox = $(this).siblings("input[name='q']");

		if($(window).width() > 320) {
			if(querybox.hasClass('squeeze')) {
				querybox.attr('readonly', false).removeClass('squeeze').focus();
				$(this).addClass('active');
				e.preventDefault();
			} else {
				if(querybox.val().length === 0) {
					querybox.attr('readonly', true).blur().addClass('squeeze');
					$(this).removeClass('active');
					e.preventDefault();
				}
			}
		}
	});

	// hide input placeholders on focus
	$("input[type], textarea").focus(function() {
		var ph = $(this).attr('placeholder');
		$(this).data('placeholder', ph).attr('placeholder', "");
	}).blur(function() {
		var ph = $(this).data('placeholder');
		$(this).attr('placeholder', ph).data('placeholder', "");
	});

	// handle enter key for comment box input (shift+enter for new line)
	$('.comment-box form').keyup(function(e) {
	    if(e.keyCode == 13 && !e.shiftKey) {
	    	$(this).submit();
	    }
	});

	// expanding textareas for comment boxes
	$('.comment-box textarea:visible').expanding();

	// content menu in-page links
	$(".content-menu a").click(function(e) {
		var page = $(this).attr('href') + '-page';
		var li = $(this).closest('li');

		if(!li.hasClass('active')) {
			$(".content-menu li").removeClass('active');
			li.addClass('active');

			$(".page").fadeOut('fast').addClass('hide');
			$(page).hide().removeClass('hide').fadeIn('fast');

			$(page).find('.comment-box textarea:visible').expanding();
		}

		e.preventDefault();
	});

	// hover on shelf/grid movie poster
	$(document).on("mouseover", ".movie-poster .poster, .movie-poster .overlay-white", function() {
		var box = $(this).closest('.movie-poster');
		var overlay = box.find('.overlay-white');
		box.addClass('active');
		overlay.stop().fadeIn(250);
	}).on("mouseleave", ".movie-poster .poster, .movie-poster .overlay-white", function() {
		var box = $(this).closest('.movie-poster');
		var overlay = box.find('.overlay-white');
		box.removeClass('active');
		overlay.stop().fadeOut(250);
	});

	// tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// display status message
	function status(title, body, type, box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	}
});